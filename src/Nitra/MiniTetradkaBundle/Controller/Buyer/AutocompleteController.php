<?php
namespace Nitra\MiniTetradkaBundle\Controller\Buyer;

//use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * AutocompleteController
 */
class AutocompleteController extends Controller
{
    
    /**
     * Поиск покупателей автокомплит
     * @Route("/", name="Nitra_MiniTetradkaBundle_Buyer_autocomplete")
     */
    public function indexAction(Request $request)
    {
        
        // строка поиска
        $searchValue = $request->get('q', false);
        
        // тип поиска по какому полю делаем поиск
        $searchBy = $request->get('searchBy', 'name');
        
        // если поиск по телефону то 
        // из строки поиска убрать все лишне, убрать все что не цыфры
        if ($searchBy == 'phone') {
            $searchValue = preg_replace('/[^0-9]/', '', $searchValue);
        }
        
        // получить массив слов поиска t1,t2 t3 получить массив из [t1, t2, t3]
        $words = preg_split('/[\s,]/', $searchValue, null, PREG_SPLIT_NO_EMPTY);
        
        // запрос поиска
        $searchQuery = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('b')
            ->from('NitraMiniTetradkaBundle:Buyer', 'b');
        
        // добавить в запрос слова поиска 
        $iteration = 0;
        foreach($words as $word) { ++$iteration;
            
            // проверяем поле поиска
            switch($searchBy) {
                // поиск по телефону
                case 'phone':
                    // поиск по номеру телефона
                    $paramName = 'phone'.$iteration;
                    $searchQuery->orWhere('b.phone LIKE :'.$paramName)->setParameter($paramName, $word.'%');
                    break;
                
                // поиск по имени
                case 'name':
                    $paramName = 'name'.$iteration;
                    $searchQuery->andWhere('b.name LIKE :'.$paramName)->setParameter($paramName, '%'.$word.'%');
                    break;
            }
        }
        
        // добавить сортировку списка в зависимости от поля поиска
        switch($searchBy) {
            // поиск по телефону
            case 'phone':
                $searchQuery->orderBy('b.phone', 'ASC');
                break;
            
            // поиск по имени
            case 'name':
            default:
                $searchQuery->orderBy('b.name', 'ASC');
                break;
        }
        
        // получить покупателей
        $buyers = $searchQuery
            ->setMaxResults(10)
            ->getQuery()
            ->execute();
        
        // результирующий массив
        $result = array();
        
        // обойти всех покупателей наполнить результирующий массив
        foreach($buyers as $buyer) {
            
            // результирущая запись поиска одного покупателя
            $buyerRow = array(
                'value' => $buyer->getId(),
                'jsonData' => array(
                    'id' => $buyer->getId(),
                    'name' => $buyer->getName(),
                    'phone' => $buyer->getPhone(),
            ));
            
            // добавить сортировку списка в зависимости от поля поиска
            switch($searchBy) {
                // поиск по телефону
                case 'phone':
                    $buyerRow['display'] = '+'.$buyer->getPhoneDisplay();
                    $buyerRow['label'] = '+'.$buyer->getPhoneDisplay();
                    break;

                // поиск по имени
                case 'name':
                    $buyerRow['display'] = $buyer->getName();
                    $buyerRow['label'] = $buyer->getName();
                    break;
                
                // строка ответа по умолчанию 
                default:
                    $buyerRow['display'] = $buyer->getName().' +'.$buyer->getPhoneDisplay();
                    $buyerRow['label'] = $buyer->getName().' '.$buyer->getPhoneDisplay();
                    break;
            }
            
            // обновить результрующий массив
            $result[] = $buyerRow['display'] . '|' . $buyerRow['label'] . '|' . $buyerRow['value'] . '|' . json_encode($buyerRow['jsonData']);
        }
        
        // проверить установлен ли флаг добавления новой записи
        if ($request->get('addNew', false)) {
            $buyerNew = mb_convert_case(implode(' ', $words), MB_CASE_TITLE, 'UTF-8');
            $result[] = $buyerNew. "\\\\".$this->get('translator')->trans('Новый покупатель')."|" . $buyerNew . "|" . "new" . '|' . json_encode(array());
        }
        
        // вернуть результат поиска
        return new Response(implode("\n", $result));
    }
    
    
}
