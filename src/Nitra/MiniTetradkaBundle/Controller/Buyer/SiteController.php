<?php
namespace Nitra\MiniTetradkaBundle\Controller\Buyer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * SiteController
 * Контроллер получения данных сайтом
 */
class SiteController extends Controller
{
 
    /**
     * Получить \Doctrine\ORM\EntityManager 
     */
    public function getEntityManager()
    {
        return $this->get('doctrine.orm.entity_manager');
    }
    
    /**
     * @Route("/get-buyer-info-site", name="get-buyer-info-site")
     * @return json array
     */
    public function getBuyerInfoAction() 
    {
        // получить передаваемые данные по из сайта
        $id         = $this->getRequest()->get('id');
        $phone      = $this->getRequest()->get('phone');
        $orders     = $this->getRequest()->get('orders');
        $storeId    = $this->getRequest()->get('storeId');
        
        // результирующий массив
        $json = array();
        
        // проверить обязательные параметры
        if (!$id && !$phone) {
            // ошибка
            $json['status'] = 'error';
            $json['msg']    = 'errors.input';
            // вернуть ошибку
            return new JsonResponse($json);
        }
        
        // поиск покупателя по ID
        if($id) {
            // поиск пользователя по id
            $buyer = $this->getEntityManager()->getRepository('NitraMiniTetradkaBundle:Buyer')->find($id);
            if (!$buyer) {
                // ошибка
                $json['status'] = 'error';
                $json['msg'] = 'errors.id';
                // вернуть ошибку
                return new JsonResponse($json);
            }
            
            // покупатель получен успешно 
            $json['status'] = 'success';
        }
        
        // поиск пользователя по телефону
        elseif ($phone) {
            // получить покупателя
            $buyer = $this->getEntityManager()->getRepository('NitraMiniTetradkaBundle:Buyer')->findOneByPhone($phone);
            if (!$buyer) {
                // ошибка
                $json['status'] = 'error';
                $json['msg'] = 'errors.phone';
                // вернуть ошибку
                return new JsonResponse($json);
            }
            
            // покупатель получен успешно 
            $json['status'] = 'success';
        }
        
        // покупатель получен успешно
        if ($buyer && ($json['status'] == 'success')) {
            
            // данные покупателя
            $json['buyer'] = array(
                'id' => $buyer->getId(),
                'name' => $buyer->getName(),
                'phone' => $buyer->getPhone(),
                // 'address'   => $buyer->getAddress(),
            );
            
            // добавить заказы покупателя
            if ($orders) {
                // получить список заказов
                $_orders = $this->getEntityManager()->getRepository('NitraMiniTetradkaBundle:Order')->findBy(array(
                    'buyer' => $buyer->getId(),
                    //'storeId'   => $storeId,
                    ), array('deliveryDate' => 'DESC'));
                // запоснить список заказов в результирующем ответе
                $json['orders'] = array();
                $json['orders_history'] = array();
                foreach ($_orders as $order) {
                    // город заказа
                    $city = $order->getCity();
                    
                    $history_or_no = 'orders';
                    if ($order->getOrderStatus()->getMethodName() == 'completed') {
                        $history_or_no = 'orders_history';
                    }
                    $json[$history_or_no][$order->getId()]['status']        = $order->getOrderStatus()->getName();
                    $json[$history_or_no][$order->getId()]['updatedAt']     = $order->getUpdatedAt();                    // дата последнего обновления (для оповещений)
                    $json[$history_or_no][$order->getId()]['completedAt']   = $order->getCompletedAt();                  // дата завершения
                    $json[$history_or_no][$order->getId()]['deliveryDate']  = $order->getDeliveryDate();                 // дата доставки
                    $json[$history_or_no][$order->getId()]['deliveryCost']  = $order->getDeliveryCost();                 // стоимость доставки
                    $json[$history_or_no][$order->getId()]['createdAt']     = $order->getCreatedAt();                    // дата создания заказа 
                    $json[$history_or_no][$order->getId()]['isDirect']      = $order->getIsDirect();                     // адрессная ли доставка?
                    $json[$history_or_no][$order->getId()]['isCod']         = $order->getIsCod();                        // Наложенный платёж
                    $json[$history_or_no][$order->getId()]['address']       = $order->getAddress();
                    $json[$history_or_no][$order->getId()]['city']          = array(
                        'name'      => ($city) ? (string)$city : '',
                        'region'    => ($city) ? (string)$city->getRegion() : '',
                    );
                    $json[$history_or_no][$order->getId()]['orderEntries']  = array();

                    foreach ($order->getOrderEntry() as $order_entry) {
                        $json[$history_or_no][$order->getId()]['orderEntries'][$order_entry->getId()] = array(
                            'quantity' => $order_entry->getQuantity(),
                            'currency' => array(
                                'name'      => $order_entry->getCurrency()->getName(),
                                'exchange'  => $order_entry->getCurrency()->getExchange(),
                                'symbol'    => $order_entry->getCurrency()->getSymbol(),
                                'code'      => $order_entry->getCurrency()->getCode(),
                            ),
                            'priceOut'          => $order_entry->getPriceOut(),
                            'productId'         => $order_entry->getProductId(),
                            'discountPercent'   => $order_entry->getDiscountPercent(),
                            'status'            => $order_entry->getOrderEntryStatus()->getName(),
                        );
                    }
                }
            }
        }
        
        // вернуть результат
        return new JsonResponse($json);
    }
    
    
}
