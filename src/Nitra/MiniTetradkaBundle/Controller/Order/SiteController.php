<?php
namespace Nitra\MiniTetradkaBundle\Controller\Order;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nitra\MiniTetradkaBundle\Entity\Buyer;
use Nitra\MiniTetradkaBundle\Entity\Order;
use Nitra\MiniTetradkaBundle\Entity\OrderEntry;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;

/**
 * SiteController
 * Контроллер получения данных сайтом
 */
class SiteController extends Controller
{
    
    /**
     * Получить DocumentManager
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     * @DI\Inject("doctrine_mongodb.odm.document_manager") */
    private $dm;
    
    /**
     * получить сервис изменения статусов заказа
     * @DI\Inject("nitra.minitetradka.order_status") */
    private $nitraOrderStatus;
    
    /**
     * получить сервис изменения статусов заказа
     * @DI\Inject("nitra.minitetradka.order_entry_status") */
    private $nitraOrderEntryStatus;
    
    /**
     * @var array $deliveryWarehouses - массив складов ТК
     */
    private $deliveryWarehouses;
    
    
    /**
     * Получить EntityManager
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->get('doctrine.orm.entity_manager');
    }

        /**
     * Получить города города из базы тетрадки для сайта
     * @Route("/city/get-all-cities", name="get-all-cities")
     * @return json
     */
    public function getAllCititesAction() 
    {
//        // получить массив городов
//        $cities = $this->getEntityManager()
//            ->createQueryBuilder()
//            ->select('c.name as city_name, c.id, r.name as region_name')->from('TetradkaGeoBundle:City', 'c')
//            ->leftJoin("TetradkaGeoBundle:Region", "r", "WITH", "c.region = r.id")
//            ->getQuery()
//            ->getArrayResult();
//        
//        // вернуть массив городов
//        return new JsonResponse($cities);
        return new JsonResponse();
    }
    
    /**
     * @Route("/order/buyers-synchronizer", name="buyers_synchronizer")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function buyersSynchronizerAction(Request $request)
    {
        // если пришли пользователи - обновляем
        if ($request->request->has('buyers')) {
            $inserted = 0;
            $updated  = 0;
            $failed   = 0;
            foreach ($request->request->get('buyers') as $id => $buyer) {
                // если пользователь подвязан - обновляем, иначе - создаем новый
                $eBuyer = key_exists('tetradkaId', $buyer)
                    ? $this->getEntityManager()->find('NitraMiniTetradkaBundle:Buyer', $buyer['tetradkaId'])
                    : new Buyer();
                $Buyer = $eBuyer
                    ? $eBuyer
                    : new Buyer();
                
                try {
                    $Buyer->setName($buyer['name']);
                    if (key_exists('phone', $buyer)) {
                        $Buyer->setPhone($buyer['phone']);
                    }
                    if (key_exists('email', $buyer) && method_exists('\Nitra\NitraMiniTetradkaBundle\Entity\Buyer', 'setEmail')) {
                        $Buyer->setEmail($buyer['email']);
                    }
                    $this->getEntityManager()->persist($Buyer);
                    $this->getEntityManager()->flush($Buyer);
                    if (key_exists('tetradkaId', $buyer)) {
                        $updated ++;
                    } else {
                        $inserted ++;
                    }
                } catch (\Exception $ex) {
                    $failed ++;
                }
            }
            $json = array(
                'inserted'  => $inserted,
                'updated'   => $updated,
                'failed'    => $failed,
            );
        // иначе - отдаем
        } else {
            $email = method_exists('\Nitra\NitraMiniTetradkaBundle\Entity\Buyer', 'getEmail') ? ' b.email,' : null;
            $Buyers = $this->getEntityManager()->createQueryBuilder()
                ->from('NitraMiniTetradkaBundle:Buyer', 'b')
                ->select("b.id, b.name, b.phone,$email o.address, c.id as city_id, c.name as city_name")
                ->leftJoin("NitraMiniTetradkaBundle:Order", "o", "WITH", "b.id = o.buyer")
                ->leftJoin("NitraTetradkaGeoBundle:City", "c", "WITH", "c.id = o.city")
                ->orWhere('o.createdAt = (SELECT MAX(so.createdAt) FROM NitraMiniTetradkaBundle:Order so WHERE so.buyer = b.id)')
                ->orWhere('o.createdAt IS NULL')
                ->getQuery()->execute(null, \Doctrine\ORM\Query::HYDRATE_ARRAY);
            $json   = array();

            foreach ($Buyers as $buyer) {
                $id = $buyer['id'];
                $json[$id] = $buyer;
            }
        }
        
        return new JsonResponse($json);
    }
    
    /**
     * обновление пользователя, при редактировании им данных из личного кабинета
     * @Route("/order/update-buyer", name="update_buyer")
     */
    public function updateBuyer(Request $request)
    {
        $data   = $request->request->all();
        if (!isset($data['id']) || !$data['id']) {
            return new JsonResponse();
        }
        
        $Buyer  = $this->getEntityManager()->find('NitraMiniTetradkaBundle:Buyer', $data['id']);
        $return = array();
        
        if ($Buyer) {
            $Buyer->setName($data['name']);
            $Buyer->setPhone($data['phone']);
            if (method_exists('\Nitra\NitraMiniTetradkaBundle\Entity\Buyer', 'setEmail')) {
                $Buyer->setEmail($data['email']);
            }
            $this->getEntityManager()->flush($Buyer);
            $return['status'] = 'success';
        } else {
            $return['status'] = 'error';
        }
        
        return new JsonResponse($return);
    }
    
    
    /**
     * @return json все валюты для сайта
     * @Route("/currency/get-all-currencies", name="get-all-currencies")
     */
    public function getAllCurrenciesAction()
    {
        $em                 = $this->get('doctrine.orm.entity_manager');
        $objCurrencies      = $em->getRepository('NitraMiniTetradkaBundle:Currency')->findAll();
        $currencies         = array();
        $baseCurrencyCode   = $this->container->has('currency_code') ? $this->container->get('currency_code') : 'uah';
        
        // наполнить результирующий массив
        foreach($objCurrencies as $currency) {
            $currencies[$currency->getCode()] = array(
                'name'      => $currency->getName(),
                'exchange'  => ($currency->getCode() == $baseCurrencyCode) ? 1.0 : $currency->getExchange(),
                'symbol'    => $currency->getSymbol(),
                'base'      => ($currency->getCode() == $baseCurrencyCode),
                'selected'  => false,
            );
        }
        
        // вернуть массив валют
        return new JsonResponse($currencies);
    }
    
    /**
     * Проверить ТК имеет склад в городе
     * @param integer $deliveryId - ID ТК 
     * @param integer $cityId - ID города 
     * @return boolean 
     *          true  - ТК есть склад в городе
     *          false - ТК нет склада в городе
     */
    private function isDeliveryHasWarehouseInCity($deliveryId, $cityId)
    {
        
        // провевить получали ли ранее склады
        if (is_null($this->deliveryWarehouses)) {
            $this->deliveryWarehouses = $this->getEntityManager()
                ->getRepository('NitraMiniTetradkaBundle:Warehouse')
                ->buildQueryDeliveryWarehouses()
                ->select('q, d, c')
                ->innerJoin('q.delivery', 'd')
                ->innerJoin('q.city', 'c')
                ->getQuery()
                ->getArrayResult();
        }
        
        // обойти массив складов
        foreach($this->deliveryWarehouses as $warehouseId => $warehouse) {
            // проверить если склад ТК принадлежит проверяемой ТК 
            // и склад в проверяемом городе 
            if ($warehouse['delivery']['id'] == $deliveryId && 
                $warehouse['city']['id'] == $cityId
            ) {
                // ТК есть склад в городе
                return true;
            }
        }
        
        // нет ТК в городе
        return false;
    }
    
    /**
     * получение списка городов для сайта
     * все города из базы тетрадки в которые осуществляется доставка 
     * @return json 
     * @Route("/city/get-delivery-cities", name="get-delivery-cities")
     */
    public function getDeliveryCitites(Request $request)
    {
        
        // получить склады
        $warehousesResult = $this->getEntityManager()
            ->getRepository('NitraMiniTetradkaBundle:Warehouse')
            ->buildQueryDeliveryWarehouses()
            ->select('q, d, c, r')
            ->innerJoin('q.delivery', 'd')
            ->innerJoin('q.city', 'c')
            ->innerJoin('c.region', 'r')
            ->andwhere('q.address IS NOT NULL AND LENGTH(q.address) > 0')
            ->getQuery()
            ->getArrayResult();        
        
        // проверить склады
        if (!$warehousesResult) {
            // нет данных 
            return new JsonResponse();
        }
        
        // массив регионов
        $regions = array();
        $sortByRegion = array();
        
        // массив городов
        $cities = array();
        $sortByCity = array();
        
        // обойти склады наполниь массивы ТК и складов
        foreach($warehousesResult as $warehouse) {
            
            // получить регион для быстрго обращения
            $region = $warehouse['city']['region'];
            
            // наполнить массив регионов 
            if (!isset($regions[$region['id']])) {
                // запомнить регион
                $regions[$region['id']] = array(
                    'id'            => $region['id'], 
                    'businessKey'   => $region['businessKey'],
                    'name'          => $region['name'],
                );
                // обновить массив сортировки
                $sortByRegion[] = $region['name'];
            }
            
            // получить город для быстрго обращения
            $city = $warehouse['city'];
            
            // наполнить массив городов 
            if (!isset($cities[$city['id']])) {
                // запомнить город
                $cities[$city['id']] = array(
                    'id'            => $city['id'], 
                    'businessKey'   => $city['businessKey'],
                    'name'          => $city['name'],
                    'regionId'      => $region['id'],
                    'regionName'    => $region['name'],
                );
                // обновить массив сортировки
                $sortByCity[] = $city['name'];
            }
        }
        
        // выполнить сортировку регионов
        array_multisort(
            $sortByRegion, SORT_STRING, SORT_ASC,
            $regions
        );
        
        // выполнить сортировку городов
        array_multisort(
            $sortByCity, SORT_STRING, SORT_ASC,
            $cities
        );
        
        // результрующий массив
        $result = array(
            'regions'       => $regions,
            'cities'        => $cities,
        );
        
        // вернуть результирующий массив
        return new JsonResponse($result);
    }
    
    /**
     * получение списка городов для сайта 
     * города из базы тетрадки с флаголм наличия ТК в городе
     * @return json 
     * @Route("/city/get-delivery-cities-with-tk", name="get-delivery-citieswith-tk")
     */
    public function getDeliveryCititesWithTk(Request $request)
    {
        
        // получить ТК 
        $deliveries = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('d.id, d.businessKey, d.name')
            ->from('NitraMiniTetradkaBundle:Delivery', 'd', 'd.id')
            ->getQuery()
            ->getArrayResult();
        
        // проверить ТК 
        if (!$deliveries) {
            // нет данных 
            return new JsonResponse();
        }
        
        // получить коллекцию городов
        $cities = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('geoCity.id, geoCity.businessKey, geoCity.name')
            ->from('NitraTetradkaGeoBundle:City', 'geoCity', 'geoCity.id')
            ->where('geoCity.businessKey IS NOT NULL')
            ->getQuery()
            ->getArrayResult();
        
        // проверить города
        if (!$cities) {
            // нет данных 
            return new JsonResponse();
        }
        
        // добавить доступность ТК в каждый город
        foreach($cities as $city) {
            // проверить наличие склада в горде
            foreach($deliveries as $delivery) {
                $cities[$city['id']]['availableDeliveries'][$delivery['id']] = $this->isDeliveryHasWarehouseInCity($delivery['id'], $city['id']);
            }
        }
        
        // результрующий массив
        $result = array(
            'deliveries' => $deliveries,
            'cities' => $cities,
        );
        
        // вернуть результирующий массив
        return new JsonResponse($result);
    }
    
    /**
     * получение списка городов, ТК, складов для сайта
     * @return json все склады из базы тетрадки в которые осуществляется доставка 
     * @Route("/city/get-delivery-warehouses", name="get-delivery-warehouses")
     */
    public function getDeliveryWarehouses(Request $request)
    {
        
        // получить склады
        $warehousesResult = $this->getEntityManager()
            ->getRepository('NitraMiniTetradkaBundle:Warehouse')
            ->buildQueryDeliveryWarehouses()
            ->select('q, d, c, r')
            ->innerJoin('q.delivery', 'd')
            ->innerJoin('q.city', 'c')
            ->innerJoin('c.region', 'r')
            ->andwhere('q.address IS NOT NULL AND LENGTH(q.address) > 0')
            ->getQuery()
            ->getArrayResult();        
        
        // проверить склады
        if (!$warehousesResult) {
            // нет данных 
            return new JsonResponse();
        }
        
        // массив ТК
        $deliveries = array();
        $sortByDelivery = array();
        
        // массив регионов
        $regions = array();
        $sortByRegion = array();
        
        // массив городов
        $cities = array();
        $sortByCity = array();
        
        // массив складов
        $warehouses = array();
        $sortByWarehouse = array();
        
        // обойти склады наполниь массивы ТК и складов
        foreach($warehousesResult as $warehouse) {
            
            // получить ТК для быстрго обращения
            $delivery = $warehouse['delivery'];
            
            // наполнить массив ТК 
            if (!isset($deliveries[$delivery['id']])) {
                // запомнить ТК 
                $deliveries[$delivery['id']] = array(
                    'id'            => $delivery['id'], 
                    'businessKey'   => $delivery['businessKey'],
                    'name'          => $delivery['name'],
                );
                // обновить массив сортировки
                $sortByDelivery[] = $delivery['name'];
            }
            
            // получить регион для быстрго обращения
            $region = $warehouse['city']['region'];
            
            // наполнить массив регионов 
            if (!isset($regions[$region['id']])) {
                // запомнить регион
                $regions[$region['id']] = array(
                    'id'            => $region['id'], 
                    'businessKey'   => $region['businessKey'],
                    'name'          => $region['name'],
                );
                // обновить массив сортировки
                $sortByRegion[] = $region['name'];
            }
            
            // получить город для быстрго обращения
            $city = $warehouse['city'];
            
            // наполнить массив городов 
            if (!isset($cities[$city['id']])) {
                // запомнить город
                $cities[$city['id']] = array(
                    'id'            => $city['id'], 
                    'businessKey'   => $city['businessKey'],
                    'name'          => $city['name'],
                    'regionId'      => $region['id'],
                    'regionName'    => $region['name'],
                );
                // обновить массив сортировки
                $sortByCity[] = $city['name'];
            }
            
            // наполнить массив складов
            $warehouses[$warehouse['id']] = array(
                'id'            => $warehouse['id'],
                'deliveryId'    => $warehouse['delivery']['id'],
                'cityId'        => $warehouse['city']['id'],
                'businessKey'   => $warehouse['businessKey'],
                'address'       => $warehouse['address'],
            );
            // обновить массив сортировки
            $sortByWarehouse[] = $warehouse['address'];
        }
        
        // выполнить сортировку ТК 
        array_multisort(
            $sortByDelivery, SORT_STRING, SORT_ASC,
            $deliveries
        );
        
        // выполнить сортировку регионов
        array_multisort(
            $sortByRegion, SORT_STRING, SORT_ASC,
            $regions
        );
        
        // выполнить сортировку городов
        array_multisort(
            $sortByCity, SORT_STRING, SORT_ASC,
            $cities
        );
        
        // выполнить сортировку складов
        array_multisort(
            $sortByWarehouse, SORT_STRING, SORT_ASC,
            $warehouses
        );
        
        // результрующий массив
        $result = array(
            'deliveries'    => $deliveries,
            'regions'       => $regions,
            'cities'        => $cities,
            'warehouses'    => $warehouses,
        );
        
        // вернуть результирующий массив
        return new JsonResponse($result);
    }
    
    
    /**
     * получить первый склад на котором есть продукт
     * @Route("/order/get-closest-warehouse-for-product", name="get_closest_warehouse_for_product")
     */
    public function getClosestWarehouseForProduct(Request $request)
    {
        // продукт не найден ни на одном из складов
        // контрабас нет стоков
        return new JsonResponse();
        
        // получить продукт
        $product = $this->dm
            ->getRepository('NitraMiniTetradkaBundle:Product')
            ->find($request->get('productId', false));
        
        // Проверить продукт
        if (!$product) {
            // вернуть пустой результат
            return new JsonResponse();
        }
        
        // получаем id своих складов
        $ownWarehouseIds = $this->getEntityManager()
            ->getRepository('NitraMiniTetradkaBundle:Warehouse')
            ->buildQueryOwnWarehouses()
            ->select('q.id, q.id')
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // если есть свои склады 
        if ($ownWarehouseIds) {
            // получаем стоки складов
            $ownStocks = $this->dm
                ->createQueryBuilder('NitraMiniTetradkaBundle:Stock')
                ->field('productId')->equals($product->getId())
                // проверяем склад по array_keys потмоу что 
                // в проитвном случае монго воспринимает 
                // key как integer value как string
                // и отдает не праивльный результат
                ->field('warehouseId')->in(array_keys($ownWarehouseIds))
                // получаем стоки свободные для заказа 
                ->where("function() { return (this.quantity-this.reserved) > 0; }")
                ->getQuery()
                ->execute();
            
            // проверить стоки 
            if ($ownStocks->count()) {
                
                // получить склад первого стока
                $ownWarehouse = $this->getEntityManager()
                    ->getRepository('NitraMiniTetradkaBundle:Warehouse')
                    ->find($ownStocks->getSingleResult()->getWarehouseId());
                
                // вернуть склад
                return new JsonResponse(array(
                    'id' => $ownWarehouse->getId(),
                    'businessKey' => $ownWarehouse->getBusinessKey(),
                    'deliveryId' => ($ownWarehouse->getDelivery()) ? $ownWarehouse->getDelivery()->getId() : false,
                    'supplierId' => ($ownWarehouse->getSupplier()) ? $ownWarehouse->getSupplier()->getId() : false,
                    'address' => $ownWarehouse->getAddress(),
                    'cityId' => ($ownWarehouse->getCity()) ? $ownWarehouse->getCity()->getId() : false,
                    'cityBusinessKey' => ($ownWarehouse->getCity()) ? $ownWarehouse->getCity()->getBusinessKey() : false,
                ));
            }
        }
        
        
        // получаем id складов ТК
        $tkWarehouseIds = $this->getEntityManager()
            ->getRepository('NitraMiniTetradkaBundle:Warehouse')
            ->buildQueryDeliveryWarehouses()
            ->select('q.id, q.id')
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // если есть склады 
        if ($tkWarehouseIds) {
            // получаем стоки складов
            $tkStocks = $this->dm
                ->createQueryBuilder('NitraMiniTetradkaBundle:Stock')
                ->field('productId')->equals($product->getId())
                // проверяем склад по array_keys потмоу что 
                // в проитвном случае монго воспринимает 
                // key как integer value как string
                // и отдает не праивльный результат
                ->field('warehouseId')->in(array_keys($tkWarehouseIds))
                // получаем стоки свободные для заказа 
                ->where("function() { return (this.quantity-this.reserved) > 0; }")
                ->getQuery()
                ->execute();
            
            // проверить стоки 
            if ($tkStocks->count()) {
                
                // получить склад первого стока
                $tkWarehouse = $this->getEntityManager()
                    ->getRepository('NitraMiniTetradkaBundle:Warehouse')
                    ->find($tkStocks->getSingleResult()->getWarehouseId());
                
                // вернуть склад
                return new JsonResponse(array(
                    'id' => $tkWarehouse->getId(),
                    'businessKey' => $tkWarehouse->getBusinessKey(),
                    'deliveryId' => ($tkWarehouse->getDelivery()) ? $tkWarehouse->getDelivery()->getId() : false,
                    'supplierId' => ($tkWarehouse->getSupplier()) ? $tkWarehouse->getSupplier()->getId() : false,
                    'address' => $tkWarehouse->getAddress(),
                    'cityId' => ($tkWarehouse->getCity()) ? $tkWarehouse->getCity()->getId() : false,
                    'cityBusinessKey' => ($tkWarehouse->getCity()) ? $tkWarehouse->getCity()->getBusinessKey() : false,
                ));
            }
        }
        
        
        // получаем id складов поставщиков
        $supWarehouseIds = $this->getEntityManager()
            ->getRepository('NitraMiniTetradkaBundle:Warehouse')
            ->buildQuerySupplierWarehouses()
            ->select('q.id, q.id')
            ->getQuery()
            ->execute(array(), 'KeyPair');
        
        // если есть склады 
        if ($supWarehouseIds) {
            // получаем стоки складов
            $supStocks = $this->dm
                ->createQueryBuilder('NitraMiniTetradkaBundle:Stock')
                ->field('productId')->equals($product->getId())
                // проверяем склад по array_keys потмоу что 
                // в проитвном случае монго воспринимает 
                // key как integer value как string
                // и отдает не праивльный результат
                ->field('warehouseId')->in(array_keys($supWarehouseIds))
                // получаем стоки свободные для заказа 
                ->where("function() { return (this.quantity-this.reserved) > 0; }")
                ->getQuery()
                ->execute();
            
            // проверить стоки 
            if ($supStocks->count()) {
                
                // получить склад первого стока
                $supWarehouse = $this->getEntityManager()
                    ->getRepository('NitraMiniTetradkaBundle:Warehouse')
                    ->find($supStocks->getSingleResult()->getWarehouseId());
                
                // вернуть склад
                return new JsonResponse(array(
                    'id' => $supWarehouse->getId(),
                    'businessKey' => $supWarehouse->getBusinessKey(),
                    'deliveryId' => ($supWarehouse->getDelivery()) ? $supWarehouse->getDelivery()->getId() : false,
                    'supplierId' => ($supWarehouse->getSupplier()) ? $supWarehouse->getSupplier()->getId() : false,
                    'address' => $supWarehouse->getAddress(),
                    'cityId' => ($supWarehouse->getCity()) ? $supWarehouse->getCity()->getId() : false,
                    'cityBusinessKey' => ($supWarehouse->getCity()) ? $supWarehouse->getCity()->getBusinessKey() : false,
                ));
            }
        }
        
        // продукт не найден ни на одном из складов
        return new JsonResponse();
    }    
    
    /**
     * отправить ответ для сайта 
     * @param string $type      - тип отправляемого ответа
     * @param string $message   - текст сообщения отправляемый сайту
     * @return JsonResponse
     */
    private function siteResponse($type, $message) 
    {
        // результирующий массив ответа 
        $result = array(
            'status' => $type,
            'message' => $message,
        );
        
        // вернуть ответ на сайт
        return new JsonResponse($result);
    }
    
    /**
     * создание заказа с сайта
     * @Route("/create-order-from-site", name="create-order-from-site")
     * @return json array
     */
    public function createOrderFromSiteAction(Request $request)
    {
        
        // получить данные по заказу из сайта
        $orderData = $request->get('orderData', false);
        if(!$orderData) {
            // ошибка
            return $this->siteResponse('error', 'Не указан массив параметров для формирования заказа.');
        }
        
        //декодировать данные из json в масссив
        $orderData = json_decode($orderData, true); 
        if(!$orderData) {
            // ошибка
            return $this->siteResponse('error', 'Массив параметров для формирования заказа передан не верно.');
        }
        
        // проверка входящих параметров
        if(!isset($orderData['products']) || !$orderData['products'] || !is_array($orderData['products'])
            || !isset($orderData['storeId']) || !$orderData['storeId']
            || !isset($orderData['fio']) || !$orderData['fio']
            || !isset($orderData['phone']) || !$orderData['phone']
        ) {
            // ошибка
            return $this->siteResponse('error', 'Не указаны обязательные параметры для формирования заказа.');
        }
        
        // проверить магазин сайта и магазин тетрадки
        if ($orderData['storeId'] != $this->container->getParameter('nitra_minitetradka_store_id')) {
            // ошибка
            return $this->siteResponse('error', 'Магазин сайта не соответствует магазину тетрадки.');
        }
        
        // валидировать продукты
        $productIds = array();
        foreach($orderData['products'] as $product) {
            // проверка массива продуктов обязательные параметры
            if(!isset($product['quantity']) || !$product['quantity']
                || !isset($product['price']) || !$product['price']
                || !isset($product['productId']) || !$product['productId']
            ) {
                // ошибка
                return $this->siteResponse('error', 'Не указаны обязательные параметры по продуктам для формирования заказа.');
            }
            
            // запомнить ID продукта
            $productIds[] = $product['productId'];
        }
        
        // проверить массив ID продуктов
        if (!$productIds) {
            // ошибка
            $this->siteResponse('error', 'Не указаны продукты для формирования заказа.');
        }
        
        // получить продукты из базы
        $productsMongo = NitraGlobals::$dm
            ->createQueryBuilder('NitraMiniTetradkaBundle:Product')
            ->field('id')->in($productIds)
            ->getQuery()
            ->execute();
        
        // массив продуктов
        // преобразовать к виду array productId => Product
        $products = array();
        foreach($productsMongo as $product) {
            $products[$product->getId()] = $product;
        }
        
        // проверка кол-ва найденных продуктов 
        // и кол-ва продуктов полученных с сайта
        // исключаем ситуацию когда не найден какойто продукт
        // все продукты долны быть найдены
        if (count($productIds) != count($products)) {
            $this->siteResponse('error', 'В базе данных не найдены продукты продукты для формирования заказа.');
        }
        
        // поиск покупателя по телефону
        $buyer = $this->getDoctrine()
            ->getManager()
            ->getRepository('NitraMiniTetradkaBundle:Buyer')
            ->findOneByPhone($orderData['phone']);
        
        // покупатель не найден
        if (!$buyer) {
            // создать нового покупателя
            $buyer = new Buyer();
            $buyer->setName($orderData['fio']);
            $buyer->setPhone($orderData['phone']);
            $this->getDoctrine()->getManager()->persist($buyer);
        }
        
        // получить валидатор
        $validator = $this->container->get('validator');
        
        // получить валюту поу молчанию
        $currencyDefault = $this->getDoctrine()
            ->getManager()
            ->getRepository('NitraMiniTetradkaBundle:Currency')
            ->getDefaultCurrency();
        
        // создать заказ
        $order = new Order();
        $order->setBuyer($buyer);
        
        // persist заказ
        $this->getDoctrine()
            ->getManager()
            ->persist($order);
        
        // установить статус заказа по умолчанию 
        $this->nitraOrderStatus->setDefaultStatusForOrder($order);
        
        // добавить комментарий к заказу
        if (isset($orderData['comment']) && trim($orderData['comment'])) {
            $order->addComment($orderData['comment'], 'Покупатель');
            // доклеиваем в конец комментария флаг-разделитель конца комментария покупателя
            $comment = $order->getComment().'<span class="endBuyer"></span>';
            $order->setComment($comment);
        }     
        
        // установка города (если был передан и найден в базе) 
        if(isset($orderData['city']) && $orderData['city']) {
            // получить город
            $city = $this->getDoctrine()
                ->getManager()
                ->getRepository('NitraTetradkaGeoBundle:City')
                ->find($orderData['city']);
            // город найден 
            if($city) {
               // установить для заказа город
               $order->setCity($city);
            }
        }
        
        // установка адреса доставки если указан
        if(isset($orderData['delivery_address']) && $orderData['delivery_address']) {
            $order->setIsDirect(true);
            $order->setAddress($orderData['delivery_address']);
        }
        
        // добавить продукты в заказ
        foreach($orderData['products'] as $product) {
            
            // создать позицию заказа
            $orderEntry = new OrderEntry();
            $orderEntry->setOrder($order);
            $orderEntry->setProductId($product['productId']);
            $orderEntry->setQuantity($product['quantity']);
            // установить статус позиции заказа по умолчанию 
            // установить статус позиции заказа по умолчанию 
            $this->nitraOrderEntryStatus->setDefaultStatusForOrderEntry($orderEntry);

            // persist позицию заказа
            $this->getDoctrine()
                ->getManager()
                ->persist($orderEntry);
            
            // сохранение скидки в процентах
            if(isset($product['discountPercent'])) {
                // позиция со скидкой
                $orderEntry->setDiscountPercent($product['discountPercent']);
                // добавить коментарий к заказу
                $order->addComment('Скидка: '.$product['discountPercent'].'% '.(string)$products[$product['productId']], 'Скидка');
            }
            
            // если пришла цена со скидкой записываем цену со скидкой 
            if (isset($product['priceDiscount']) && 
                $product['priceDiscount'] &&
                $product['priceDiscount'] != $product['price']
            ) {
                // записываем цену со скидкой
                $orderEntry->setPriceOut($product['priceDiscount']);
                
                // добавить коментарий к заказу
                $comment = (string) $products[$product['productId']]
                    ."\nЦена со скидкой: ".$product['priceDiscount']. ' '.$currencyDefault->getSymbol()
                    ."\nРекомендованная цена: ".$product['price']. ' '.$currencyDefault->getSymbol();
                $order->addComment($comment, 'Цена');
                
            } else {
                // иначе записываем цену продукта
                $orderEntry->setPriceOut($product['price']);
            }
            
            // для минитетрадки 
            // цена входа == цене выхода == рекомендованную цена
            $orderEntry->setPriceIn($orderEntry->getPriceOut()); 
            $orderEntry->setPriceRecommended($orderEntry->getPriceOut()); 
            
            // уствновить валюту
            if(isset($product['currencyCode'])){
                // получить валюту
                $currency = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('NitraMiniTetradkaBundle:Currency')
                    ->find($product['currencyCode']);
                
                // устанвовить валюту с сайта
                $orderEntry->setCurrency($currency);
                
            } else {
                
                // установить валюту по умолчанию 
                $orderEntry->setCurrency($currencyDefault);
            }
            
            // валидировать позицию заказа
            $errors = $validator->validate($orderEntry);
            if ($errors->count()) {
                foreach($errors as $error) {
                    // ошибка
                    return $this->siteResponse('error', "Ошибка валидации позиции заказа. Параметр: \"".$error->getPropertyPath()."\". ". $error->getMessage());
                }
            }
            
            // добавить в заказ позицию заказа
            $order->addOrderEntry($orderEntry);
        }
        
        // валидировать заказ
        $errors = $validator->validate($orderEntry);
        if ($errors->count()) {
            foreach ($errors as $error) {
                // ошибка
                return $this->siteResponse('error', "Ошибка валидации заказа. Параметр: \"" . $error->getPropertyPath() . "\". " . $error->getMessage());
            }
        }
        
        // обработка сохранения заказа
        try {
            
            // flush 
            $this->getDoctrine()
                ->getManager()
                ->flush();
            
        }
        catch (\Exception $e) {
            // ошибка
            return $this->siteResponse('error', "Ошибка сохранения заказа. " . $e->getMessage());
        }
        
        
        // заказ сохранен успешно
        return new JsonResponse(array(
            'status' => 'success',
            'orderId' => $order->getId(),
        ));
    }
    
}
