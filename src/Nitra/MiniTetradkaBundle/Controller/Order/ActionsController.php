<?php
namespace Nitra\MiniTetradkaBundle\Controller\Order;

use Admingenerated\NitraMiniTetradkaBundle\BaseOrderController\ActionsController as BaseActionsController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;
use Nitra\MiniTetradkaBundle\Form\Type\Order\OrderCommentType;


/**
 * ActionsController
 */
class ActionsController extends BaseActionsController
{
    
    /**
     * добавление комментария
     * @Route("/comment-add", name="Nitra_MiniTetradkaBundle_Order_comment_add")
     * @return JsonResponse
     */
    public function commentAddAction()
    {
        
        // обработка формы
        if ($this->getRequest()->getMethod() == 'POST') {
            
            // объект формы 
            $form = $this->createForm(new OrderCommentType());
            
            // заполнить форму 
            $form->submit($this->getRequest());
            
            // проверить валидность формы
            if ($form->isValid()) {
                
                // получить обект данных форм
                $formData = $form->getData();
                // получить заказ
                $order = $form['id']->getData();
                
                // получить пользователя
                $user = NitraGlobals::getUser();
                $username = ($user) 
                    ? $user->getUsername()
                    : '';
                
                // добавить коментарий
                $order->addComment($formData['comment'], $username);                    
                
                // сохранить 
                try {
                    
                    // сохранить 
                    $this->getDoctrine()->getManager()->flush();
                    
                } catch (\Exception $e) {
                    // вернуть ошибку
                    return new JsonResponse(array('type' => 'error', 'message' => $e->getMessage()));
                }
                
                // комментарий успешно добавлен, вернуть текст комментария
                return new JsonResponse(array('type' => 'success', 'orderId' => $order->getId(), 'comment' => $order->getComment()));
                
            } else {
                
                // Текст ошибки валидации
                $errorsText = '';
                
                // ошибка валидации по форме
                foreach($form->getErrors() as $error) {
                    $errorsText .= " ".$error->getMessage();
                }
                
                // ошибка валидации по каждому виджету
                foreach ($form->all() as $child) {
                    $errors = $child->getErrors();
                    if ($errors) {
                        $errorsText .= " ".$errors[0]->getMessage();
                    }
                }
                
                // отображение ошибки валидации формы
                return new JsonResponse(array(
                    'type' => 'error', 
                    'message' => $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator').$errorsText
                ));
            }
        }
        
        // вернуть пустой ответ
        return new JsonResponse();
    }
    
}
