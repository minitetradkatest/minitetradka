<?php

namespace Nitra\MiniTetradkaBundle\Controller\Order;

use JMS\DiExtraBundle\Annotation as DI;
use Admingenerated\NitraMiniTetradkaBundle\BaseOrderController\ListController as BaseListController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nitra\MiniTetradkaBundle\Entity\Order;
use Nitra\MiniTetradkaBundle\Form\Type\Order\OrderCommentType;

/**
 * ListController
 */
class ListController extends BaseListController {

    /**
     * Получить EntityManager 
     * @DI\Inject("doctrine.orm.entity_manager") */
    private $em;

    /**
     * @DI\Inject("doctrine_mongodb.odm.document_manager")
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;

    /**
     * получить сервис изменения статусов заказа
     * @DI\Inject("nitra.minitetradka.order_status") */
    private $nitraOrderStatus;

    /**
     * получить сервис изменения статусов заказа
     * @DI\Inject("nitra.minitetradka.order_entry_status") */
    private $nitraOrderEntryStatus;

    /**
     * Отображение строки заказа
     * @Route("/{pk}-render-row", name="Nitra_MiniTetradkaBundle_Order_render_row", options={"expose"=true})
     * @ParamConverter("order", class="NitraMiniTetradkaBundle:Order", options={"id" = "pk"})
     * @Template("NitraMiniTetradkaBundle:OrderList:row.html.twig")
     */
    public function renderRowAction(Order $order) {

        // массив доступных цепочек по каждой позиции заказа
        $orderEntryAllowedStatuses = array();
        foreach ($order->getOrderEntry() as $orderEntry) {
            // получить доступные цепочки для позиции заказа
            $orderEntryAllowedStatuses[$orderEntry->getId()] = $this->nitraOrderEntryStatus->getAllowStatuses($orderEntry);
        }

        // вернуть массив данных передаваемых в шаблон 
        return array(
            // массив доступных цепочек для заказа
            'orderAllowedStatuses' => $this->nitraOrderStatus->getAllowStatuses($order),
            // массив доступных цепочек по каждой позиции заказа
            'orderEntryAllowedStatuses' => $orderEntryAllowedStatuses,
            // объект заказа
            'Order' => $order,
        );
    }

    /**
     * получить массив дополнительных параметров передаваемых в шаблон
     * @return array
     */
    public function getAdditionalRenderParameters() {
        // объект формы добавления комментариев
        $commentForm = $this->createForm(new OrderCommentType());

        // вернуть массив параметров передаваемых в шаблон
        return array(
            // форма добавления коментариев
            'commentForm' => $commentForm->createView(),
        );
    }

    /**
     * processFilters
     * выполнить фильтр 
     * @param Doctrine\ORM\QueryBuilder $query объект запроса
     */
    protected function processFilters($query) {

        // выполнить фильтр родителя
        parent::processFilters($query);

        // получить объект запроса
        $queryFilter = $this->get('admingenerator.queryfilter.doctrine');
        $queryFilter->setQuery($query);

        // получить объект фильтров
        $filterObject = $this->getFilters();
        //поиск по покупателю
        if (isset($filterObject['buyer']['id']) && !is_null($filterObject['buyer']['id'])) {
            $queryFilter->addEntityFilter('buyer', $filterObject['buyer']['id']);
        }

        // фильтр название продукта
        if (isset($filterObject['productName']) && $filterObject['productName']) {
            // Исключаем лишние пробелы
            $productName = preg_replace("/[[:blank:]]+/", ' ', $filterObject['productName']);

            // Если введено несколко фраз - разбиваем по пробелу
            // получить продукты
            $qb = $this->dm->createQueryBuilder('NitraMiniTetradkaBundle:Product')
                            ->hydrate(false)->select('_id');
            foreach (explode(' ', $productName) as $w) {
                $qb->addAnd(
                        $qb->expr()->field('fullNameForSearch')->equals(new \MongoRegex('/' . $w . '/i'))
                );
            }
            $productsIds = $qb->getQuery()->execute()->toArray();
            $ids = array_keys($productsIds);
            // нет продуктов удовлетвоярющих условию фильтра
            if (!$ids) {
                // добавить не существующий ID
                $ids[] = 'NOT_EXISTED_PROFUCT_ID';
            }
            // добавить в основной запрос поиск позиций по продуктам
            if ($ids) {
                $query->andWhere('(EXISTS (
                    SELECT oeProduct.id 
                    FROM NitraMiniTetradkaBundle:OrderEntry oeProduct 
                    WHERE oeProduct.order = q.id 
                        AND oeProduct.productId IN (:productIds) 
                        AND oeProduct.deletedAt IS NULL
                    ))');
                $query->setParameter('productIds', $ids);
            }
        }
            
        if (isset($filterObject['orderEntryStatus']) && !is_null($filterObject['orderEntryStatus'])) {

            $query->andWhere('(EXISTS (
                    SELECT oe 
                    FROM NitraMiniTetradkaBundle:OrderEntry oe 
                    WHERE oe.order = q.id 
                        AND oe.orderEntryStatus = :status 
                    ))');
            $query->setParameter('status', $filterObject['orderEntryStatus']);
        }
    }

}
