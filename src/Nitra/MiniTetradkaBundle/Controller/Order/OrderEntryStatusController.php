<?php
namespace Nitra\MiniTetradkaBundle\Controller\Order;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nitra\MiniTetradkaBundle\Entity\OrderEntry;

/**
 * OrderEntryStatusController
 * Статусы позиций заказов
 */
class OrderEntryStatusController extends Controller
{
    
    /**
     * Получить EntityManager 
     * @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * получить сервис изменения статусов заказа
     * @DI\Inject("nitra.minitetradka.order_entry_status")
     */
    private $nitraOrderEntryStatus;
    
    /**
     * отправить ответ
     * @param string $type    - тип отправляемого ответа
     * @param string $message - текст сообщения отправляемый сайту
     * @param array  $options - массив доп параметров передаваемых в ответ
     * @return JsonResponse
     */
    private function siteResponse($type, $message='', array $options = null) 
    {
        // результирующий массив ответа 
        $result = array();
        
        // проверить если указан массив доп параметров
        if ($options) {
            // добавить доп параметры в возвразаемый ответ
            $result = $options;
        }
        
        // добавить данные ответа тип и сообщение
        $result['type'] = $type; 
        $result['message'] = $message;
        
        // вернуть ответ на сайт
        return new JsonResponse($result);
    }
    
    /**
     * Изменение статуса позиции заказа
     * @Route("/set-status", name="Nitra_MiniTetradkaBundle_OrderEntry_set_status", options={"expose"=true})
     * @return mixed response
     */
    public function setStatusAction(Request $request)
    {
        
        // получить объект позиции заказа
        $orderEntry = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderEntry')
            ->find($request->get('orderEntryId', false));
        if (!$orderEntry) {
            // позиция заказа не найдена
            return $this->siteResponse('error', "Позиция заказа не найдена.");
        }
        
        // получить новый статус
        $toStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderEntryStatus')
            ->findOneBy(array('methodName' => $request->get('toStatus', false)));
        if (!$toStatus) {
            // устанавливаемый статус не найден
            return $this->siteResponse('error', "Новый статус позиции заказа не найден.");
        }
        
        // статус отображенный пользователю
        $displayedStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderEntryStatus')
            ->findOneBy(array('methodName' => $request->get('displayedStatus', false)));
        if (!$displayedStatus) {
            // отображенный пользователю статус не найден
            return $this->siteResponse('error', "Отображенный пользователю статус позиции заказа не найден.");
        }
        
        // Направление изменения статуса
        $direction = $orderEntry->getOrderEntryStatus()->getMethodName() . '_to_' . $toStatus->getMethodName();
        
        // массив параметров изменения статуса
        // по умолчанию берем весь массив request
        $statusParameters = $request->request->all();
        
//        // проверить цепочку 
//        // выполнить действия по конкретным цепочкам
//        switch($direction) {
//            
//            // обработка цепочек
//            case '...':
//            case '...':
//            case '...':
//                
//                // continue
//                break;
//        }
        
        // установить данные для изменения статуса
        $this->nitraOrderEntryStatus
            ->setOrderEntry($orderEntry)
            ->setToStatus($toStatus)
            ->setDisplayedStatus($displayedStatus)
            ->setParameters($statusParameters);
        
        // валидировать изменение статуса
        $errorMessage = $this->nitraOrderEntryStatus->validate();
        if ($errorMessage) {
            // ошибка валидации
            return $this->siteResponse('error', $errorMessage);
        }
        
        // выполнить изменение статуса
        $errorMessage = $this->nitraOrderEntryStatus->tryProcess();
        if ($errorMessage) {
            // ошибка валидации
            return $this->siteResponse('error', $errorMessage);
        }
        
        // сохранить
        $this->em->flush();
        
        // изменение статуса выполнено успешно
        return $this->siteResponse('success');
    }
    
    /**
     * colorbox история изменения статусов
     * @Route("/{pk}-status-history", name="Nitra_MiniTetradakBundle_OrderEntry_status_history")
     * @ParamConverter("orderEntry", class="NitraMiniTetradkaBundle:OrderEntry", options={"id" = "pk"})
     * @Template("NitraMiniTetradkaBundle:OrderActions:order_entry.status_history.html.twig")
     */
    public function statusHistoryAction(OrderEntry $orderEntry, Request $request)
    {
        // вернуть массив данных передаваемых в шаблон 
        return array(
            'orderEntry' => $orderEntry,
        );
    }
    
}