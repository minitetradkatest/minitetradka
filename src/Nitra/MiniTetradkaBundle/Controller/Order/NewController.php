<?php
namespace Nitra\MiniTetradkaBundle\Controller\Order;

use Admingenerated\NitraMiniTetradkaBundle\BaseOrderController\NewController as BaseNewController;
use Nitra\MiniTetradkaBundle\Entity\Order;
use Nitra\MiniTetradkaBundle\Traits\OrderFormAdditionalRenderParameters;

/**
 * NewController
 */
class NewController extends BaseNewController
{
    
    // trait дополнительные параметры формы заказа
    use OrderFormAdditionalRenderParameters;
    
    /**
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * @param Order $Order object
     * @return array
     **/
    protected function getAdditionalRenderParameters(Order $Order)
    {
        // получить обзий массив параметров 
        // формы заказа
        return $this->getOrderFormAdditionalRenderParameters($this->getNewType(), $Order);
    }
    
}
