<?php
namespace Nitra\MiniTetradkaBundle\Controller\Order;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nitra\MiniTetradkaBundle\Entity\Order;

/**
 * OrderStatusController
 * Статусы позиций заказов
 */
class OrderStatusController extends Controller
{
    
    /**
     * Получить EntityManager 
     * @DI\Inject("doctrine.orm.entity_manager") */
    private $em;
    
    /**
     * получить сервис изменения статусов заказа
     * @DI\Inject("nitra.minitetradka.order_status")
     */
    private $nitraOrderStatus;
    
    /**
     * отправить ответ
     * @param string $type    - тип отправляемого ответа
     * @param string $message - текст сообщения отправляемый сайту
     * @param array  $options - массив доп параметров передаваемых в ответ
     * @return JsonResponse
     */
    private function siteResponse($type, $message='', array $options = null) 
    {
        // результирующий массив ответа 
        $result = array();
        
        // проверить если указан массив доп параметров
        if ($options) {
            // добавить доп параметры в возвразаемый ответ
            $result = $options;
        }
        
        // добавить данные ответа тип и сообщение
        $result['type'] = $type; 
        $result['message'] = $message;
        
        // вернуть ответ на сайт
        return new JsonResponse($result);
    }
    
    /**
     * Изменение статуса заказа
     * @Route("/set-status", name="Nitra_MiniTetradkaBundle_Order_set_status", options={"expose"=true})
     * @return mixed response
     */
    public function setStatusAction(Request $request)
    {
        
        // получить объект позиции заказа
        $order = $this->em
            ->getRepository('NitraMiniTetradkaBundle:Order')
            ->find($request->get('orderId', false));
        if (!$order) {
            // заказ не найдена
            return $this->siteResponse('error', "Заказ не найден.");
        }
        
        // получить новый статус
        $toStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderStatus')
            ->findOneBy(array('methodName' => $request->get('toStatus', false)));
        if (!$toStatus) {
            // устанавливаемый статус не найден
            return $this->siteResponse('error', "Новый статус заказа не найден.");
        }
        
        // статус отображенный пользователю
        $displayedStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderStatus')
            ->findOneBy(array('methodName' => $request->get('displayedStatus', false)));
        if (!$displayedStatus) {
            // отображенный пользователю статус не найден
            return $this->siteResponse('error', "Отображенный пользователю статус заказа не найден.");
        }
        
        // Направление изменения статуса
        $direction = $order->getOrderStatus()->getMethodName() . '_to_' . $toStatus->getMethodName();
        
        // массив параметров изменения статуса
        // по умолчанию берем весь массив request
        $statusParameters = $request->request->all();
        
//        // проверить цепочку 
//        // выполнить действия по конкретным цепочкам
//        switch($direction) {
//            
//            // обработка цепочек
//            case '...':
//            case '...':
//            case '...':
//                
//                // continue
//                break;
//        }
        
        // установить данные для изменения статуса
        $this->nitraOrderStatus
            ->setOrder($order)
            ->setToStatus($toStatus)
            ->setDisplayedStatus($displayedStatus)
            ->setParameters($statusParameters);
        
        // валидировать изменение статуса
        $errorMessage = $this->nitraOrderStatus->validate();
        if ($errorMessage) {
            // ошибка валидации
            return $this->siteResponse('error', $errorMessage);
        }
        
        // выполнить изменение статуса
        $errorMessage = $this->nitraOrderStatus->tryProcess();
        if ($errorMessage) {
            // ошибка валидации
            return $this->siteResponse('error', $errorMessage);
        }
        
        // сохранить
        $this->em->flush();
        
        // изменение статуса выполнено успешно
        return $this->siteResponse('success');
    }

    /**
     * colorbox история изменения статусов
     * @Route("/{pk}-status-history", name="Nitra_MiniTetradakBundle_Order_status_history")
     * @ParamConverter("order", class="NitraMiniTetradkaBundle:Order", options={"id" = "pk"})
     * @Template("NitraMiniTetradkaBundle:OrderActions:order.status_history.html.twig")
     */
    public function statusHistoryAction(Order $order, Request $request)
    {
        // вернуть массив данных передаваемых в шаблон 
        return array(
            'order' => $order,
        );
    }
    
}