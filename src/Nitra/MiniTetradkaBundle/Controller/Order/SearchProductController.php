<?php
namespace Nitra\MiniTetradkaBundle\Controller\Order;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nitra\MiniTetradkaBundle\Form\Type\Order\SearchProductType;

/**
 * SearchProductController
 * Поиск продуктов
 */
class SearchProductController extends Controller
{
    
    /**
     * Получить DocumentManager 
     * @DI\Inject("doctrine_mongodb.odm.document_manager") 
     */
    private $dm;
    
    /**
     * поиск товаров 
     * @Route("/", name="Nitra_MiniTetradkaBundle_search_product")
     * @return mixed - если есть результаты для отображения 
     *                 то вернуть шаблон который будет отображен
     *                 иначе вернуть пустой контент
     */
    public function searchProductAction(Request $request)
    {
        // форма поиска продуктов
        $searchProduct = $this->createForm(new SearchProductType());
        
        // данные формы поиска
        $postData = $request->get($searchProduct->getName());
        
        // строка поиска
        $searchValue = trim(preg_replace("/[[:blank:]]+/", ' ', $postData['name']));
        
        // кол-во позиций отображаемых на одной странице
        $countOnPage = 20;
        
        // отображаемая страница
        $page = isset($postData['page']) && $postData['page'] 
            ? $postData['page'] 
            : 0;
        
        // запрос получения продуктов
        $productsQuery = $this->dm
            ->getRepository('NitraMiniTetradkaBundle:Product')
            ->createQueryBuilder('p')
            ->skip($page * $countOnPage) // Смещение
            ->limit($countOnPage) // Отображаем только 20 строк
//            ->field('isActive')->equals(true)
            ->field('status')->notEqual('deleted')
            ->sort('storePrice.' . $this->container->getParameter('nitra_minitetradka_store_id'), 'asc');
        
        // добавить строку поиска в запрос
        if ($searchValue) {
            
            // Если введено несколко слов - разбиваем по пробелу
            if (strstr($searchValue, ' ')) {
                // Поиск с учетом регистра всех вхождений
                $productsQuery->field('fullNameForSearch')
                    ->equals(new \MongoRegex('/(?=.*' . str_replace(' ', ')(?=.*', $searchValue) . ')/i'));
                
            } else {
                // Поиск по одному слову
                $productsQuery->field('fullNameForSearch')
                    ->equals(new \MongoRegex("/.*$searchValue.*/i"));
            }
        }
        
        // поиск по категории
        if (isset($postData['categoryId']) && $postData['categoryId']) {
            // добавить категорию  в запрос
            $productsQuery->field('category.$id')
                ->equals(new \MongoId($postData['categoryId']));
        }
        
        // получить продукты
        $productsFound = $productsQuery
            ->getQuery()
            ->execute();
        
        // сформировать массив найденных продуктов
        $products = array();
        foreach($productsFound as $product) {
            // запомнить продукт
            $products[$product->getId()] = array(
                'productId' => $product->getId(),
                'article' => $product->getArticle(),
                'productName' => (string)$product,
                'priceOut' => $product->getStorePriceOut($this->container->getParameter('nitra_minitetradka_store_id')),
                'isActive' => $product->getIsActive(),
                // подсветка продукта
                'colored' => false,
            );
        }
        
        // если есть продукты для отображения 
        if ($products) {
            
            // отображаемый шалон с результатами поиска
            $renderTemplate = ($page == 0)
                ? 'searchProduct.html.twig'
                : 'searchProductTr.html.twig';
            
            // вернуть шаблон отображения
            return $this->render('NitraMiniTetradkaBundle:OrderEdit:'.$renderTemplate, array(
                'products'  => $products,
                'page'      => $page,
            ));
        }
        
        // иначе вернуть пустой контент
        return new Response();        
    }
    
}

