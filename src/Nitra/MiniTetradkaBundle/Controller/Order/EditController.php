<?php
namespace Nitra\MiniTetradkaBundle\Controller\Order;

use Admingenerated\NitraMiniTetradkaBundle\BaseOrderController\EditController as BaseEditController;
use Nitra\MiniTetradkaBundle\Entity\Order;
use Nitra\MiniTetradkaBundle\Traits\OrderFormAdditionalRenderParameters;

/**
 * EditController
 */
class EditController extends BaseEditController
{
    
    // trait дополнительные параметры формы заказа
    use OrderFormAdditionalRenderParameters;
    
    /**
     * получить массив дополнительных параметров передаваемых в шаблон для отображения
     * @param Order $Order object
     * @return array
     **/
    protected function getAdditionalRenderParameters(Order $Order)
    {
        // получить обзий массив параметров 
        // формы заказа
        return $this->getOrderFormAdditionalRenderParameters($this->getEditType(), $Order);
    }
        
}
