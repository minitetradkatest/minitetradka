<?php
namespace Nitra\MiniTetradkaBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NitraMiniTetradkaExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        
        // добавить статус по умолчанию
        $container->setParameter('nitra_minitetradka_default_order_status', $config['default_order_status']);
        $container->setParameter('nitra_minitetradka_default_order_entry_status', $config['default_order_entry_status']);
        
        // добавить ID магазина по умолчанию 
        $container->setParameter('nitra_minitetradka_store_id', '');
        
        // добавить валюту по умолчанию
        $container->setParameter('nitra_minitetradka_currency_code', $config['currency_code']);
        $container->setParameter('nitra_minitetradka_currency_symbol', $config['currency_symbol']);
    }
}
