<?php

namespace Nitra\MiniTetradkaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nitra_minitetradka')->addDefaultsIfNotSet();
        
        // добавить настройки статусов
        $this->addDefaultStatuses($rootNode);
        
        // добавить настройки вылюты
        $this->addDefaultCurrency($rootNode);
        
        // вернуть дерево настроек
        return $treeBuilder;
    }
    
    
    /**
     * Добавить настройки стаусов по умолчанию 
     * add статус заказа
     * add статус позиции заказа по умолчанию 
     * @param ArrayNodeDefinition $treeBuilder
     */
    private function addDefaultStatuses(ArrayNodeDefinition $treeBuilder)
    {
        
        $treeBuilder
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('default_order_status')->defaultValue('waiting')->end()
                ->scalarNode('default_order_entry_status')->defaultValue('waiting')->end()
            ->end();
    }
    
    /**
     * Добавить настройки валюты по умолчанию 
     * add статус заказа
     * add статус позиции заказа по умолчанию 
     * @param ArrayNodeDefinition $treeBuilder
     */
    private function addDefaultCurrency(ArrayNodeDefinition $treeBuilder)
    {
        $treeBuilder
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('currency_code')->defaultValue('uah')->end()
                ->scalarNode('currency_symbol')->defaultValue('грн.')->end()
            ->end();
    }
    
}
