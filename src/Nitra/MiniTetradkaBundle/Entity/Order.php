<?php
namespace Nitra\MiniTetradkaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Order
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="Nitra\MiniTetradkaBundle\Repository\OrderRepository")
 */
class Order
{
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool $isDirect - адрессная ли доставка?
     * @ORM\Column(type="boolean")
     */
    private $isDirect;
    
    /**
     * @var string $address - адресс доставки
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $address;
    
    /**
     * @var bool $isCod - Наложенный платёж
     * @ORM\Column(type="boolean" )
     */
    private $isCod;
    
    /**
     * @ORM\ManyToOne(targetEntity="Buyer", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Type(type="Nitra\MiniTetradkaBundle\Entity\Buyer")
     * @Assert\NotBlank(message="Не указан покупатель.")
     */
    private $buyer;
    
    /**
     * @ORM\OneToMany(targetEntity="OrderEntry", mappedBy="order")
     */
    private $orderEntry;

    /**
     * @ORM\ManyToOne(targetEntity="OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан статус заказа.")
     * @Assert\Type(type="Nitra\MiniTetradkaBundle\Entity\OrderStatus")
     */
    private $orderStatus;
    
    /**
     * @ORM\OneToMany(targetEntity="OrderStatusHistory", mappedBy="order")
     */
    private $orderStatusHistory;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Nitra\GeoBundle\Entity\Model\CityInterface")
     * @ORM\JoinColumn(nullable=true)
     */
    private $city;
    
    /**
     * @var string $comment
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;
    
    /**
     * @var Date - дата доставки
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    private $deliveryDate;    
    
    /**
     * @var integer $deliveryCost - стоимость доставки
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Не указана стоимость доставки")
     * @Assert\Range(min=0, max=999999)
     */
    private $deliveryCost;
    
    /**
     * Дата завершения заказа
     * @var DateTime $completedAt
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $completedAt;
    
    /**
     * Get amount
     * @return float
     */
    public function getAmount()
    {
        $summ = 0;
        foreach ($this->getOrderEntry() as $oe) {
            if ($oe->getOrderEntryStatus()->getMethodName()=='completed') {
                $summ += $oe->getPriceOut()* $oe->getQuantity();
            }
        }

        return $summ;
    }
    
    /**
     * Get profit
     * @return float
     */
    public function getProfit()
    {
        $summ = 0;
        foreach ($this->getOrderEntry() as $oe) {
            if (!in_array($oe->getOrderEntryStatus()->getMethodName(), array('canceled'))) {
                $summ += ($oe->getPriceOut() - $oe->getPriceIn()) * $oe->getQuantity();
            }
        }
        
        return $summ;
    }
    
    /**
     * Get total
     * @return float
     */
    public function getTotal()
    {
        $summ = 0;
        foreach ($this->getOrderEntry() as $oe) {
            if ($oe->getOrderEntryStatus() &&
                !in_array($oe->getOrderEntryStatus()->getMethodName(), array('canceled'))) {
                $summ += $oe->getPriceOut() * $oe->getQuantity();
            }
        }

        return $summ;
    }    
    
    /**
     * this object to string
     * @return string
     */
    public function __toString()
    {
        return (string)$this->id;
    }
    
    /**
     * Валидатор проверка кол-ва позиций заказов
     * @Assert\True(message = "Нет ни одной товарной позиции для заказа.")
     */
    public function isOrderEntriesExists()
    {
        // проверить количество позиций
        if (count($this->orderEntry)) {
            return true;
        }
        // нет позиций
        return false;
    }
    
    /**
     * Add autoincrement comment in order
     * 
     * @param string $comment
     * @param string $username
     * @return self
     */
    public function addComment($comment, $username)
    {
        $comment = trim($comment);
        $username = trim($username);
        
        // добавить комментарий
        if ($comment /*&& $username*/) {
            
            $formatter = \IntlDateFormatter::create(
                \Locale::getDefault(), 
                \IntlDateFormatter::MEDIUM, 
                \IntlDateFormatter::NONE
            );
            
            $formatter->setPattern("d MMM H:mm");
            $nowDateString = $formatter->format(new \DateTime());
            $commentText = '<div class="comment_item"><b>' . $username . '</b> <small>(' . $nowDateString . ')</small>: ' . $comment . '</div>';
            $this->setComment($this->getComment() . $commentText);
            
        }
        
        return $this;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        // стоимость доставки
        $this->deliveryCost = 0;
        
        // адрессная ли доставка
        $this->isDirect = false;
        
        // Наложенный платёж
        $this->isCod = false;
        
        // позиции заказа 
        $this->orderEntry = new \Doctrine\Common\Collections\ArrayCollection();
        
        // история статусов
        $this->orderStatusHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isDirect
     *
     * @param boolean $isDirect
     * @return Order
     */
    public function setIsDirect($isDirect)
    {
        $this->isDirect = $isDirect;
    
        return $this;
    }

    /**
     * Get isDirect
     *
     * @return boolean 
     */
    public function getIsDirect()
    {
        return $this->isDirect;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Order
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set isCod
     *
     * @param boolean $isCod
     * @return Order
     */
    public function setIsCod($isCod)
    {
        $this->isCod = $isCod;
    
        return $this;
    }

    /**
     * Get isCod
     *
     * @return boolean 
     */
    public function getIsCod()
    {
        return $this->isCod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Order
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set buyer
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\Buyer $buyer
     * @return Order
     */
    public function setBuyer(\Nitra\MiniTetradkaBundle\Entity\Buyer $buyer)
    {
        $this->buyer = $buyer;
    
        return $this;
    }

    /**
     * Get buyer
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\Buyer 
     */
    public function getBuyer()
    {
        return $this->buyer;
    }
    
    /**
     * Add orderEntry
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntry $orderEntry
     * @return Order
     */
    public function addOrderEntry(\Nitra\MiniTetradkaBundle\Entity\OrderEntry $orderEntry)
    {
        $this->orderEntry[] = $orderEntry;
    
        return $this;
    }

    /**
     * Remove orderEntry
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntry $orderEntry
     */
    public function removeOrderEntry(\Nitra\MiniTetradkaBundle\Entity\OrderEntry $orderEntry)
    {
        $this->orderEntry->removeElement($orderEntry);
    }

    /**
     * Get orderEntry
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderEntry()
    {
        return $this->orderEntry;
    }
    
    /**
     * Set orderStatus
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderStatus $orderStatus
     * @return Order
     */
    public function setOrderStatus(\Nitra\MiniTetradkaBundle\Entity\OrderStatus $orderStatus)
    {
        // заказ завершен 
        if ($orderStatus->getMethodName() == 'completed') {
            // установить дату завершения заказа
            $this->setCompletedAt(new \DateTime());
        } else {
            // сбросить дату завершения заказа
            $this->setCompletedAt(null);
        }
        
        // обновить статус 
        $this->orderStatus = $orderStatus;
        
        // вернуть сущность 
        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\OrderStatus 
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Add orderStatusHistory
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderStatusHistory $orderStatusHistory
     * @return Order
     */
    public function addOrderStatusHistory(\Nitra\MiniTetradkaBundle\Entity\OrderStatusHistory $orderStatusHistory)
    {
        $this->orderStatusHistory[] = $orderStatusHistory;

        return $this;
    }

    /**
     * Remove orderStatusHistory
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderStatusHistory $orderStatusHistory
     */
    public function removeOrderStatusHistory(\Nitra\MiniTetradkaBundle\Entity\OrderStatusHistory $orderStatusHistory)
    {
        $this->orderStatusHistory->removeElement($orderStatusHistory);
    }

    /**
     * Get orderStatusHistory
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderStatusHistory()
    {
        return $this->orderStatusHistory;
    }
    
    /**
     * Set city
     *
     * @param \Nitra\GeoBundle\Entity\City $city
     * @return Order
     */
    public function setCity(\Nitra\GeoBundle\Entity\City $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \Nitra\GeoBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }
    
    
    /**
     * Set delivery_date
     *
     * @param \DateTime $deliveryDate
     * @return Order
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    
        return $this;
    }

    /**
     * Get delivery_date
     *
     * @return \DateTime 
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }
    
    /**
     * Set $deliveryCost
     * @param integer $deliveryCost
     * @return Order
     */
    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;
    
        return $this;
    }

    /**
     * Get $deliveryCost
     * @return integer 
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }
    
    /**
     * Set completedAt
     *
     * @param \DateTime $completedAt
     * @return Order
     */
    public function setCompletedAt($completedAt)
    {
        $this->completedAt = $completedAt;

        return $this;
    }

    /**
     * Get completedAt
     *
     * @return \DateTime 
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }
    
}
