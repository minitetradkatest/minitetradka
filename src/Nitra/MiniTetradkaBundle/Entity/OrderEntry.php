<?php
namespace Nitra\MiniTetradkaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * OrderEntry
 * @ORM\Entity(repositoryClass="Nitra\MiniTetradkaBundle\Repository\OrderEntryRepository")
 */
class OrderEntry
{
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer $quantity
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Не указано количество товара")
     * @Assert\Range(min=1, max=999999)
     */
    private $quantity;

    /**
     * @var float $price_in
     * @ORM\Column(name="price_in", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(message="Не указана цена входа")
     * @Assert\Range(min=0, max=999999.99)
     */
    private $priceIn;
    
    /**
     * Валюта
     * @ORM\ManyToOne(targetEntity="Nitra\MiniTetradkaBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Не указана валюта")
     */
    private $currency;

    /**
     * @var float $price_out
     * @ORM\Column(name="price_out", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(message="Не указана цена выхода")
     * @Assert\Range(min=0, max=999999.99)
     */
    private $priceOut;

    /**
     * @var float $price_recommended
     * Цена по которой товар был предложен покупателю в самый первый раз
     * @ORM\Column(name="price_recommended", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(message="Не указана рекомендуемая цена")
     * @Assert\Range(min=0, max=999999.99)
     */
    private $priceRecommended;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderEntry")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @Assert\NotBlank(message="Не указан заказ для позиции заказа")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntryStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан статус товарной позиции")
     */
    private $orderEntryStatus;
    
    /**
     * @ORM\OneToMany(targetEntity="OrderEntryStatusHistory", mappedBy="orderEntry")
     */
    private $orderEntryStatusHistory;

    /**
     * @var string $productId
     * @ORM\Column(name="product_id", type="string", length=24)
     * @Assert\NotBlank(message="Не указан продукт товарной позиции")
     * @Assert\Length(max=24)
     */
    private $productId;

    /**
     * @var \Nitra\MiniTetradkaBundle\Document\Product
     */
    protected $product;
    
    /**
     * @var string $serialNumber
     * @ORM\Column(name="serial_number", type="string", length=64, nullable=true)
     * @Assert\Length(max="64")
     */
    private $serialNumber;

    /**
     * @var integer $warrantyPeriod
     * @ORM\Column(name="warranty_period", type="integer", nullable=true)
     * @Assert\Range(min=0, max=999999)
     */
    private $warrantyPeriod;
    
    /**
     * скидка в процентах (необязательный параметр при формировании заказа с сайта)
     * @var string $discountPercent
     * @ORM\Column(name="discount_percent", type="decimal", precision=3, scale=3, nullable=true) 
     * @Assert\Range(min=0, max=999.999)
     */
    private $discountPercent;
    
    /**
     * object to string 
     * @return string
     */
    public function __toString()
    {
        
        // проверить кол-во продукта больше 1
        if ($this->getQuantity() > 1 ) {
            // вернуть навзание позиции с учетом кол-ва
            return $this->getQuantity() . " x " . $this->getEntryName();
        }
        
        // вернуть навзание позиции
        return $this->getEntryName();
    }
    
    /**
     * Получить навзание товарной позиции
     * @return string
     */
    public function getEntryName()
    {
        
        // возвразаемое имя 
        $entryName = '';
        
        // проверить связь с объектом продукта
        if ($this->getProduct()) {
            $entryName = (string)$this->getProduct();
        }
        
        // вернуть string 
        return $entryName;
    }
    
    /**
     * Set product
     *
     * @param \Nitra\MiniTetradkaBundle\Document\Product $product
     */
    public function setProduct($product)
    {
        $this->productId = $product->getId();
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * получить сумму по позиции заказа с учетом входа, выхода и количества.
     */
    public function getAmount()
    {
        return ($this->priceOut - $this->priceIn) * $this->quantity;
    }
    
    /**
     * получить total
     */
    public function getTotal()
    {
        return $this->quantity * $this->priceOut;
    }
    
    /**
     * Отмененная позиция
     * @return boolean
     */
    public function isCanceled() 
    {
        // получить статус позиции заказа
        $orderEntryStatus = $this->getOrderEntryStatus();
        // массив отмененных статусов
        $canceledStatuses = array('canceled', 'return');
        if ($orderEntryStatus &&
            in_array($orderEntryStatus->getMethodName(), $canceledStatuses)
        ) {
            // позиция отменена
            return true;
        }
        
        // позиция не отменена
        return false;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        // история статусов
        $this->orderEntryStatusHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return OrderEntry
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set priceIn
     *
     * @param string $priceIn
     * @return OrderEntry
     */
    public function setPriceIn($priceIn)
    {
        $this->priceIn = $priceIn;
    
        return $this;
    }

    /**
     * Get priceIn
     *
     * @return string 
     */
    public function getPriceIn()
    {
        return $this->priceIn;
    }

    /**
     * Set priceOut
     *
     * @param string $priceOut
     * @return OrderEntry
     */
    public function setPriceOut($priceOut)
    {
        $this->priceOut = $priceOut;
    
        return $this;
    }

    /**
     * Get priceOut
     *
     * @return string 
     */
    public function getPriceOut()
    {
        return $this->priceOut;
    }

    /**
     * Set priceRecommended
     *
     * @param string $priceRecommended
     * @return OrderEntry
     */
    public function setPriceRecommended($priceRecommended)
    {
        $this->priceRecommended = $priceRecommended;
    
        return $this;
    }

    /**
     * Get priceRecommended
     *
     * @return string 
     */
    public function getPriceRecommended()
    {
        return $this->priceRecommended;
    }
    
    /**
     * Set currency
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\Currency $currency
     * @return OrderEntry
     */
    public function setCurrency(\Nitra\MiniTetradkaBundle\Entity\Currency $currency)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }    
    
    /**
     * Set productId
     *
     * @param string $productId
     * @return OrderEntry
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    
        return $this;
    }

    /**
     * Get productId
     *
     * @return string 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     * @return OrderEntry
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;
    
        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string 
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set warrantyPeriod
     *
     * @param integer $warrantyPeriod
     * @return OrderEntry
     */
    public function setWarrantyPeriod($warrantyPeriod)
    {
        $this->warrantyPeriod = $warrantyPeriod;
    
        return $this;
    }

    /**
     * Get warrantyPeriod
     *
     * @return integer 
     */
    public function getWarrantyPeriod()
    {
        return $this->warrantyPeriod;
    }

    /**
     * Set discountPercent
     *
     * @param string $discountPercent
     * @return OrderEntry
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;
    
        return $this;
    }

    /**
     * Get discountPercent
     *
     * @return string 
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * Set order
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\Order $order
     * @return OrderEntry
     */
    public function setOrder(\Nitra\MiniTetradkaBundle\Entity\Order $order = null)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set orderEntryStatus
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $orderEntryStatus
     * @return OrderEntry
     */
    public function setOrderEntryStatus(\Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $orderEntryStatus)
    {
        $this->orderEntryStatus = $orderEntryStatus;
    
        return $this;
    }

    /**
     * Get orderEntryStatus
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus 
     */
    public function getOrderEntryStatus()
    {
        return $this->orderEntryStatus;
    }
    
    /**
     * Add orderEntryStatusHistory
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory
     * @return OrderEntry
     */
    public function addOrderEntryStatusHistory(\Nitra\MiniTetradkaBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory)
    {
        $this->orderEntryStatusHistory[] = $orderEntryStatusHistory;

        return $this;
    }

    /**
     * Remove orderEntryStatusHistory
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory
     */
    public function removeOrderEntryStatusHistory(\Nitra\MiniTetradkaBundle\Entity\OrderEntryStatusHistory $orderEntryStatusHistory)
    {
        $this->orderEntryStatusHistory->removeElement($orderEntryStatusHistory);
    }

    /**
     * Get orderEntryStatusHistory
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderEntryStatusHistory()
    {
        return $this->orderEntryStatusHistory;
    }
    
}
