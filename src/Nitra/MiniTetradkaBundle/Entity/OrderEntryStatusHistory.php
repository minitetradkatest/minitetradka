<?php
namespace Nitra\MiniTetradkaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * OrderEntryStatusHistory
 * @ORM\Entity
 */
class OrderEntryStatusHistory
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntry", inversedBy="orderStatusHistory")
     * @ORM\JoinColumn()
     * @Assert\NotBlank(message="Не указана позиция заказа.")
     */
    private $orderEntry;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntryStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан начальный статус.")
     */
    private $fromStatus;
    
    /**
     * @ORM\ManyToOne(targetEntity="OrderEntryStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан конечный статус.")
     */
    private $toStatus;
    
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderEntry
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntry $orderEntry
     * @return OrderEntryStatusHistory
     */
    public function setOrderEntry(\Nitra\MiniTetradkaBundle\Entity\OrderEntry $orderEntry = null)
    {
        $this->orderEntry = $orderEntry;

        return $this;
    }

    /**
     * Get orderEntry
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\OrderEntry 
     */
    public function getOrderEntry()
    {
        return $this->orderEntry;
    }

    /**
     * Set fromStatus
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $fromStatus
     * @return OrderEntryStatusHistory
     */
    public function setFromStatus(\Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $fromStatus)
    {
        $this->fromStatus = $fromStatus;

        return $this;
    }

    /**
     * Get fromStatus
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus 
     */
    public function getFromStatus()
    {
        return $this->fromStatus;
    }

    /**
     * Set toStatus
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $toStatus
     * @return OrderEntryStatusHistory
     */
    public function setToStatus(\Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $toStatus)
    {
        $this->toStatus = $toStatus;

        return $this;
    }

    /**
     * Get toStatus
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus 
     */
    public function getToStatus()
    {
        return $this->toStatus;
    }
    
}
