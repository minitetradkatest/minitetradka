<?php
namespace Nitra\MiniTetradkaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Buyer
 * @ORM\Entity(repositoryClass="Nitra\MiniTetradkaBundle\Repository\BuyerRepository")
 * @UniqueEntity(fields="phone", message="Клиент с таким номером телефона уже существует")
 */
class Buyer
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Не указано имя продавца")
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @var string $phone
     *
     * @ORM\Column(type="string", length=15)
     * 
     * @Assert\NotBlank
     * @Assert\Length(min="10", max="15")
     * @Assert\Regex(pattern="/[\(\)\+\-0-9]{10,15}$/", message="Телефон указан не верно")
     */
    private $phone;

    /**
     * @var string $comment
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $comment;
    
    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="buyer")
     */
    private $orders;
    
    /**
     * this object to string
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }
    
    /**
     * Получить номер телефона для отображения
     * @return string 
     */
    public function getPhoneDisplay()
    {
        // в зависимости от кол-ва цифр в номере телефона 
        // меняем формат отоюражения
        switch(strlen($this->phone)) {
            
            // россия
            case 11:
                return preg_replace("/([0-9{1})([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{2})/", "$1 ($2) $3-$4-$5", $this->phone);
            // украина 
            case 12:
                return preg_replace("/([0-9{1})([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{2})/", "$1 ($2) $3-$4-$5", $this->phone);
                break;
        }
        
        // вернуть телефор без формата
        return $this->phone;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Buyer
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Buyer
     */
    public function setPhone($phone)
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Buyer
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add orders
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\Order $orders
     * @return Buyer
     */
    public function addOrder(\Nitra\MiniTetradkaBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;
    
        return $this;
    }

    /**
     * Remove orders
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\Order $orders
     */
    public function removeOrder(\Nitra\MiniTetradkaBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }
}
