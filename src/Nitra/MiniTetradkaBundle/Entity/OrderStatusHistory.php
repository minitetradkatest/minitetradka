<?php
namespace Nitra\MiniTetradkaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * OrderStatusHistory
 * @ORM\Entity
 */
class OrderStatusHistory
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderStatusHistory")
     * @ORM\JoinColumn()
     * @Assert\NotBlank(message="Не указан заказ.")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан начальный статус.")
     */
    private $fromStatus;
    
    /**
     * @ORM\ManyToOne(targetEntity="OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан конечный статус.")
     */
    private $toStatus;
    
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set order
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\Order $order
     * @return OrderStatusHistory
     */
    public function setOrder(\Nitra\MiniTetradkaBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set fromStatus
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderStatus $fromStatus
     * @return OrderStatusHistory
     */
    public function setFromStatus(\Nitra\MiniTetradkaBundle\Entity\OrderStatus $fromStatus)
    {
        $this->fromStatus = $fromStatus;

        return $this;
    }

    /**
     * Get fromStatus
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\OrderStatus 
     */
    public function getFromStatus()
    {
        return $this->fromStatus;
    }

    /**
     * Set toStatus
     *
     * @param \Nitra\MiniTetradkaBundle\Entity\OrderStatus $toStatus
     * @return OrderStatusHistory
     */
    public function setToStatus(\Nitra\MiniTetradkaBundle\Entity\OrderStatus $toStatus)
    {
        $this->toStatus = $toStatus;

        return $this;
    }

    /**
     * Get toStatus
     *
     * @return \Nitra\MiniTetradkaBundle\Entity\OrderStatus 
     */
    public function getToStatus()
    {
        return $this->toStatus;
    }
    
}
