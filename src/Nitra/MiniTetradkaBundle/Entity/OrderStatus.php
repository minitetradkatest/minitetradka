<?php
namespace Nitra\MiniTetradkaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * OrderStatus
 * @ORM\Entity()
 * @UniqueEntity(fields="methodName", message="Статус заказа с таким ключем уже существует")
 */
class OrderStatus
{
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Не указано название статуса заказа.")
     * @Assert\Length(max=255)
     */
    private $name;

    /**
     * @var string $method_name
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Не указан ключ статуса заказа.")
     * @Assert\Length(max=255)
     */
    private $methodName;

    /**
     * @var integer $sort_order
     * @ORM\Column(type="integer")
     * @Assert\Range(min=0, max=999999)
     */
    private $sortOrder;
    
    /**
     * @var string $name
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $cssClass;
    
    /**
     * this object to string
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return OrderStatus
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set methodName
     *
     * @param string $methodName
     * @return OrderStatus
     */
    public function setMethodName($methodName)
    {
        $this->methodName = $methodName;
    
        return $this;
    }

    /**
     * Get methodName
     *
     * @return string 
     */
    public function getMethodName()
    {
        return $this->methodName;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return OrderStatus
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
    
    /**
     * Set cssClass
     *
     * @param string $cssClass
     * @return OrderStatus
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;

        return $this;
    }

    /**
     * Get cssClass
     *
     * @return string 
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }
    
}
