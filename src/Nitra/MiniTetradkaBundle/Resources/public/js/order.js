/**
 * объект работы с заказами
 **/
var order = {
    
    /**
     * ajax запрос 
     * @param string url    - url ajax запроса
     * @param object params - параметры 
     * @return json  result - результат 
     **/
    ajax: function (actionUrl, params) {
        
        // объект параметров отправки ajax запроса
        if (typeof(params) == 'undefined') {
            params = {};
        }
        
        // результирующие данные отправки запроса
        var result;
        
        // отправить ajax запрос
        $.ajax({
            url: actionUrl,
            type: (typeof(params.type) != 'undefined') ? params.type : 'POST',
            data: (typeof(params.data) != 'undefined') ? params.data : {},
            dataType: (typeof(params.dataType) != 'undefined') ? params.dataType : 'json', // если params.dataType = '' то Intelligent Guess (xml, json, script, or html)
            async: (typeof(params.async) != 'undefined') ? params.async : false,
            success: function(fromServer){
                // ответ от сервера получен успешно
                if (fromServer) {
                    result = fromServer;
                }
            }
        });
        // венруть объект
        return result;
    },
    
    /**
     * обновление строки TR данных заказа
     * @param integer orderId - ID идентификатор заказа
     **/
    renderRow: function (orderId) {
        
        // ID идентификатор заказа
        var orderId = parseInt(orderId);
        
        // url контроллер получение html данных строки заказа
        var actionUrl = Routing.generate('Nitra_MiniTetradkaBundle_Order_render_row', { pk: orderId }, true);        
        
        // отправить ajax запрос на получаение данных по строке заказа
        var fromServer = order.ajax(actionUrl, {
            dataType: 'html'
        });
        
        // обновить html контент строки заказа
        $('#tr_order_id'+orderId).replaceWith(fromServer);
        
        // Colorbox link
        $('#tr_order_id'+orderId).on('click' , '.colorbox',function(){
            $(this).colorbox();
        })
        
        // спрятать popup
        $('.popup').removeClass('active');
        
        // Popup box
        $('#tr_order_id'+orderId+' .popup').each(function(){
            $(this).html('<div class="popup_content">' + $(this).html() + '</div>');
            $(this).prepend('<div class="popup_arrow"></div>');
            $(this).addClass('popup_ready');
        })
        $('#tr_order_id'+orderId+' .popup .popup_arrow').click(function(){
            $(this).parent('div.popup').toggleClass('popup_active');
            $('.popup').not($(this).parent('div.popup')).removeClass('popup_active');
        });
        
    },
    
    /**
     * изменить статус по заказу
     * @param integer orderId           - ID идентификатор заказа
     * @param string  toStatus          - название нового метода статуса заказа
     * @param string  statusName        - текстовое название нового статуса заказа
     * @param string  displayedStatus   - название метода статуса отображенного пользователю
     **/
    setStatus: function(orderId, toStatus, statusName, displayedStatus) {

        // проверить параметр orderId
        if (typeof(orderId) == 'undefined') {
            console.error("Не указан параметр идентификатор заказа.");
            return false;
        }

        // проверить параметр toStatus
        if (typeof(toStatus) == 'undefined') {
            console.error("Не указан параметр название метода нового статуса позиции заказа.");
            return false;
        }

        // проверить параметр statusName
        if (typeof(statusName) == 'undefined') {
            console.error("Не указан параметр название нового статуса позиции заказа.");
            return false;
        }
        
        // проверить параметр displayedStatus
        if (typeof(displayedStatus) == 'undefined') {
            console.error("Не указан параметр название метода статуса отображенного пользователю.");
            return false;
        }

        // url контроллер установить статус позиции заказа
        var actionUrl = Routing.generate('Nitra_MiniTetradkaBundle_Order_set_status', false, true);
        
        // отправить ajax запрос на изменение статуса позиции заказа
        var fromServer = order.ajax(actionUrl, {
            data: {
                orderId: orderId, 
                toStatus: toStatus,
                displayedStatus: displayedStatus
            },
            dataType: '' // не указываем, может возвращать json, script в зависимости от логики цепочки
        });
        
        // проверить тип ответа от сервера
        if (typeof(fromServer) != 'undefined' && // ответ сервера
            typeof(fromServer.type) != 'undefined'// тип ответа
        ) {
            // обработка в зависимости от типа ответа сервера
            switch (fromServer.type) {

                // отображение ошибки
                case 'error':
                    common.flash('Невозможно перевести заказ в статус "' + statusName + '". ' + fromServer.message, fromServer.type);
                    break;

                    // изменение статуса прошло успешно
                case 'success':
                    // изменение статуса прошло успешно
                    common.flash('Заказ переведен в статус "' + statusName + '".', 'notice');
                    // обновить строку заказа
                    order.renderRow(orderId);
                    break;

                default:
                    return;
                    break;
            }

        }
        
        // прерываем выполнение
        return;
    },    
    
    
    /**
     * объект позиции заказа 
     **/
    orderEntry: {
        
        /**
         * изменить статус позиции по заказу
         * @param integer orderId - ID идентификатор заказа
         * @param integer orderEntryId - ID идентификатор позиции заказа
         * @param string  toStatus     - название нового метода статуса позиции заказа
         * @param string  statusName   - текстовое название нового статуса позиции заказа
         * @param string  displayedStatus   - название метода статуса отображенного пользователю
         **/
        setStatus: function(orderId, orderEntryId, toStatus, statusName, displayedStatus) {
            
            // проверить параметр orderId
            if (typeof(orderId) == 'undefined') {
                console.error("Не указан параметр идентификатор заказа.");
                return false;
            }
            
            // проверить параметр orderEntryId
            if (typeof(orderEntryId) == 'undefined') {
                console.error("Не указан параметр идентификатор позиции заказа.");
                return false;
            }
            
            // проверить параметр toStatus
            if (typeof(toStatus) == 'undefined') {
                console.error("Не указан параметр название метода нового статуса позиции заказа.");
                return false;
            }
            
            // проверить параметр statusName
            if (typeof(statusName) == 'undefined') {
                console.error("Не указан параметр название нового статуса позиции заказа.");
                return false;
            }
            
            // проверить параметр displayedStatus
            if (typeof(displayedStatus) == 'undefined') {
                console.error("Не указан параметр название метода статуса отображенного пользователю.");
                return false;
            }
            
            // url контроллер установить статус позиции заказа
            var actionUrl = Routing.generate('Nitra_MiniTetradkaBundle_OrderEntry_set_status', false, true);
            
            // отправить ajax запрос на изменение статуса позиции заказа
            var fromServer = order.ajax(actionUrl, {
                data: { 
                    orderEntryId: orderEntryId, 
                    toStatus: toStatus,
                    displayedStatus: displayedStatus
                },
                dataType: '' // не указываем, может возвращать json, script в зависимости от логики цепочки
            });
            
            // проверить тип ответа от сервера
            if (typeof(fromServer) != 'undefined' && // ответ сервера
                typeof(fromServer.type) != 'undefined'// тип ответа
            ) {
                // обработка в зависимости от типа ответа сервера
                switch(fromServer.type) {
                    
                    // отображение ошибки
                    case 'error':
                        common.flash('Невозможно перевести позицию в статус "' + statusName + '". '+fromServer.message, fromServer.type);
                        break;
                        
                    // изменение статуса прошло успешно
                    case 'success':
                        // изменение статуса прошло успешно
                        common.flash('Позиция переведена в статус "' + statusName + '".', 'notice');
                        // обновить строку заказа
                        order.renderRow(orderId);
                        break;
                        
                    default:
                        return;
                        break;
                }
                
            }
            
            // прерываем выполнение
            return;
        }
        
    }, 
    
    /**
     * объект формы inPlaceEdit 
     * добавление комментария для заказа
     **/
    comment: {
        
        // объект на котром произошло событие клик редактирования
        tagObj: false,
        
        // объект формы 
        formObj: false,
        
        /**
         * отображение формы inPlaceEdit добавления коментария
         * @param integer orderId - ID заказ
         * @param object _this    - javascript this объект на котором произошло событие клик редактирования
         **/
        add: function (orderId, _this) {
        
            // проверить открыта ли форма 
            if (typeof(order.comment.formObj) == 'object') {
                // форма открыта, закрыть форму
                order.comment.formObj.children('*[name="cancel"]:first').click();
            }
            
            // установить объект на котором произошло событыие 
            order.comment.tagObj = $(_this);
            order.comment.tagObj.hide(); // спрятать ссылку по которой произошол клик 
            
            // получить форму
            order.comment.formObj = $('#order_comment_form');
            
            // установить данные формы
            $('#order_comment_id').val(orderId);
            $('#order_comment_comment').val('').focus();
            
            // отображение формы 
            order.comment.formObj.insertBefore(order.comment.tagObj);
            order.comment.formObj.show();            
        },
        
        /**
         * Отмена редактирования
         **/
        cancel: function () {
            order.comment.formObj.hide();
            $('#order_comment_order').val('');
            $('#order_comment_comment').val('');
            order.comment.tagObj.show(); // показать ссылку по которой произошол клик 
        },
        
        /**
         * сохранить форму inPlacedEdit
         **/
        send: function () {
            
            // url контроллер добавления комментария к заказу 
            var actionUrl = order.comment.formObj.attr('action');
            
            // оправить ajax форму на сервер 
            var fromServer = order.ajax(actionUrl, {
                data: order.comment.formObj.serialize()
            });
            
            // проверить тип ответа от сервера
            if (typeof(fromServer) != 'undefined' && // ответ сервера
                typeof(fromServer.type) != 'undefined'// тип ответа
            ) {
                // обработка в зависимости от типа ответа сервера
                switch(fromServer.type) {
                    
                    // отображение ошибки
                    case 'error':
                        common.flash('Невозможно добавить комментарий для заказа. '+fromServer.message, fromServer.type);
                        // прерываем выполнение
                        return;
                        break;
                        
                    // обновление комментария прошло успешно
                    case 'success':
                    default:
                        common.flash('Комментарий к заказу успешно добавлен.','notice');
                        
                        // показать комментарий
                        if (typeof(fromServer.comment) != 'undefined') {
                            $('#comment_text'+fromServer.orderId).html(fromServer.comment);
                        }
                        break;
                }
                
            }            
            
            // обновление отображения формы
            order.comment.formObj.hide();
            order.comment.tagObj.show();
        }
        
    },
    
    /**
     * форма добавления редактирования 
     * продуктов в заказе
     **/
    form: {
        
        // флаг попадания скроллинга в самый низ
        activeScroll: false,
        
        // XMLHttpRequest 
        // поиск продуктов
        xhrSearchProduct: false,
        xhrEstimateDelivery: false,
        
        /**
         * поиск продуктов в форме добавления/редактирования заказа
         * @param string - тип поиска 
         **/
        searchProduct: function(type) {
            
            // тип поиска по умолчанию
            if (typeof(type) == 'undefined') {
                var type = 'search';
            }
            
            // Отображаем loader
            $('#loader').show(); 

            // начать поиск сначала
            if(type == 'search') {
                // переводим скроллинг в верх 
                // для предотвращения двойного поиска по одному запросу
                $('#seacrh_product_list').scrollTop(0);
                // обнуляем результат поиска
                $('#search_product_page').val(0);
                $('#seacrh_product_list').html('');
                order.form.activeScroll = true;
                
            } else if(type == 'scroll') {
                // scroll
                var page = parseInt($('#search_product_page').val()) + 1;
                $('#search_product_page').val(page);
            }

            // получить форму поиска 
            var form = $('#search_product_form');
            var actionUrl = form.attr('action');
            
            // перед выполнением поиска устанавливаем флаг попадания скроллинга в самый низ
            order.form.activeScroll = false;
            
            // проверить, если запушен предыдущий поиск продуктов
            if (order.form.xhrSearchProduct) {
                // прервать предыдущий поиск
                order.form.xhrSearchProduct.abort();
            }
            
            // отправить ajax запрос поиск продуктов
            // отправляем не через общий метод
            order.form.xhrSearchProduct = $.ajax({
                url: actionUrl,
                type: 'POST',
                data: form.serialize(),
                dataType: 'html',
                async: true,
                error: function() {
                    // обнулить xhrSearchProduct
                    order.form.xhrSearchProduct = false;
                    // спрятать loader
                    $('#loader').hide();            
                },
                success: function(fromServer){
                    
                    // текущая страница поиска
                    var page = $('#search_product_page').val();
                    
                    // проверить ответ сервера
                    if (fromServer) {
                        
                        // первая страница 
                        if (page == 0) {
                            // добавить результат в общий DIV результатов поиска
                            // в отвере приходит table
                            $('#seacrh_product_list').append(fromServer);
                            
                        } else {
                            // если не первая страница то 
                            // в ответе приходят только TR
                            // добавляем ответ в таблицу результатов
                            $('#seacrh_product_table').find('tbody').append(fromServer);
                        }
                        
                        // обновляем флаг скроллинга
                        order.form.activeScroll = true;
                    }
                    
                    // обнулить xhrSearchProduct
                    order.form.xhrSearchProduct = false;
                    // спрятать loader
                    $('#loader').hide();
                    
                    // отключаем событие onclick
                    // для td с классом td_site_url
                    $('#seacrh_product_table').find('.td_site_url').on('click', function(e){
                        // отменяем все действия
                        e.stopPropagation();
                    });
                    
                    
                }
            });
        },
            
        /**
         * добавление продукта
         * из левого списка в правый
         * @param object _this - javascript this объект на котором произошло событие клик
         **/
        addProduct: function(_this) {

            // получаем параметры добавляемого продукта
            var productId = $(_this).attr('productId');
            var priceOut = $(_this).attr('priceOut');
            var isActive = $(_this).attr('isActive');
            var productName = $(_this).attr('productName');
            var productArticle = $(_this).attr('productArticle');
            
            // счетчик продуктов
            var counter = (order.form.getProductsCounter() + 1);
            
            // строка позиции заказа
            var orderEntryRow = $('#orderEntryProductId'+productId);
            
            // если продукт есть в заказе
            if (orderEntryRow.length) {
                // продукт в заказ добавлен ранее 
                var quantity = parseInt(orderEntryRow.find('.quantity').val());
                orderEntryRow.find('.quantity').val(quantity+1).change();
                // прервать выполнение 
                return false;
            }
            
            // продукт в заказ не добавлен
            // добавить продукт в заказ 
            
            // html строка добавления
            var addTrHtml = $('#cartListTable').attr('add-block');
            
            // Заменяем названия - индексом и устанавливаем название продукта
            addTrHtml = addTrHtml
                .replace(/__iteration__/g, counter)
                .replace(/__name__/g, counter)
                .replace(/__ProductId__/g, productId)
                .replace(/__ProductName__/g, productName)
                .replace(/__ProductArticle__/g, productArticle)
                .replace(/__Total__/g, priceOut);

            // Добавляем позицию в правый список
            $('#cartList').append(addTrHtml);

            // заполнить позицию заказа
            $('#order_orderEntry_' + counter + '_productId').val(productId);
            $('#order_orderEntry_' + counter + '_priceOut').val(priceOut).change(function(){
                order.form.recalcTotal(this); // пересчтитать тотал
                order.form.deliverySyncEstimateDelivery(); // пересчитать стоимость доставки
            });
            $('#order_orderEntry_' + counter + '_quantity').val('1').change(function(){
                order.form.recalcTotal(this); // пересчтитать тотал
                order.form.deliverySyncEstimateDelivery(); // пересчитать стоимость доставки
            });
            
            // пересчтитать тотал
            order.form.recalcTotal(this);
            // Получить расчет стоимостей доставки
            order.form.deliverySyncEstimateDelivery();
        },
        
        /**
         * удаление продукта
         * из левого списка
         * @param object _this - javascript this объект на котором произошло событие клик
         * @param integer productId - ID продукта
         **/
        delProduct: function(_this) {
            // проверить, если запушен предыдущий поиск продуктов
            if (order.form.xhrEstimateDelivery) {
                // прервать предыдущий поиск
                order.form.xhrEstimateDelivery.abort();
            }
            // удалить продукт
            $(_this).closest('tr').remove();
            order.form.recalcTotal(_this);
            order.form.deliverySyncEstimateDelivery();
        }, 
        
        /**
         * подсчет кол-ва TR продуктов 
         **/
        getProductsCounter: function() {
            
            var counter = 0;
            var iterations = [];
            
            // получить максивальный элемент массива
            iterations.max = function(arr){ 
                return Math.max.apply(Math,arr);
            };
            
            // обойти все TR с продуктами
            $('#cartList tr').each(function(){
                // проверить установлен ли атрибут iteration для TR
                if (typeof($(this).attr('iteration')) != 'undefined') {
                    // наполнить массив 
                    iterations.push(parseInt($(this).attr('iteration')));
                }
            });
            
            // проверить кол-во елементов в массиве, для выбора максимального значения
            if (iterations.length) {
                // получить максимальный элемент массива
                counter = iterations.max(iterations);
            }
            
            // вернуть счетчик
            return counter;
        },
        
        /**
         * Пересчитать тотал
         * @param object _this - javascript this объект на котором произошло событие клик
         **/
        recalcTotal: function(_this) {
            
            var tr = $(_this).closest('tr');
            var quantity = parseInt(tr.find('.quantity').val());
            var price_out = parseFloat(tr.find('.price_out').val());
            
            tr.find('.order_entry_total .order_entry_total_input').val(quantity * price_out);
            
            // Обновление итого по заказу
            var total = 0
            $('.order_entry_total .order_entry_total_input').each(function(){
                if (!$(this).is('tr.order_entry_canceled>td')) {
                    total += parseInt($(this).val());
                }
            });
            $('#order_total').text(total);
        },
        
        
        /**
         * Отправить завпрос в DS 
         * для получения инфо по доставке
         **/
        deliverySyncEstimateDelivery: function() 
        {
            return;
            // получить выбранный город
            var city = $('#order_select_city_for_warehouse option:selected');
            if (typeof(city) == 'undefined') {
                return;
            }
            
            // проверить если не выбран город доставки прерываем выполенение
            if (!city.val()) {
                // не выбран город получатель
                return;
            }
            
            // счетчик продуктов
            var counter = (order.form.getProductsCounter());
            if (counter == 0) {
                // нет ни одного продукта для доставки
                return;
            }
            
            // получить выбранный склад
            var warehouse = $('#order_warehouse option:selected');
            
            // спрятать результирующие данные доставки
            $('.estimate_delivery_product').remove();
            $('.estimate_delivery_deliveries').remove();
            
            // массив доставляемых продуктов
            var deliveryData = {
                // ID города получателя
                toCityId: city.val(),
                // ID склада получателя
                toWarehouseId: warehouse.val(),
                // массив доставляемых продуктов
                products: []
            };
            
            
            // обойти все TR с продуктами
            $('#cartList tr').each(function(){
                
                // проверить необходимые данные для расчета стоимости доставки
                if (typeof($(this).attr('iteration')) == 'undefined' || 
                    typeof($(this).attr('productId')) == 'undefined'
                ) {
                    // continue
                    return true;
                }

                // наполнить массив доставляемых продуктов
                // JSON.stringify(
                deliveryData.products.push({
                    productId: $(this).attr('productId'),
                    iteration: $(this).attr('iteration'),
                    quantity: $(this).find('.quantity').val(),
                    priceOut: parseFloat($(this).find('.price_out').val())
                });
            });
            
            // проверить кол-во доставляемых продуктов
            if (deliveryData.products.length == 0) {
                // прерываем выполенение
                return;
            }
            
            // проверить, если запушен предыдущий поиск продуктов
            if (order.form.xhrEstimateDelivery) {
                // прервать предыдущий поиск
                order.form.xhrEstimateDelivery.abort();
            }
            
            // показать процесс загрузки
            var htmlResult = $('#estimate_delivery_template')
                .html()
                .replace('<tbody>', '<tbody><tr><td colspan="6">'+$('#estimate_delivery_loader').html() +'</tr>');
            $('#estimate_delivery_result').html(htmlResult);
            
            // url контроллер получение данных по доставке 
            var actionUrl = Routing.generate('Nitra_MiniTetradkaBundle_DeliverySync_estimate_delivery', false, true);
            
            // отправить ajax запрос
            order.form.xhrEstimateDelivery = $.ajax({
                url: actionUrl,
                type: 'POST',
                data: deliveryData,
                dataType: 'json',
                async: true,
                
                // ощибка получения данных 
                error: function() {
                    order.form.deliverySyncEstimateDeliveryOnError('Сервис расчета стоимостей доставки не доступен.');
                },
                
                // данные полуены успешно
                success: function(fromServer){
                    
                    // проверить ответ сервера
                    if (typeof(fromServer) == 'undefined' || 
                        // ответ сервера должен содержать тип ответа
                        typeof(fromServer.type) == 'undefined'
                    ) {
                        // от сервера получен не корректный ответ
                        order.form.deliverySyncEstimateDeliveryOnError('Сервис расчета стоимостей доставки не доступен.');
                        return;
                    }
                    
                    // ответ сервера ошибка
                    if (fromServer.type == 'error') {
                        // отображение ошибки 
                        order.form.deliverySyncEstimateDeliveryOnError(fromServer.message);
                        // прервать выполнение расчетов
                        return;
                    }
                    
                    // отображение результатов расчета 
                    $('#estimate_delivery_result').html(fromServer.html);
                    
                    // получить список доставляемых продуктов с расчетными данными
                    // обновление отображения списка продуктов
                    var products = fromServer.estimateDelivery.products;
                    $.each(products, function(productKey, product) {
                        
                        // доступные ТК для каждого продукта
                        var htmlDeliveries = '';
                        // флаг ТК может доставить продукт
                        var isAvailable = false;
                        
                        // проверить возможность доставки продукта ТК
                        if (typeof(product.deliveries) != 'undefined') {
                            $.each(product.deliveries, function(deliveryKey, delivery) {
                                
                                // проверить ключ ТК 
                                // ключь с отрицательным числом только для ТК тариф Интернет магазин 
                                if (deliveryKey <= 0) {
                                    // continue
                                    return true;
                                }
                                
                                // проверить ТК
                                if (delivery &&
                                    delivery.isAvailable
                                ) {
                                    isAvailable = true;
                                    htmlDeliveries += '<span class="green">'+delivery.name+'</span>; ';
                                } else {
                                    htmlDeliveries += '<span class="red">'+delivery.name+'</span>; ';
                                }
                            });
                        }
                        
                        var divIdProduct    = 'estimate_delivery_product_'+product.iteration;
                        var divIdDeliveries = 'estimate_delivery_product_'+product.iteration + '_by_deliveries';
                        
                        var htmlProductDelivery 
                                = '<div id="'+divIdProduct+'" class="estimate_delivery_product">'
                                + '<a href="javascript:void(0);" '
                                + " onclick=\"$('#"+divIdProduct+"').remove();$('#"+divIdDeliveries+"').show();\" "
                                + '>Доставка ТК</a>: ';
                        if (isAvailable) {
                            htmlProductDelivery += '<img src="/bundles/nitraminitetradka/images/delivery_sync/tk_is_available_1.png"> ';
                        } else {
                            htmlProductDelivery += '<img src="/bundles/nitraminitetradka/images/delivery_sync/tk_is_available_0.png"> ';
                        }
                        
                        htmlProductDelivery 
                                    // зактыть div id= divIdProduct
                                    += '</div>'
                                    +  '<div id="' + divIdDeliveries + '" '
                                    +  ' class="estimate_delivery_deliveries" style="display:none">' 
                                    +  htmlDeliveries
                                    +  '</div>';
                        
                        // стоимость доставки для каждого продукта
                        var htmlProductCost = '';
             
                        // проверить если продукт не доступен 
                        // для расчета стоимости доставки
                        if (!product.isAvailableEstimateCost) {
                            htmlProductCost 
                                += '<div class="estimate_delivery_deliveries">'
                                +  '<span class="red">Расчет стоимости доставки не производился. Не указаны размеры.</span>'
                                +  '</div>';
                        }
                        
                        // добавить в конец ячейки TD
                        $('#cartList')
                                .find('[iteration="'+product.iteration+'"]')
                                .find('.order_entry_name')
                                .append(htmlProductDelivery + htmlProductCost);
                    });
                    
                    // обнулить xhrEstimateDelivery
                    order.form.xhrEstimateDelivery = false;
                }
            });
        },
        
        /**
         * обработка ошибочной ситуации 
         * при расчете стоимостей доставки
         * @param string message - текст сообщения
         **/
        deliverySyncEstimateDeliveryOnError: function(message)
        {
            
            // обнулить xhrEstimateDelivery
            order.form.xhrEstimateDelivery = false;
            
            // проверить текст ошибки 
            if (typeof(message) == 'undefined') {
                var message = '';
            }
            
            // полказать процесс загрузки
            var htmlResult = $('#estimate_delivery_template')
                    .html()
                    .replace('<tbody>', '<tbody><tr><td colspan="6">'+ message +'</tr>');
            $('#estimate_delivery_result').html(htmlResult);
        },
        
        /**
         * конвертировать расчетную дату доставки в формат для отображения
         **/
        estimateDeliveryDateConverter: function(UNIX_timestamp) {
            var a = new Date(UNIX_timestamp*1000);
            var months = ['Янв','Февр','Март','Апр','Май','Июнь','Июль','Авг','Сент','Окт','Нояб','Дек'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            return date + ' ' + ' ' + month + ' ' + year;
        }
        
    },
    
    /** 
     * перевести объект в стринг
     * @param object - объект переводимый в string
     * @return string - объект преобразованный в строку
     **/
    toString: function(obj) {

        // проверить объект переводимы в стинг
        if (typeof(obj) == 'undefined') {
            console.error("Нет параметра obj для преобразования в строку");
        }

        // перевести в строку 
        var s = '{\n';
        for (var p in obj) {
            s += '    "' + p + '": "' + obj[p] + '"\n';
        }

        // вернуть строку
        return s + '}';
    }
    
    
}
