<?php
namespace Nitra\MiniTetradkaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;

/**
 * CurrencyRepository
 */
class CurrencyRepository extends EntityRepository
{
    
    /**
     * валюта по умолчанию 
     * @var OrderEntryStatus
     */
    protected $defaultCurrency;
    
    /**
     * получить код валюты по умолчанию
     * @return string currency_code
     * @throw \Exception ошибка код валюты по умолчснию не найден
     */
    public function getDefaultCurrencyCode()
    {
        // проверить существования переменной в нараметрах
        if (NitraGlobals::$container->hasParameter('nitra_minitetradka_currency_code')) {
            // вернуть код валюты 
            return NitraGlobals::$container->getParameter('nitra_minitetradka_currency_code');
        }
        
        // статус не найден
        throw new \Exception('Код валюты по умолчанию не найден.');
    }
    
    /**
     * получить валюту по умолчанию
     * @return Currency 
     * @throw \Exception ошибка получения валюты по умолчанию 
     */
    public function getDefaultCurrency()
    {
        // проверить если валюту по умолчанию не получали ранее
        // предотвращене получения повторных данных, 
        // уменьшение кол-ва запросов к БД 
        // полезно для случаев когда создается много новых сущностей с валютой по умолчанию
        if (!$this->defaultCurrency) {
            
            // получить статус по умолчанию 
            $this->defaultCurrency = $this->getEntityManager()
                ->getRepository('NitraMiniTetradkaBundle:Currency')
                ->find($this->getDefaultCurrencyCode());
            
            // проверить статус
            if (!$this->defaultCurrency) {
                // статус не найден
                throw new \Exception('В базе данных не найдена валюта по умолчанию.');
            }
        }
        
        // вернуть валюту по умолчанию
        return $this->defaultCurrency;
    }
    
    /**
     * выполнить округление
     * @param  float $val значение для округления
     * @return float результат округления
     * @example CurrencyRepository::round(343.259999999) ... return 343.25
     */
    public static function round($val)
    {
        return (intval($val*100)/100);
    }
    
}
