<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus;

/**
 * OrderStatus
 * Интерфейс сервиса изменения статусов 
 */
interface OrderStatusInterface
{
    
    /**
     * установить заказ для которого изменяется статус
     * @param Order $order заказ
     */
    public function setOrder(\Nitra\MiniTetradkaBundle\Entity\Order $order);
    
    /**
     * установить статус заказа
     * @param OrderStatus $orderStatus статус закза
     */
    public function setToStatus(\Nitra\MiniTetradkaBundle\Entity\OrderStatus $orderStatus);
    
    /**
     * установить статус заказа который был отображен пользователю
     * @param OrderStatus $orderStatus статус закза
     */
    public function setDisplayedStatus(\Nitra\MiniTetradkaBundle\Entity\OrderStatus $orderStatus);
    
}
