<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus;

/**
 * ChainsAllow
 * доступные цепочки изменения статусов
 */
class ChainsAllow
{
    
    /**
     * @var array
     * массив цепочек изменения статусов 
     */
    public static $chains = array(
        
        // waiting с будильника 
        'waiting_to_ordered',
        'waiting_to_completed',
        'waiting_to_canceled',
        
        // ordered - с заказан
        'ordered_to_waiting',
        'ordered_to_completed',
        'ordered_to_canceled',
        
        // completed - с завершено
        'completed_to_waiting',
        'completed_to_ordered',
        
        // canceled - с отмены
        'canceled_to_waiting',        
        'canceled_to_ordered',        
    );    
    
}
