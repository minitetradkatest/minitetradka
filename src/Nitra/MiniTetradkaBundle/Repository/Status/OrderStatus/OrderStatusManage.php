<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus;

use Nitra\MiniTetradkaBundle\Entity\Order;
use Nitra\MiniTetradkaBundle\Entity\OrderStatus;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;
use Nitra\MiniTetradkaBundle\Repository\Status\StatusManage;
use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ChainsAllow;
use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\OrderStatusInterface;

/**
 * OrderStatusManage
 */
class OrderStatusManage extends StatusManage implements OrderStatusInterface
{
    
    /**
     * @var Order 
     * Заказ
     */
    protected $order;
    
    /**
     * @var OrderStatus 
     * устанавливаемы статус
     */
    protected $toStatus;
    
    /**
     * @var OrderStatus
     * отображенный пользователю статус
     */
    protected $displayedStatus;
    
    /**
     * @var OrderStatus
     * статус по умолчанию инициализиуется после первого получения
     */
    private $defaultStatus;
    
    /**
     * {@inheritDoc}
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setToStatus(OrderStatus $status)
    {
        $this->toStatus = $status;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDisplayedStatus(OrderStatus $status)
    {
        $this->displayedStatus = $status;
        return $this;
    }
    
    /**
     * получить статус по умолчанию
     * @return OrderStatus 
     * @throw \Exception ошибка получения статуса по умолчанию
     */
    public function getDefaultStatus()
    {
        // проверить если статус по умолчанию не получали ранее
        // предотвращене получения повторных данных, 
        // уменьшение кол-ва запросов к БД 
        // полезно для случаев когда создается много новых сущностей со статусом по умолчанию 
        if (!$this->defaultStatus) {
            
            // получить статус по умолчанию 
            $this->defaultStatus = NitraGlobals::$em
                ->getRepository('NitraMiniTetradkaBundle:OrderStatus')
                ->findOneBy(array(
                    'methodName' => NitraGlobals::$container->getParameter('nitra_minitetradka_default_order_status')
                ));
            
            // проверить статус
            if (!$this->defaultStatus) {
                // статус не найден
                throw new \Exception('В базе данных не найден статус заказа по умолчанию.');
            }
        }
        
        // вернуть статус по умолчанию
        return $this->defaultStatus;
    }    
    
    /**
     * установить статус заказа по умолчанию
     * @param Order $order
     */
    public function setDefaultStatusForOrder(Order $order)
    {
        // получить статус по умолчанию
        $status = $this->getDefaultStatus();
        // установить статус по умолчанию 
        $order->setOrderStatus($status);
    }        
    
    /**
     * получить доступные статусы для позиции заказа $order, 
     * в кторые можно перевести позицию заказа
     * @param Order $order - позиция заказа 
     * @return array $orderStatuses - массив доступных цепочек для позиции заказа
     * @return false доступные цепочки не найдены
     */
    public function getAllowStatuses(Order $order)
    {
        // массив доступных цепочек для позиии заказов
        $chains = ChainsAllow::$chains;
        
        // обойти все цепочки найти подходящие цепочки
        $chainArr = array();
        foreach ($chains as $chain) {

            // цепочка из статуса позиции 
            $chainFrom = substr($chain, 0, strlen($order->getOrderStatus()->getMethodName()));

            // подходящая цепочка
            if ($chainFrom == $order->getOrderStatus()->getMethodName()) {
                // цепочка в статус позиции 
                $chainTo = substr($chain, strlen($chainFrom . '_to_'));
                // наполнить массив 
                // запоминаем в виде key => value 
                $chainArr[$chainTo] = $chainTo;
            }
        }
        
        // получить массив статусов в которые разрешено переводить из текущего
        $chainArr = array_values($chainArr);
        
        // получить статусы
        if ($chainArr) {
            // получить статусы по по массиву
            $orderStatuses = NitraGlobals::$em
                ->createQueryBuilder()
                ->select('st.id, st.name, st.methodName, st.cssClass')
                ->from('NitraMiniTetradkaBundle:OrderStatus', 'st', 'st.id')
                ->where('st.methodName IN(:statuses)')->setParameter('statuses', $chainArr)
                ->getQuery()
                ->getArrayResult();
            
            // вернуть массив статусов 
            return $orderStatuses;
        }
        
        // доступные статусы не найдены
        return false;
    }    
    
    
    
    
}
