<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus;

use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\DependencyInjection\Container;
use Nitra\MiniTetradkaBundle\Entity\OrderStatusHistory;
use Nitra\MiniTetradkaBundle\Repository\Status\StatusInterface;
use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\OrderStatusManage;

/**
 * OrderStatusService
 * Сервис именения статусов заказов
 */
class OrderStatusService extends OrderStatusManage implements StatusInterface
{
    
    /**
     * @var Container $container
     */
    protected $container;
    
    /**
     * @var object $processApi
     * обработчик цепочки
     */
    protected $processApi;
    
    /**
     * Конструктор
     */
    public function __construct(Container $container)
    {
        // установить $container
        $this->container = $container;
    }

    /**
     * валидация 
     * @return boolean 
     *          true - валидация пройдена успешно
     *          false - валидация команды не пройдена
     */
    public function isValid()
    {
        // валидировать команду
        $errorMessage = $this->validate();
        if ($errorMessage) {
            // валидация не пройдена
            return false;
        }
        
        // валидация пройдена успешно
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    public function validate()
    {
        // проверить заказ 
        if (!$this->order) {
            return "Не указан заказ для которого изменяется статус.";
        }
        
        // проверить заказ 
        if (!$this->toStatus) {
            return "Не указан статус в который будет переведен заказ.";
        }
        
        // проверить текущий статус
        if ($this->toStatus->getId() == $this->order->getOrderStatus()->getId()) {
            return "Заказ ".(string) $this->order." уже находиться в статусе ".(string)$this->toStatus .".";
        }
        
        // проверить отображенный статус
        if (!$this->displayedStatus) {
            return "Не указан статус, который был отображен пользователю.";
        }
        
        // проверяем соответствие отображенного пользователю и текущего статуса 
        if ($this->displayedStatus->getId() != $this->order->getOrderStatus()->getId()) {
            return "Отображенные ранее Вам данные устарели. Проверьте текущий статус и при необходимости повторите попытку заново.";
        }
        
        // Направление изменения статуса
        $direction = $this->order->getOrderStatus()->getMethodName() . '_to_' . $this->toStatus->getMethodName();
        
        // получитьмассив доступных цепочек
        $chainsAllow = ChainsAllow::$chains;
        
        // проверить цепочку в массиве допустимых цепочек
        if (!in_array($direction, $chainsAllow)) {
            return "Не указан порядок изменения статуса для заказа. Цепочка ".$direction." не найдена.";
        }
        
        // обработчик цепочки
        $processName = 'Process'.Inflector::classify($direction);
        $processClass = '\\'.__NAMESPACE__.'\\Process\\'.$processName;
        
        // проверить существование класса обработчика цепочки 
        if (!class_exists($processClass)) {
            return "Для цепочки \"".$direction."\" не найден обработчик: \"".$processName."\".";
        }
        
        // создать обработчик
        $this->processApi = new $processClass($this->order, $this->getParameters());
        
        // валидировать цепочку
        $errorMessage = $this->processApi->validate();
        if ($errorMessage) {
            return $errorMessage;
        }
        
        // валидация пройдена успешно
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    public function process()
    {
        // валидировать команду
        $errorMessage = $this->validate();
        if ($errorMessage) {
            throw new \Exception($errorMessage);
        }
        
        // валидировать команду
        $errorMessage = $this->processApi->validate();
        if ($errorMessage) {
            throw new \Exception($errorMessage);
        }
        
        // получить EntityManager
        $em = $this->container->get('doctrine.orm.entity_manager');
        
        // выполнить цепочку
        $this->processApi->process();
        
        // создать историю изменения статусов
        $orderStatusHistory = new OrderStatusHistory();
        $orderStatusHistory->setOrder($this->order);
        $orderStatusHistory->setFromStatus($this->order->getOrderStatus());
        $orderStatusHistory->setToStatus($this->toStatus);
        $em->persist($orderStatusHistory);
        
        // установить статус
        $this->order->setOrderStatus($this->toStatus);
        $this->order->addOrderStatusHistory($orderStatusHistory);
        
        // цепочка обработана успешно
        return false;
    }
    
    /**
     * выполнить установку статуса try ... catch
     * @return false цепочка обработана успешно
     * @return string ошибка обработки цепочки 
     */
    public function tryProcess()
    {
        
        // попытка обработки цепочки
        try {
            
            // обработать цепочку
            $this->process();
            
        } catch (\Exception $e) {
            // вернуть текст ошибки 
            return $e->getMessage();
        }
        
        // цепочка обработана успешно
        return false;
    }
    
}
