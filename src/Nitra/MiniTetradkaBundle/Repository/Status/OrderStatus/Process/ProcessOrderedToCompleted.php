<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessOrderedToCompleted
 * обработчик цепочки ordered_to_completed
 */
class ProcessOrderedToCompleted extends ProcessCommon
{
    
}
