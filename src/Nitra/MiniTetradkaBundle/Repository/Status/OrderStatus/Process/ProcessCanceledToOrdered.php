<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessCanceledToOrdered
 * обработчик цепочки canceled_to_ordered
 */
class ProcessCanceledToOrdered extends ProcessCommon
{
    
}
