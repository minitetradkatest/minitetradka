<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessWaitingToCompleted
 * обработчик цепочки waiting_to_completed
 */
class ProcessWaitingToCompleted extends ProcessCommon
{
    
}
