<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessCanceledToWaiting
 * обработчик цепочки canceled_to_waiting
 */
class ProcessCanceledToWaiting extends ProcessCommon
{
    
}
