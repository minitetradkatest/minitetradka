<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessWaitingToOrdered
 * обработчик цепочки waiting_to_ordered
 */
class ProcessWaitingToOrdered extends ProcessCommon
{
    
}
