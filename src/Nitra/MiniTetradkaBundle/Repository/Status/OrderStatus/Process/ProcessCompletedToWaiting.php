<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessCompletedToWaiting
 * обработчик цепочки completed_to_waiting
 */
class ProcessCompletedToWaiting extends ProcessCommon
{
    
}
