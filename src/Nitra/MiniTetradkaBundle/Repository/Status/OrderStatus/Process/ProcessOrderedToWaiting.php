<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessOrderedToWaiting
 * обработчик цепочки ordered_to_waiting
 */
class ProcessOrderedToWaiting extends ProcessCommon
{
    
}
