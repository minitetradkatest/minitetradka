<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessWaitingToCanceled
 * обработчик цепочки waiting_to_canceled
 */
class ProcessWaitingToCanceled extends ProcessCommon
{
    
}
