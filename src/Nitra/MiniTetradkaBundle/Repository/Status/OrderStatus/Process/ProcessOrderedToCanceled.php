<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessOrderedToCanceled
 * обработчик цепочки ordered_to_canceled
 */
class ProcessOrderedToCanceled extends ProcessCommon
{
    
}
