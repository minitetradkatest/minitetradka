<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus\ProcessCommon;

/**
 * ProcessCompletedToOrdered
 * обработчик цепочки completed_to_ordered
 */
class ProcessCompletedToOrdered extends ProcessCommon
{
    
}
