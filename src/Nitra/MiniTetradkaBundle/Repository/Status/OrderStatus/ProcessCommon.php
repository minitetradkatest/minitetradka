<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderStatus;

use Nitra\MiniTetradkaBundle\Repository\Status\StatusInterface;
use Nitra\MiniTetradkaBundle\Repository\Status\StatusParameters;
use Nitra\MiniTetradkaBundle\Entity\Order;

/**
 * ProcessCommon
 * обший класс обработчиков цепоцек
 */
class ProcessCommon extends StatusParameters implements StatusInterface
{
    
    /**
     * @var Order $order заказ
     */
    protected $order;
    
    /**
     * Конструктор
     */
    public function __construct(Order $order, array $parameters = null)
    {
        // установить заказ 
        $this->order = $order;
        
        // установить параметры выполнения цепочки
        $this->setParameters($parameters);
    }
    
    /**
     * {@inheritDoc}
     */
    public function isValid()
    {
        // валидировать команду
        $errorMessage = $this->validate();
        if ($errorMessage) {
            // валидация не пройдена
            return false;
        }
        
        // валидация пройдена успешно
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    public function validate()
    {
        // валидация пройдена успешно
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    public function process()
    {
        // валидировать команду
        $errorMessage = $this->validate();
        if ($errorMessage) {
            throw new \Exception($errorMessage);
        }
        
        // цепочка обработана успешно
        return false;
    }
    
}
