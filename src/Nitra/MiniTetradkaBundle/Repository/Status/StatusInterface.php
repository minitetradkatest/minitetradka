<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status;

/**
 * StatusInterface
 * описание обработчиков цепочек
 */
interface StatusInterface
{
    
    /**
     * валидация 
     * @return boolean 
     *          true - валидация пройдена успешно
     *          false - валидация команды не пройдена
     */
    public function isValid();    
    
    /**
     * валидировать установку статуса
     * @return string - текст сообщения об ошибке 
     * @return false  - валидация пройдена успешно
     */
    public function validate();
    
    /**
     * выполнить установку статуса
     * @throw Exception - ошибка
     * @return mixed - результат выполнения цепочки
     */
    public function process();
    
}
