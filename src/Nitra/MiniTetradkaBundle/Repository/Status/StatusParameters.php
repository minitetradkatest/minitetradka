<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status;

/**
 * StatusParameters
 * Парамерты изменения статусов
 */
class StatusParameters
{
    
    /**
     * @var array $parameters - массив параметров
     */
    protected $parameters;    
    
    /**
     * получить Client
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     * получить массив parameters
     */
    public function getParameters()
    {
        return $this->parameters;
    }
    
    /**
     * установить массив parameters
     * @param array $parameters - массив устанавливаемых параметров
     * @return $this->parameters
     */
    public function setParameters(array $parameters=null)
    {
        // установить массив параметров 
        if ($parameters) {
            $this->parameters = $parameters;
        } else {
            $this->parameters = array();
        }
        
        // вернуть установленный массив параметров
        return $this->parameters;
    }
    
    /**
     * получить елемента массива parameters
     * @return mixed параметр елемента массива parameters
     * @throw Exception указанный параметр не найден
     */
    public function getParameter($parameterName)
    {
        // проверить параметр в массиве параметров
        if (!isset($this->parameters[$parameterName])) {
            throw new \Exception('Указанный параметр: '.$parameterName.' не найден в массиве параметров.');
        }
        
        // вернуть значение параметра
        return $this->parameters[$parameterName];
    }
    
    /**
     * проверить существование параметра
     * @return boolean - флаг существования параметра в массиве параметров
     * @return true    - параметр найден 
     * @return false   - параметр не найден 
     */
    public function hasParameter($parameterName)
    {
        // проверить параметр в массиве параметров
        if (isset($this->parameters[$parameterName])) {
            // параметр найден
            return true;
        }
        
        // параметр не найден
        return false;
    }
    
    /**
     * установить елемент массива parameters
     * @param string $parameterName - имя добавляемого параметра
     * @param mixed  $parameterValue - знаяение добавляемого параметра
     * @return $this->parameters[$parameterName] - массив всех параметров
     */
    public function setParameter($parameterName, $parameterValue=null)
    {
        // инищиализировать массив параметров
        if (!$this->parameters) {
            $this->parameters = array();
        }
        
        // установить новое знаение параметра
        $this->parameters[$parameterName] = $parameterValue;
        
        // вернуть значение добавленного параметра
        return $this->parameters[$parameterName];
    }
    
}
