<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus;

/**
 * ChainsAllow
 * доступные цепочки изменения статусов
 */
class ChainsAllow
{
    
    /**
     * @var array
     * массив цепочек изменения статусов 
     */
    public static $chains = array(
        
        // waiting - с будильника 
        'waiting_to_completed',
        'waiting_to_canceled',
        
        // completed - с завершено
        'completed_to_waiting',
        
        // canceled - с отмены
        'canceled_to_waiting',
        
    );    
    
}
