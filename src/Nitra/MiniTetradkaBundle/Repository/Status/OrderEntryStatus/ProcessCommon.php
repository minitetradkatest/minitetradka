<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus;

use Nitra\MiniTetradkaBundle\Repository\Status\StatusInterface;
use Nitra\MiniTetradkaBundle\Repository\Status\StatusParameters;
use Nitra\MiniTetradkaBundle\Entity\OrderEntry;

/**
 * ProcessCommon
 * обший класс обработчиков цепоцек
 */
class ProcessCommon extends StatusParameters implements StatusInterface
{
    
    /**
     * @var OrderEntry $orderEntry позиция заказа
     */
    protected $orderEntry;
    
    /**
     * Конструктор
     */
    public function __construct(OrderEntry $orderEntry, array $parameters = null)
    {
        // установить позицию заказ 
        $this->orderEntry = $orderEntry;
        
        // установить параметры выполнения цепочки
        $this->setParameters($parameters);
    }
    
    /**
     * {@inheritDoc}
     */
    public function isValid()
    {
        // валидировать команду
        $errorMessage = $this->validate();
        if ($errorMessage) {
            // валидация не пройдена
            return false;
        }
        
        // валидация пройдена успешно
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    public function validate()
    {
        // валидация пройдена успешно
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    public function process()
    {
        // валидировать команду
        $errorMessage = $this->validate();
        if ($errorMessage) {
            throw new \Exception($errorMessage);
        }
        
        // цепочка обработана успешно
        return false;
    }
    
}
