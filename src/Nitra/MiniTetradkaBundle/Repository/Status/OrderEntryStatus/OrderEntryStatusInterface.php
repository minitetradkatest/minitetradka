<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus;

/**
 * OrderEntryStatus
 * Интерфейс сервиса изменения статусов 
 */
interface OrderEntryStatusInterface
{
    
    /**
     * установить заказ для которого изменяется статус
     * @param Order $order заказ
     */
    public function setOrderEntry(\Nitra\MiniTetradkaBundle\Entity\OrderEntry $orderEntry);
    
    /**
     * установить статус позиции заказа
     * @param OrderEntryStatus $orderEntryStatus статус позиции закза
     */
    public function setToStatus(\Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $orderEntryStatus);
    
    /**
     * установить статус позиции заказа который был отображен пользователю
     * @param OrderEntryStatus $orderEntryStatus статус позиции закза
     */
    public function setDisplayedStatus(\Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus $orderEntryStatus);
    
}
