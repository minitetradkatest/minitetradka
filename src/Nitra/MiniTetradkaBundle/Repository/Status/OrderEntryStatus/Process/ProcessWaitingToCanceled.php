<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\ProcessCommon;

/**
 * ProcessWaitingToCanceled
 * обработчик цепочки waiting_to_canceled
 * @author user
 */
class ProcessWaitingToCanceled extends ProcessCommon
{
    
}
