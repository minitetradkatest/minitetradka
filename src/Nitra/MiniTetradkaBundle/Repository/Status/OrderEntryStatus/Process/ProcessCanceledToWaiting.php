<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\ProcessCommon;

/**
 * ProcessCanceledToWaiting
 * обработчик цепочки canceled_to_waiting
 * @author user
 */
class ProcessCanceledToWaiting extends ProcessCommon
{
    
}
