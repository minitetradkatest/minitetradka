<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\ProcessCommon;

/**
 * ProcessCompletedToWaiting
 * обработчик цепочки completed_to_waiting
 * @author user
 */
class ProcessCompletedToWaiting extends ProcessCommon
{
    
}
