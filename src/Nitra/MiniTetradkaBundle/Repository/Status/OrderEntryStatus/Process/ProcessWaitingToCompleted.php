<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\Process;

use Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\ProcessCommon;

/**
 * ProcessWaitingToCompleted
 * обработчик цепочки waiting_to_completed
 * @author user
 */
class ProcessWaitingToCompleted extends ProcessCommon
{
    
}
