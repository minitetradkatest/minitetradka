<?php
namespace Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus;

use Nitra\MiniTetradkaBundle\Entity\OrderEntry;
use Nitra\MiniTetradkaBundle\Entity\OrderEntryStatus;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;
use Nitra\MiniTetradkaBundle\Repository\Status\StatusManage;
use Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\ChainsAllow;
use Nitra\MiniTetradkaBundle\Repository\Status\OrderEntryStatus\OrderEntryStatusInterface;

/**
 * OrderEntryStatusManage
 */
class OrderEntryStatusManage extends StatusManage implements OrderEntryStatusInterface
{
    
    /**
     * @var OrderEntry
     * Позиция заказа
     */
    protected $orderEntry;
    
    /**
     * @var OrderEntryStatus 
     * устанавливаемы статус
     */
    protected $toStatus;
    
    /**
     * @var OrderEntryStatus
     * отображенный пользователю статус
     */
    protected $displayedStatus;
    
    /**
     * @var OrderEntryStatus
     * статус по умолчанию инициализиуется после первого получения
     */
    private $defaultStatus;
    
    /**
     * {@inheritDoc}
     */
    public function setOrderEntry(OrderEntry $orderEntry)
    {
        $this->orderEntry = $orderEntry;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setToStatus(OrderEntryStatus $status)
    {
        $this->toStatus = $status;
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setDisplayedStatus(OrderEntryStatus $status)
    {
        $this->displayedStatus = $status;
        return $this;
    }
    
    /**
     * получить статус по умолчанию
     * @return OrderEntryStatus 
     * @throw \Exception ошибка получения статуса по умолчанию
     */
    public function getDefaultStatus()
    {
        // проверить если статус по умолчанию не получали ранее
        // предотвращене получения повторных данных, 
        // уменьшение кол-ва запросов к БД 
        // полезно для случаев когда создается много новых сущностей со статусом по умолчанию 
        if (!$this->defaultStatus) {
            
            // получить статус по умолчанию 
            $this->defaultStatus = NitraGlobals::$em
                ->getRepository('NitraMiniTetradkaBundle:OrderEntryStatus')
                ->findOneBy(array(
                    'methodName' => NitraGlobals::$container->getParameter('nitra_minitetradka_default_order_entry_status')
                ));
            
            // проверить статус
            if (!$this->defaultStatus) {
                // статус не найден
                throw new \Exception('В базе данных не найден статус позиции заказа по умолчанию.');
            }
        }
        
        // вернуть статус по умолчанию
        return $this->defaultStatus;
    }    
    
    /**
     * установить статус позиции заказа по умолчанию
     * @param OrderEntry $orderEntry
     */
    public function setDefaultStatusForOrderEntry(OrderEntry $orderEntry)
    {
        // получить статус по умолчанию
        $status = $this->getDefaultStatus();
        // установить статус по умолчанию 
        $orderEntry->setOrderEntryStatus($status);
    }        
    
    /**
     * получить доступные статусы для позиции заказа $order, 
     * в кторые можно перевести позицию заказа
     * @param OrderEntry $orderEntry - позиция заказа 
     * @return array $OrderEntryStatuses - массив доступных цепочек для позиции заказа
     * @return false доступные цепочки не найдены
     */
    public function getAllowStatuses(OrderEntry $orderEntry)
    {
        // массив доступных цепочек для позиии заказов
        $chains = ChainsAllow::$chains;
        
        // обойти все цепочки найти подходящие цепочки
        $chainArr = array();
        foreach ($chains as $chain) {

            // цепочка из статуса позиции 
            $chainFrom = substr($chain, 0, strlen($orderEntry->getOrderEntryStatus()->getMethodName()));

            // подходящая цепочка
            if ($chainFrom == $orderEntry->getOrderEntryStatus()->getMethodName()) {
                // цепочка в статус позиции 
                $chainTo = substr($chain, strlen($chainFrom . '_to_'));
                // наполнить массив 
                // запоминаем в виде key => value 
                $chainArr[$chainTo] = $chainTo;
            }
        }
        
        // получить массив статусов в которые разрешено переводить из текущего
        $chainArr = array_values($chainArr);
        
        // получить статусы
        if ($chainArr) {
            // получить статусы по по массиву
            $OrderEntryStatuses = NitraGlobals::$em
                ->createQueryBuilder()
                ->select('st.id, st.name, st.methodName, st.cssClass')
                ->from('NitraMiniTetradkaBundle:OrderEntryStatus', 'st', 'st.id')
                ->where('st.methodName IN(:statuses)')->setParameter('statuses', $chainArr)
                ->getQuery()
                ->getArrayResult();
            
            // вернуть массив статусов 
            return $OrderEntryStatuses;
        }
        
        // доступные статусы не найдены
        return false;
    }    
    
    
    
    
}
