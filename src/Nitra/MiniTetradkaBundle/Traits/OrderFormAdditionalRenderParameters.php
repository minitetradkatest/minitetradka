<?php
namespace Nitra\MiniTetradkaBundle\Traits;

use Symfony\Component\Form\Form;
use Nitra\MiniTetradkaBundle\Entity\Order;
use Nitra\MiniTetradkaBundle\Form\Type\Order\EditType;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;
use Nitra\MiniTetradkaBundle\Form\Type\Order\SearchProductType;
   

/**
 * OrderFormAdditionalParameters
 * дополнительные параметры формы заказа
 */
trait OrderFormAdditionalRenderParameters
{
    
    /**
     * Получить массив доп. параметров передаваемых в шаблон
     * @param Form $form the valid form
     * @param Order $order - заказ для которого строится массив доп. параметров
     * @return array
     */
    public function getOrderFormAdditionalRenderParameters(EditType $formType, Order $Order)
    {
        
        // получть данные формы 
        $formData = NitraGlobals::$container->get('request')->get($formType->getName());
        
        // массив названий продуктов
        $productNames = array();
        
        // массив ID продуктов 
        $productIds = array();
        
        // наполнить массив ID продуктов из форомы заказов
        if (isset($formData['orderEntry']) && $formData['orderEntry']) {
            foreach($formData['orderEntry'] as $orderEntry) {
                $productIds[] = $orderEntry['productId'];
            }
        }
        
        // наполнить массив ID продуктов из объекта заказа
        foreach($Order->getOrderEntry() as $entry) {
            $productIds[] = $entry->getProductId();
        }
        
        // получить список продуктов по массиву ID 
        $productIds = array_unique($productIds);
        if ($productIds) {
            // получить продукты
            $products = NitraGlobals::$dm
                ->createQueryBuilder('NitraMiniTetradkaBundle:Product')
                ->field('id')->in($productIds)             
                ->getQuery()
                ->execute();
            
            // наполнить массив имен продуктов
            if ($products) {
                foreach($products as $product ) {
                    // запомнить массив названий продуктов
                    $productNames[$product->getId()] = array(
                        'id' => $product->getId(),
                        'name' => (string)$product,
                        'article' => $product->getArticle(),
                    );
                }
            }
        }
        
        // форма поиска продуктов
        $searchProduct = NitraGlobals::$container
            ->get('form.factory')
            ->create(new SearchProductType());
        
        // вернуть массив доп параметров передаваемых 
        // для отобрадение формы заказа
        return array(
            // форма фоиска продуктов
            'searchProductForm' => $searchProduct->createView(),
            // массив названий продуктов
            'productNames' => $productNames,
        );
    }
    
}
