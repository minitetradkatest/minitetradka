<?php
namespace Nitra\MiniTetradkaBundle\Form\Validator;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;

/**
 * валидировать все дочерние элементы формы
 * используем данный метод вместо setDefaultOptions:: cascade_validation => true
 * потому что cascade_validation выдает повторное отображени ошибки дочерней записи
 * например quantity = 33w отображается 2 ошибки вместо одной
 */
class FormChildrenValidator
{
    /**
     * метод валидатора
     * @param Form $form валидируемы элемент формы
     * @return boolean - результат валидации формы
     *          true  - форма валидная
     *          false - оформа валидацию не прошла
     */
    
    public static function validate(Form $form)
    {
        
        // счетчик кол-ва ошибок формы 
        // static выполняется рекурсивно
        static $errorsCounter = 0;
        
        // рекурсивный вызов функции если форма содержит свои вложенные формы
        foreach($form->all() as $child) {
            if($child->all()) {
                self::validate($child);
            }
        }
        
        // получить валидатор
        $validator = NitraGlobals::$container->get('validator');
        
        // валидация формы
        $errors = $validator->validate($form->getData());
        
        // добавление в форму ошибок для отображения (только по полям указаным в форме)
        foreach($errors as $error) {
            foreach($form->all() as $formField) {
                if($error->getPropertyPath() === (string) $formField->getPropertyPath()) {
                    // добавить ошибку для виджета
                    $formField->addError(new FormError($error->getMessage()));
                    // увеличить счетчик ошибок
                    ++$errorsCounter;
                }
            }
        }
        
        // ошибка валидации 
        if ($errorsCounter) {
            return false;
        }
        
        // валидация пройдена успешно
        return true;
    }
}
