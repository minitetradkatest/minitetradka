<?php
namespace Nitra\MiniTetradkaBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;

/**
 * OrderIdToEntityTransformer
 */
class OrderIdToEntityTransformer implements DataTransformerInterface
{

    /**
     * Transforms an object to a string.
     * @param  object|null $transformObj
     * @return string
     */
    public function transform($transformObj)
    {
        if (null === $transformObj) {
            return '';
        }
        
        return $transformObj->getId();
    }

    /**
     * Transforms a string (id) to an object.
     * @param  string $id
     * @return $transformObj|null
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
        
        $transformObj = NitraGlobals::$em
            ->getRepository('NitraMiniTetradkaBundle:Order')
            ->find($id)
        ;

        if (null === $transformObj) {
            throw new TransformationFailedException(sprintf('An object with Id "%s" does not exist!', $id));
        }

        return $transformObj;
    }
}
