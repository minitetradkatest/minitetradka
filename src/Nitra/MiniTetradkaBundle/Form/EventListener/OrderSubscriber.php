<?php
namespace Nitra\MiniTetradkaBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;
use Nitra\MiniTetradkaBundle\Entity\Buyer;

/**
 * OrderSubscriber
 * Обработчик событий для формы заказа 
 */
class OrderSubscriber implements EventSubscriberInterface
{    
    /**
     * @var OrderStatusService
     * сервис изменения статусов заказа
     */
    protected  $nitraOrderStatus;
    
    /**
     * @var OrderEntryStatusService
     * сервис изменения статусов позиций заказа
     */
    protected  $nitraOrderEntryStatus;
        
    /**
     * Контсруктор
     */
    public function __construct()
    {
        // получить сервис изменения статусов заказа
        $this->nitraOrderStatus = NitraGlobals::$container->get('nitra.minitetradka.order_status');
        
        // получить сервис изменения статусов позици заказа
        $this->nitraOrderEntryStatus = NitraGlobals::$container->get('nitra.minitetradka.order_entry_status');
    }
    
    /**
     * получить массив событий
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::SUBMIT => 'submit');
    }
    
    /**
     * submit
     * устанавливаем данные формы по умолчанию 
     */
    public function submit(FormEvent $event)
    {
        
        // получить заказ
        $order = $event->getData();
        
        // массив удаляемых позиций заказа
        $orderEntryRemove = array();
        if ($order->getId()) {
            // если заказ сохранен в БД 
            // получить массив позиций заказа сохраненных в БД
            $orderEntryRemove = NitraGlobals::$em->createQueryBuilder()
                ->select('oe')
                ->from('NitraMiniTetradkaBundle:OrderEntry', 'oe', 'oe.id')
                ->where('oe.order = :order')->setParameter('order', $order->getId())
                ->getQuery()
                ->execute();
        }
        
        // если новый заказ, 
        // если заказ не сохранен в БД
        if (!$order->getId()) {
            // установить статус заказа по умолчанию 
            $this->nitraOrderStatus->setDefaultStatusForOrder($order);
        }
        
        // получить валюту поу молчанию
        $currency = NitraGlobals::$em
            ->getRepository('NitraMiniTetradkaBundle:Currency')
            ->getDefaultCurrency();
        
        // обойти все позиции заказа 
        foreach ($order->getOrderEntry() as $orderEntry) {
            
            // установить данные по умолчанию 
            $orderEntry->setPriceIn($orderEntry->getPriceOut());
            $orderEntry->setPriceRecommended($orderEntry->getPriceOut());
            
            // если новая позиция заказа, не сохранена в БД
            if (!$orderEntry->getId()) {
                $orderEntry->setOrder($order);
                $orderEntry->setCurrency($currency);
                // установить статус позиции заказа по умолчанию 
                $this->nitraOrderEntryStatus->setDefaultStatusForOrderEntry($orderEntry);
                // persist
                NitraGlobals::$em->persist($orderEntry);
                
            } else {
                // редактирование позиции заказа 
                // Отсеиваем позиции, которые пришли в посте
                if (isset($orderEntryRemove[$orderEntry->getId()])) {
                    // позиция заказе удаляется из массива позиций на удаление
                    unset($orderEntryRemove[$orderEntry->getId()]);
                }
            }
            
        }
        
        // Удаляем позиции из заказа
        foreach ($orderEntryRemove as $orderEntry) {
            NitraGlobals::$em->remove($orderEntry);
        }
        
        // получить форму
        $form = $event->getForm();
        
        // получть данные формы 
        $formData = NitraGlobals::$container->get('request')->get($form->getName());
        
        // вырезать + из номера телефона
        $formData['buyer_phone']['name'] = preg_replace('/[^0-9]/', '', $formData['buyer_phone']['name']);
        
        // покупатель заказа 
        // по умолчанию покупатель не найден 
        // будет создан новый покупатель
        $buyer = null;
        
        // получить покупателя по автокомплит
        // получитьпокупателя по имени 
        if ($formData['buyer_name']['id']) {
            $buyer = NitraGlobals::$em
                ->getRepository('NitraMiniTetradkaBundle:Buyer')
                ->find($formData['buyer_name']['id']);
        } 
        // получитьпокупателя по телефону
        elseif($formData['buyer_phone']['id']) {
            $buyer = NitraGlobals::$em
                ->getRepository('NitraMiniTetradkaBundle:Buyer')
                ->find($formData['buyer_phone']['id']);
        }
        
        // покупатель не найден
        // создать нового покупателя
        if (!$buyer) {
            // создать нового покупателя
            $buyer = new Buyer();
            $buyer->setName($formData['buyer_name']['name']);
            $buyer->setPhone($formData['buyer_phone']['name']);
            // persist
            NitraGlobals::$em->persist($buyer);
            
        } else {
            // покупатель найден 
            
            // обновить имя покупателя если изменилось
            if ($buyer->getName() != $formData['buyer_name']['name']) {
                $buyer->setName($formData['buyer_name']['name']);
            }
            
            // обновить телефон покупателя, если изменилось
            if ($buyer->getPhone() != $formData['buyer_phone']['name']) {
                $buyer->setPhone($formData['buyer_phone']['name']);
            }
            
        }
        
        // установить покупателя для заказа
        $order->setBuyer($buyer);
        
        // автоинкримент коментария 
        if ($formData['commentAdd']) { 
            // получить пользователя
            $user = NitraGlobals::getUser();
            $username = ($user) 
                ? $user->getUsername()
                : '';
            
            // добавить коментарий
            $order->addComment($formData['commentAdd'], $username);
        }
        
    }
    
}
