<?php
namespace Nitra\MiniTetradkaBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Nitra\MiniTetradkaBundle\Form\Validator\FormChildrenValidator;

/**
 * FormChildrenValidSubscriber
 * валидировать все дочерние элементы формы
 */
class FormChildrenValidSubscriber implements EventSubscriberInterface
{    
    
    /**
     * получить массив событий
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::SUBMIT => 'submit');
    }
    
    /**
     * submit
     * устанавливаем данные формы по умолчанию 
     */
    public function submit(FormEvent $event)
    {
        // получить форму
        $form = $event->getForm();
        
        // валидировать все дочерние элементы формы
        FormChildrenValidator::validate($form);
    }
    
}

