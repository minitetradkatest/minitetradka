<?php
namespace Nitra\MiniTetradkaBundle\Form\Type\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * OrderEntryType
 * Форма позиции заказа
 */
class OrderEntryType extends AbstractType
{
    
    /**
     * построить форму позиции заказа
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', 'text', array('attr' => array('class' => 'quantity')))
            ->add('priceOut', 'text', array('attr' => array('class' => 'price_out')))
            ->add('productId', 'hidden')
        ;
    }
    
    /**
     * Получить имя формы
     */
    public function getName()
    {
        return 'order_entry';
    }
    
    /**
     * установить данные формы по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nitra\MiniTetradkaBundle\Entity\OrderEntry'
        ));
    }

}
