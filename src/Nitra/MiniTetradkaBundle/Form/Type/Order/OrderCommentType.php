<?php
namespace Nitra\MiniTetradkaBundle\Form\Type\Order;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Nitra\MiniTetradkaBundle\Form\DataTransformer\OrderIdToEntityTransformer;

/**
 * форма добавления комментария
 */
class OrderCommentType extends AbstractType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // ID заказа
        $builder->add('id', 'hidden', array(
            'required' => true,
            'constraints' => array(new NotBlank(array('message' => 'Не указан идентификатор.'))),
        ));
        // установить трансформер для идентификатора
        $builder
            ->get('id')
            ->addModelTransformer(new OrderIdToEntityTransformer());
        
        // комментарий
        $builder->add('comment', 'textarea', array(
            'required' => true,
            'constraints' => array(new NotBlank(array('message' => 'Не указан комментарий.'))),
            'label' => 'Комментарий', 
        ));
    }
    
    /**
     * form name
     */
    public function getName()
    {
        return 'order_comment';
    }
    
}
