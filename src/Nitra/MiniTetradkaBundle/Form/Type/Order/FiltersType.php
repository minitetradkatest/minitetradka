<?php

namespace Nitra\MiniTetradkaBundle\Form\Type\Order;

use Admingenerated\NitraMiniTetradkaBundle\Form\BaseOrderType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\MiniTetradkaBundle\Form\Type\AjaxAutocompleteType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;

/**
 * FiltersType
 */
class FiltersType extends BaseFiltersType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        parent::buildForm($builder, $options);

        // виджет имя покупателя автокомплит
        $afterJavaScriptBuyerName = "  
            json_data = eval('('+row.extra[1]+')');
            $('#order_buyer_phone_name').val( json_data.phone );";
        $builder->add('buyer', new AjaxAutocompleteType(), array_merge(array(
            'label' => 'Покупатель',
            'required' => false,
            'selected' => key_exists('buyer', $builder->getData()) ? $builder->getData()['buyer'] : array(),
            'autocompleteActionRouting' => 'Nitra_MiniTetradkaBundle_Buyer_autocomplete',
            'afterItemSelectJavascript' => $afterJavaScriptBuyerName,
            'extraParamsString' => "searchBy: 'name'",
                        ), $options['buyer_name_options']
        ));

        // виджет название товара
        $formOptions = $this->getFormOption('productName', array('required' => false, 'label' => 'Название товара', 'translation_domain' => 'Admin',));
        $builder->add('productName', 'text', $formOptions);

        $formOptions = $this->getFormOption('orderEntryStatus', array(
            'multiple' => false,
            'required' => false,
            'choices' => NitraGlobals::$em->createQueryBuilder()
                    ->select('oes.id , oes.name')
                    ->from('NitraMiniTetradkaBundle:OrderEntryStatus', 'oes')
                    ->getQuery()
                    ->execute(array(), 'KeyPair'),
            'label' => 'Статус позиции заказа', 'translation_domain' => 'Admin',
        ));
        $builder->add('orderEntryStatus', 'choice', $formOptions);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        // установить $options по умолчанию
        $resolver->setDefaults(array(
            // например quantity = 33w отображается 2 ошибки вместо одной
            'cascade_validation' => true,
            'buyer' => array(),
            // class
            'data_class' => null,
            // покупатель
            'buyer_object' => null,
            // автокомплит имя покупателя
            'buyer_name_options' => array('data' => array('name' => null, 'id' => null,)),
        ));
    }

}
