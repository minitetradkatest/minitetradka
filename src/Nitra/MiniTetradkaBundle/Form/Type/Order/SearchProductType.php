<?php
namespace Nitra\MiniTetradkaBundle\Form\Type\Order;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Nitra\MiniTetradkaBundle\Lib\NitraGlobals;

/**
 * SearchProductType
 * Поиск продуктов
 */
class SearchProductType extends AbstractType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // категория товаров
        $builder->add('categoryId', 'choice', array(
            'required' => false, 
            'choices' => NitraGlobals::$dm->getRepository('NitraMiniTetradkaBundle:Category')->getAllToChoice(), 
            'label' => 'Категория'
        ));
        
        // строка поиска
        $builder->add('name', 'text', array(
            'required' => false,
            'label' => 'Поиск',
            'attr' => array(
                'autocomplete' => 'off',
            ),
        ));
        
        // отобржаема страница поиска
        $builder->add('page', 'hidden', array('required' => false));
    }
    
    /**
     * Получить имя формы
     * @return string
     */
    public function getName()
    {
        return 'search_product';
    }
    
}
