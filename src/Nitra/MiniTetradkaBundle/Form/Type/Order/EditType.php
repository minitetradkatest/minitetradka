<?php
namespace Nitra\MiniTetradkaBundle\Form\Type\Order;

use Admingenerated\NitraMiniTetradkaBundle\Form\BaseOrderType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Nitra\MiniTetradkaBundle\Form\Type\AjaxAutocompleteType;
use Nitra\MiniTetradkaBundle\Form\Type\OrderEntry\OrderEntryType;
use Nitra\MiniTetradkaBundle\Form\EventListener\OrderSubscriber;

/**
 * EditType
 */
class EditType extends BaseEditType
{
    
    /**
     * buildForm
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        // если заказ был сохранен ранее
        if ($options['data']->getId()) {
            // автокомплит объект полкупателя
            $options['buyer_object'] = $options['data']->getBuyer();

            // автокомплит  имя покупателя
            $options['buyer_name_options']['data']['name'] = $options['data']->getBuyer()->getName();
            $options['buyer_name_options']['data']['id'] = $options['data']->getBuyer()->getId();

            // автокомплит телефон покупателя
            $options['buyer_phone_options']['data']['name'] = $options['data']->getBuyer()->getPhone();
            $options['buyer_phone_options']['data']['id'] = $options['data']->getBuyer()->getId();
        }
        
        // родитель создать форму
        parent::buildForm($builder, $options);
        
        // автокомплит поле имя покупателя
        $afterJavaScriptBuyerName = "  
            json_data = eval('('+row.extra[2]+')');           
            $('#order_buyer_phone_name').val( json_data.phone );";
        $builder->add('buyer_name', new AjaxAutocompleteType(), array_merge(array(
            'label' => 'Покупатель',
            'required' => true,
            'mapped' => false,
            'autocompleteActionRouting' => 'Nitra_MiniTetradkaBundle_Buyer_autocomplete',
            'afterItemSelectJavascript' => $afterJavaScriptBuyerName,
            'extraParamsString' => "searchBy: 'name', addNew: true",
            ), $options['buyer_name_options']
        ));
        
        // автокомплит поле телефон покупателя
        $afterJavaScriptBuyerPhone = "  
            json_data = eval('('+row.extra[2]+')');           
            $('#order_buyer_name_name').val( json_data.name );
            $('#order_buyer_name_id').val( json_data.id );";
        $builder->add('buyer_phone', new AjaxAutocompleteType(), array_merge(array(
            'required' => true,
            'mapped' => false,
            'label' => 'Телефон',
            'autocompleteActionRouting' => 'Nitra_MiniTetradkaBundle_Buyer_autocomplete',
            'afterItemSelectJavascript' => $afterJavaScriptBuyerPhone,
            'extraParamsString' => "searchBy: 'phone'",
            ), $options['buyer_phone_options']
        ));
        
        // виджет автоинкремента коментария к заказу
        $builder->add('commentAdd', 'text', array('required' => false, 'mapped' => false, 'label' => 'Добавить комментарий'));
        
        // виджет позиции заказа
        $builder->add('orderEntry', 'collection', array(
            'type' => new OrderEntryType(),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false
        ));
        
        // добавить Subscriber формы заказа
        $builder->addEventSubscriber(new OrderSubscriber());
    }
    
    /**
     * получить имя формы
     */
    public function getName()
    {
        return 'order';
    }
    
    /**
     * установить массив $options по умолчанию
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        
        // установить $options по умолчанию
        $resolver->setDefaults(array(
            // например quantity = 33w отображается 2 ошибки вместо одной
            'cascade_validation' => true,
            // class
            'data_class' => 'Nitra\MiniTetradkaBundle\Entity\Order',
            // покупатель
            'buyer_object' => null,
            // автокомплит имя покупателя
            'buyer_name_options' => array('data' => array('name' => null, 'id' => null,)),
            // автокомплит телефон покупателя
            'buyer_phone_options' => array('data' => array('name' => null,'id' => null,)),
        ));
        
    }
    
}
