<?php

//namespace Nitra\MiniTetradkaBundle\Tests;
//
//use Symfony\Bundle\FrameworkBundle\Console\Application;
//
///**
// * DataBaseSetupTrait
// * Trait загрузки фикстур для тестов
// **/
//trait DataBaseSetupTrait
//{
//    
//    /**
//     * @var \Symfony\Bundle\FrameworkBundle\Console\Application
//     */
//    protected static $application;
//    
//    /**
//     * It will run before any setUps and tests in given test suite
//     * This hook will drop current schema, creat schema and load fixtures
//     * then it will create a copy of the databse, so it will be used in the future tests in this suite
//     */
//    public static function setUpBeforeClass()
//    {
//        static::bootstrapApplication();
//    }
//    
//    /**
//     * Rebuilds (provides clean instance of database) for each test
//     */
//    public function setUp()
//    {
//        parent::setup();
//        static::buildDatabase();
//    }
//    
//    /**
//     * Получить класс реализующий kernel
//     * @return string namespace класса
//     */
//    protected static function getKernelClass()
//    {
//        require_once __DIR__ . '/../../../../app/bootstrap.php.cache';
//        require_once __DIR__ . '/../../../../app/AppKernel.php';
//        return '\AppKernel';
//    }
//    
//    /**
//     * Bootstraps console application. It's needed to run commands from the code
//     */
//    protected function bootstrapApplication()
//    {
//        static::$kernel = static::createKernel();
//        static::$kernel->boot();
//        static::$application = new Application(static::$kernel);
//        static::$application->setAutoExit(false);        
//    }
//    
//    /**
//     * After all tests in given test suite it will remove database copy
//     * Because of this next test suite needs to create its own
//     */
//    public static function tearDownAfterClass()
//    {
//    }
// 
//    /**
//     * Построить базу данных 
//     * Runs console commands: (all with -q and -e=test)
//     * @param boolean $dropDataIfExists - удалить данные БД 
//     */
//    protected function buildDatabase($dropDataIfExists = false)
//    {
//        
//        // проверить наличие таблшиц в БД
//        // если в БД нет таблиц переключаем флаг $dropDataIfExists на true
//        // данные будут создваться принулительно
//        if (!static::getConnection()->getSchemaManager()->listTableNames()) {
//            $dropDataIfExists = true;
//        }
//        
//        // проверить флаг создания данных в БД 
//        if ($dropDataIfExists !== true) {
//            // данные были созданы ранее
//            // при запуске первого дочернего теста
//            // прервать выполнение
//            return;
//        }
//        
//        // наполнить БД данными из фикстур
//        static::runConsole('doctrine:schema:drop', array('--force' => true));
//        static::runConsole('doctrine:schema:update', array('--force' => true));
//        
//        // получить фикстуры
//        $fixtures = static::getFixtures();
//        foreach($fixtures as $fixture) {
//            static::runConsole('doctrine:fixtures:load', array('-n' => true, '--append' => true, '--fixtures' => $fixture));
//        }
//    }
//    
//    /**
//     * Получить массив загружаемых фикстур
//     * @return array - массив путей к диреториям фикстур
//     */
//    protected function getFixtures()
//    {
//        // вернуть массив путей 
//        return array(
//            // фикстуры тетрадки
//            __DIR__ . '/../DataFixtures/',
//            // фикстуры GeoBundle
//            __DIR__ .'/../../../../vendor/nitra/geo-bundle/Nitra/GeoBundle/DataFixtures/',
//        );
//    }
//  
//    /**
//     * Получить Container
//     * @return \Symfony\Component\DependencyInjection\ContainerInterface
//     */
//    public static function getContainer()
//    {
//        return static::$application->getKernel()->getContainer();
//    }
//    
//    /**
//     * Получить сервис по ID
//     * @param string $id The service id
//     * @return mixed service
//     */
//    public static function get($id)
//    {
//        if (!static::getContainer()->has($id)) {
//            throw new \LogicException('Сервис '.$id.' не зарегистрирован.');
//        }
//        // вернуть сервис
//        return static::getContainer()->get($id);
//    }
//        
//    /**
//     * Получить connection
//     * @return \Doctrine\DBAL\Connection
//     */
//    public static function getConnection()
//    {
//        return static::get('doctrine.dbal.default_connection');
//    }
//    
//    /**
//     * Получить EntityManager 
//     * @return \Doctrine\ORM\EntityManager
//     */
//    public static function getEntityManager()
//    {
//        return static::get('doctrine.orm.entity_manager');
//    }    
//
//    /**
//     * Получить DocumentManager 
//     * @return \Doctrine\ODM\MongoDB\DocumentManager
//     */
//    public static function getDocumentManager()
//    {
//        return static::get('doctrine_mongodb.odm.document_manager');
//    }    
//    
//    /**
//     * It always run with given environment and in quiet mode (no output on the console)
//     * запустить консольную команду
//     * @param string $command       - название консольной команды
//     * @param array  $addOptions    - массив параметров команды
//     */
//    protected function runConsole($command, array $addOptions = array())
//    {
//        // параметры команды
//        $options = array_merge(array(
//            'command'   => $command,
//            '-e'        => static::$application->getKernel()->getEnvironment(),
////            '-q'        => null,
//        ), $addOptions);
//        
//        // выполнитькоманду
//        $input = new \Symfony\Component\Console\Input\ArrayInput($options);
//        $result = self::$application->run($input);
//        if (0 != $result) {
//            throw new \RuntimeException(sprintf('Something has gone wrong, got return code %d for command %s', $result, $command));
//        }
//        
//        // вернуть результат выполнения
//        return $result;
//    }
//    
//}
