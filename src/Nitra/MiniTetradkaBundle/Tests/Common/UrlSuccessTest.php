<?php

namespace Nitra\MiniTetradkaBundle\Tests\Common;

/**
 * UrlSuccessTest
 * Проверить доступность Url 
 */
class UrlSuccessTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    
    /**
     * получить массив проверяемых url
     * @return array
     */
    public function providerUrls()
    {
        return array(
            
            array('/'),
            
            array('/login'),
            array('/logout'),
            
            array('/geo-region'),
            array('/geo-region/new'),
            array('/geo-region/filter'),
            
            array('/geo-city'),
            array('/geo-city/new'),
            array('/geo-city/filter'),
            array('/city/get-all-cities'),
            
//            // не во всех минитетрадках есть данные раброчие роуты
//            array('/city/get-delivery-cities'),
//            array('/city/get-delivery-cities-with-tk'),
//            array('/city/get-delivery-warehouses'),
            
            array('/currency'),
            array('/currency/new'),
            array('/currency/filter'),
            array('/currency/eur/edit'),
            array('/currency/rub/edit'),
            array('/currency/uah/edit'),
            array('/currency/usd/edit'),
            array('/currency/get-all-currencies'),
            
            array('/buyer'),
            array('/buyer/new'),
            array('/buyer/filter'),
            array('/buyer/get-buyer-info-site'),
            array('/buyer-autocomplete'),
            
            array('/order'),
            array('/order/new'),
            array('/order/filter'),
            array('/order/comment-add'),
            array('/order-search-product'),
            array('/order-entry-status/set-status'),
            array('/order-status/set-status'),
            array('/order/buyers-synchronizer'),
            array('/order/update-buyer'),
            array('/order/get-closest-warehouse-for-product'),
            array('/create-order-from-site'),
            
        );
    }

    /** 
     * @dataProvider providerUrls 
     */
    public function testPageIsSuccessful($url)
    {
        $client = static::createClient();
        $client->request('GET', $url);
        
        // флаг доступности ссылки 
        $isSuccessful = ($client->getResponse()->isSuccessful() || $client->getResponse()->isRedirection())
            ? true 
            : false;
        
        // проверить флаг доступности ссылки
        $this->assertTrue($isSuccessful);
    }
    
//    public function testPageSuccess()
//    {
//        
//        $client = static::createClient();
//        $client->request('GET', '/order');
////        $client->followRedirect();
////        $this->assertTrue($client->getResponse()->isSuccessful());
////        $client->request('GET', '/login');
////        $this->assertTrue($client->getResponse()->isSuccessful());
////        $client->request('GET', '/logout');
////        $this->assertTrue($client->getResponse()->isSuccessful());
//        
////        print '<pre>'; var_dump($client->getRequest()->isSuccessful()); print '</pre>';
//        print '<pre>'; print_r($client->getRequest()->attributes->all()); print '</pre>';
//        print '<pre>'; var_dump($client->getResponse()->getStatusCode()); print '</pre>';
//        print '<pre>'; var_dump($client->getResponse()->isSuccessful()); print '</pre>';
////        die('<br/>line:'.__LINE__.'<br/>file: '.__FILE__);
//        $this->assertTrue(true);
//        
//        
////        $this->assertTrue(true);
////        echo "\n", "statusCode: ", $client->getResponse()->getStatusCode(), " ", $url ," getStatusCode "; var_dump($client->getResponse()->isSuccessful());
////        echo "\n" ;
//    }
    
    
}
