<?php

namespace Nitra\MiniTetradkaBundle\Tests\Common;

/**
 * FixturesOrderStatusesTest
 * Тест загруженных фикстур статусов заказов
 */
class FixturesOrderStatusesTest extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{
    
    /**
     * Получить статусы зказов в БД
     * @param string $entityName - имя сущности статусов
     * @return array (id => methodName)
     */
    protected static function getActualStatuses($entityName)
    {
        // получить записи из БД 
        $rows = static::getEntityManager()
            ->createQueryBuilder()
            ->select('s.id, s.methodName')
            ->from('NitraMiniTetradkaBundle:'.$entityName, 's', 's.methodName')
            ->getQuery()
            ->getArrayResult();
        
        // результирующий массив
        $result = array();
        foreach($rows as $row) {
            $result[] = $row['methodName'];
        }
        
        // вернуть результируюший массив
        return $result;
    }

    /**
     * Проверить статусы заказов в БД
     */
    public function testGetActualOrderStatuses()
    {
        $rows = $this->getActualStatuses('OrderStatus');
        $this->assertCount(4, $rows);
        // вернуть статусы 
        return $rows;
    }
    
    /**
     * Проверить статусы заказов эталоны
     * @depends testGetActualOrderStatuses
     */
    public function testGetExpectedOrderStatuses()
    {
        $rows = array('waiting', 'ordered', 'completed', 'canceled',);
        $this->assertCount(4, $rows);
        // вернуть статусы 
        return $rows;
    }
    
    /**
     * Проверить статусы позиций заказов в БД
     */
    public function testGetActualOrderEntryStatuses()
    {
        $rows = $this->getActualStatuses('OrderEntryStatus');
        $this->assertCount(3, $rows);
        // вернуть статусы 
        return $rows;
    }
    
    /**
     * Проверить статусы позиций заказов эталоны
     * @depends testGetActualOrderEntryStatuses
     */
    public function testGetExpectedOrderEntryStatuses()
    {
        $rows = array('waiting', 'completed', 'canceled',);
        $this->assertCount(3, $rows);
        // вернуть статусы 
        return $rows;
    }
    
    
    /**
     * Проверить статусы заказаов
     * @depends testGetExpectedOrderStatuses
     * @depends testGetActualOrderStatuses
     */
    public function testOrderStatuses($expectedOrderStatuses, $actualOrderStatuses)
    {
        $this->assertEquals($expectedOrderStatuses, $actualOrderStatuses);
    }
    
    /**
     * Проверить статусы позиций заказаов
     * @depends testGetExpectedOrderEntryStatuses
     * @depends testGetActualOrderEntryStatuses
     */
    public function testOrderEntryStatuses($expectedOrderEntryStatuses, $actualOrderEntryStatuses)
    {
        $this->assertEquals($expectedOrderEntryStatuses, $actualOrderEntryStatuses);
    }
    
}
