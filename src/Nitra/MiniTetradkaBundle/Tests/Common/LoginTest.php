<?php

namespace Nitra\MiniTetradkaBundle\Tests\Common;

/**
 * LoginTest
 * Тест авторизации пользователя
 */
class LoginTest extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{
    
    /**
     * Данные авторизации
     * @return array 
     */
    public function loginProvider()
    {
        return array(
            array(false, 'wrong-username', 'wrong-password'),
            array(true, 'admin', 'admin'),
        );
    }
    
    /**
     * Тестирование не правильной авторизации
     * @dataProvider    loginProvider
     */
    public function testLogin($isOk, $username, $password)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        
        // проверить контроллер авторизации
        $this->assertEquals('FOS\UserBundle\Controller\SecurityController::loginAction', $client->getRequest()->attributes->get('_controller'));
        
        // ввести данные пользователя
        $form = $crawler->selectButton('Войти')->form(array(
            '_username' => $username,
            '_password' => $password,
        ));
        
        // клик вход в систему
        $client->submit($form);
        
        // првоерить контроллер обработчика авторизации
        $this->assertEquals('FOS\UserBundle\Controller\SecurityController::checkAction', $client->getRequest()->attributes->get('_controller'));
        
        // перенаправить пользователя
        $client->followRedirect();
        
        // если не правилная авторизация
        if (!$isOk) {
            // проверить результат авторизации
            // рользователь должен быть перенаправлен на форму авторизации
            $this->assertEquals('FOS\UserBundle\Controller\SecurityController::loginAction', $client->getRequest()->attributes->get('_controller'));
            
        } else {
            // авторизация пройдена успешно 

            // атрибуты перенаправления
            $requestAttributes = $client->getRequest()->attributes->all();
            
            // авторизация пройдена успешно 
            // првоерить контроллер перенаправления пользователя
            $this->assertEquals('Symfony\Bundle\FrameworkBundle\Controller\RedirectController::urlRedirectAction', $requestAttributes['_controller']);

            // проверить наличие параметров перенаправления
            $this->assertArrayHasKey('path', $requestAttributes);
            $this->assertArrayHasKey('_route', $requestAttributes);

            // проверить каждый параметр перенаправления
            $this->assertEquals('/order', $requestAttributes['path']);
            $this->assertEquals('root', $requestAttributes['_route']);
        }
    }
    
}
