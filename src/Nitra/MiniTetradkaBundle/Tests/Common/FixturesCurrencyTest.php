<?php

namespace Nitra\MiniTetradkaBundle\Tests\Common;

/**
 * FixturesCurrencyTest
 * Тест загруженных фикстур валют
 */
class FixturesCurrencyTest extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{

    /**
     * Получить массив валют в БД
     * @return array
     */
    public function getActualCurrency()
    {
        // получить записи из БД 
        $rows = static::getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from('NitraMiniTetradkaBundle:Currency', 'c')
            ->getQuery()
            ->getArrayResult();
        
        // результирующий массив
        $result = array();
        foreach($rows as $row) {
            $result[] = $row['code'];
        }
        
        // вернуть запист в БД
        return $result;        
    }
    
    /**
     * Проверить статусы заказов в БД
     */
    public function testGetActualCurrency()
    {
        // получить валбты из БД
        $rows = $this->getActualCurrency();
        $this->assertTrue(count($rows) > 0);
        // вернуть статусы 
        return $rows;
    }
    
    /**
     * Проверить валюты эталоны
     * @depends testGetActualCurrency
     */
    public function testGetExpectedCurrency()
    {
        $rows = array('eur', 'rub', 'uah', 'usd');
        $this->assertCount(4, $rows);
        // вернуть статусы 
        return $rows;
    }
    
    /**
     * Проверить валюты
     * @depends testGetExpectedCurrency
     * @depends testGetActualCurrency
     */
    public function testCurrency($expectedCurrency, $actualCurrency)
    {
        foreach($expectedCurrency as $currencyCode) {
            $this->assertContains($currencyCode, $actualCurrency);
        }
    }
    
}
