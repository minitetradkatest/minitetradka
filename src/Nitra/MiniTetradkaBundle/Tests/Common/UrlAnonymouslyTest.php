<?php

namespace Nitra\MiniTetradkaBundle\Tests\Common;

/**
 * UrlAnonymouslyTest
 * Тест не авторизованных URL
 * Тест Url не требующих авторизации
 */
class UrlAnonymouslyTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    
    /**
     * получить массив проверяемых url
     * @return array
     */
    public function providerUrls()
    {
        return array(
            // сохранение заказов с сайта в тетрадку
            array('/create-order-from-site'),
            
//            // получение инфы с сайта по покупателю
//            array('/buyer/get-buyer-info-site'), 
            
            // синхронизация покупателей (ORM -> ODM)
            array('/order/buyers-synchronizer'),
            
            // принять отредактированные пользователем данные и обновить (редактирование в личном кабинете)
            array('/order/update-buyer'),
            
            // получение json-массива всех городов
            array('/city/get-all-cities'), 
            
            // получение json-массива всех валют
            array('/currency/get-all-currencies'),
            
//            // получение городов в которые осуществляется доставка
//            array('/city/get-delivery-cities'),
//            
//            // получение складов в которые осуществляется доставка
//            array('/city/get-delivery-warehouses'),
            
            // получить ближайший склад на котором нажодится продкут
            array('/order/get-closest-warehouse-for-product'),
            
        );
    }

    /** 
     * @dataProvider providerUrls 
     */
    public function testUrl($url)
    {
        // получить клиента
        $client = static::createClient();
        $client->request('GET', $url);
        // проверить флаг доступности ссылки
        $errorMessage = "Code: ". $client->getResponse()->getStatusCode(). " url: ". $url;
        $this->assertTrue($client->getResponse()->isOk(), $errorMessage);
    }
    
}
