<?php

namespace Nitra\MiniTetradkaBundle\Tests;

/**
 * TetradkaSelenium2Test
 * 
 */
class TetradkaSelenium2Test extends \PHPUnit_Extensions_Selenium2TestCase
{

    /**
     * @var string  override to provide code coverage data from the server
     */
    protected $coverageScriptUrl = PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_TESTS_URL_COVERAGE;

    /**
     */
    protected function setUp()
    {
//        $this->setSeleniumServerRequestsTimeout(600);
//        $this->markTestIncomplete('Would require PHP 5.4 for running .php files on the server');
        $this->setSeleniumServerRequestsTimeout(PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_SERVER_REQUEST_TIMEOUT);
        $this->setBrowser(PHPUNIT_TESTSUITE_EXTENSION_SELENIUM2_BROWSER);
        $this->setBrowserUrl(PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_TESTS_URL);
    }

//    public function run(\PHPUnit_Framework_TestResult $result = NULL)
//    {
//// make sure code coverage collection is enabled
//        if ($result === NULL) {
//            $result = $this->createResult();
//        }
//        if (!$result->getCollectCodeCoverageInformation()) {
//            $result->setCodeCoverage(new PHP_CodeCoverage());
//        }
//        parent::run($result);
//        $result->getCodeCoverage()->clear();
//    }

}
