<?php

namespace Nitra\MiniTetradkaBundle\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;

/**
 * TetradkaTest
 * 
 */
abstract class TetradkaTest extends WebTestCase
{

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Console\Application
     */
    protected static $application;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected static $container;

    /**
     * Клиент 
     * @var \Symfony\Bundle\FrameworkBundle\Client
     */
    protected static $client;

    /**
     * Авторизированный клиент
     * @var boolan
     */
    protected static $isClientLogin;

    /**
     * It will run before any setUps and tests in given test suite
     * This hook will drop current schema, creat schema and load fixtures
     * then it will create a copy of the databse, so it will be used in the future tests in this suite
     */
    public static function setUpBeforeClass()
    {
        static::bootstrapApplication();
    }

    /**
     * Rebuilds (provides clean instance of database) for each test
     */
    public function setUp()
    {
        parent::setup();
        static::buildDatabase();
    }

    /**
     * Получить класс реализующий kernel
     * @return string namespace класса
     */
    protected static function getKernelClass()
    {
        require_once __DIR__ . '/../../../../app/bootstrap.php.cache';
        require_once __DIR__ . '/../../../../app/AppKernel.php';
        return '\AppKernel';
    }

    /**
     * Bootstraps console application. It's needed to run commands from the code
     */
    protected function bootstrapApplication()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        static::$application = new Application(static::$kernel);
        static::$application->setAutoExit(false);
    }

    /**
     * After all tests in given test suite it will remove database copy
     * Because of this next test suite needs to create its own
     */
    public static function tearDownAfterClass()
    {
        
    }

    /**
     * Построить базу данных 
     * Runs console commands: (all with -q and -e=test)
     * @param boolean $dropDataIfExists - удалить данные БД 
     */
    protected function buildDatabase($dropDataIfExists = false)
    {
        // получить connection
        $conn = static::getConnection();

        try {
            // попытка соединения с БД 
            $conn->connect();
        } catch (\Exception $e) {
            // нет соединения с БД 
            // создать БД
            static::runConsole('doctrine:database:create', array('-n' => true));
            $dropDataIfExists = true;
        }

        // проверить наличие таблшиц в БД
        // если в БД нет таблиц переключаем флаг $dropDataIfExists на true
        // данные будут создваться принулительно
        if (!$conn->getSchemaManager()->listTableNames()) {
            $dropDataIfExists = true;
        }

        // проверить флаг создания данных в БД 
        if ($dropDataIfExists !== true) {
            // данные были созданы ранее
            // при запуске первого дочернего теста
            // прервать выполнение
            return;
        }

        // наполнить БД данными из фикстур
        static::runConsole('doctrine:schema:drop', array('--force' => true));
        static::runConsole('doctrine:schema:update', array('--force' => true));

        // получить фикстуры
        $fixtures = static::getFixtures();
        foreach ($fixtures as $fixture) {
            static::runConsole('doctrine:fixtures:load', array('-n' => true, '--append' => true, '--fixtures' => $fixture));
        }
    }

    /**
     * Получить массив загружаемых фикстур
     * @return array - массив путей к диреториям фикстур
     */
    protected function getFixtures()
    {
        // вернуть массив путей 
        return array(
            // фикстуры тетрадки
            __DIR__ . '/../DataFixtures/',
            // фикстуры GeoBundle
            __DIR__ . '/../../../../vendor/nitra/geo-bundle/Nitra/GeoBundle/DataFixtures/',
        );
    }

    /**
     * Получить Container
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public static function getContainer()
    {
        // получить контейнер
        if (static::$container === null) {
            static::$container = static::$application->getKernel()->getContainer();
        }

        // вернуть container
        return static::$container;
    }

    /**
     * Получить сервис по ID
     * @param string $id The service id
     * @return mixed service
     */
    public static function get($id)
    {
        if (!static::getContainer()->has($id)) {
            throw new \LogicException('Сервис ' . $id . ' не зарегистрирован.');
        }

        // вернуть сервис
        return static::getContainer()->get($id);
    }

    /**
     * Получить connection
     * @return \Doctrine\DBAL\Connection
     */
    public static function getConnection()
    {
        // doctrine.orm.default_entity_manager
        return static::get('doctrine.dbal.default_connection');
    }

    /**
     * Получить EntityManager 
     * @return \Doctrine\ORM\EntityManager
     */
    public static function getEntityManager()
    {
        // doctrine_mongodb.odm.default_document_manager
        return static::get('doctrine.orm.entity_manager');
    }

    /**
     * Получить DocumentManager 
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public static function getDocumentManager()
    {
        return static::get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * It always run with given environment and in quiet mode (no output on the console)
     * запустить консольную команду
     * @param string $command       - название консольной команды
     * @param array  $addOptions    - массив параметров команды
     */
    protected function runConsole($command, array $addOptions = array())
    {
        // параметры команды
        $options = array_merge(array(
            'command' => $command,
            '-e' => static::$application->getKernel()->getEnvironment(),
//            '-q'        => null,
            ), $addOptions);

        // выполнитькоманду
        $input = new \Symfony\Component\Console\Input\ArrayInput($options);
        $result = self::$application->run($input);
        if (0 != $result) {
            throw new \RuntimeException(sprintf('Something has gone wrong, got return code %d for command %s', $result, $command));
        }

        // вернуть результат выполнения
        return $result;
    }

    /**
     * GET a Client.
     * Выполнить аторизацию пользователя
     * @param string|object $user The username (like a nickname, email address, etc.), or a UserInterface instance or an object implementing a __toString method.
     * @param RoleInterface[]|string[] $roles       An array of roles
     * @return \Symfony\Bundle\FrameworkBundle\Client A Client instance
     */
    protected static function loginClient($user = 'admin', $roles = array('ROLE_ADMIN'))
    {
        // получить клиента 
        $client = static::getClient();

        // если клиент не авторизован 
        if (static::$isClientLogin !== true) {
            // создать сессию клиента
            $session = $client->getContainer()->get('session');
            $firewall = 'main';
            $token = new \Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken($user, null, $firewall, $roles);
            $session->set('_security_' . $firewall, serialize($token));
            $session->save();
            // создать cookie клиента
            $cookie = new \Symfony\Component\BrowserKit\Cookie($session->getName(), $session->getId());
            $client->getCookieJar()->set($cookie);
            // обновить флаг елинет авторизован
            static::$isClientLogin = true;
        }

        // вернуть авторизированного клиента
        return $client;
    }

    /**
     * GET a Client.
     * @param array $options An array of options to pass to the createKernel class
     * @param array $server  An array of server parameters
     * @return \Symfony\Bundle\FrameworkBundle\Client A Client instance
     */
    protected static function getClient(array $options = array(), array $server = array())
    {
        // клиент не создан
        if (static::$client === null) {
            // создать клиента родителем
            static::$client = static::createClient($options, $server);
        }
        // вернуть клиента
        return static::$client;
    }

    /**
      @param array $params - массив параметров для запроса
      @return Doctrine\ORM\QueryBuilder
     */
    protected function buildQuery(array $params = null)
    {
        // проверить обязательные параметры from
        if (!isset($params['from']) || !$params['from']) {
            throw new \LogicException('Не указан обязательный параметр "from" для запроса');
        }

        // проверить обязательные параметр alias
        if (!isset($params['alias']) || !$params['alias']) {
            throw new \LogicException('Не указан обязательный параметр "alias" для запроса');
        }

        // проверить обязательные параметр indexBy
        if (!isset($params['indexBy']) || !$params['indexBy']) {
            $params['indexBy'] = null;
        }

        // запрос получения кол-ва заказов
        $query = static::getEntityManager()
            ->createQueryBuilder()
            ->from($params['from'], $params['alias'], $params['indexBy']);

        // добавить в запрос select 
        if (isset($params['select'])) {
            $query->select($params['select']);
        }

        // добавить в запрос addSelect 
        if (isset($params['addSelect'])) {
            foreach ($params['addSelect'] as $dql) {
                $query->addSelect($dql);
            }
        }

        // добавить в запрос INNER 
        if (isset($params['innerJoin'])) {
            foreach ($params['innerJoin'] as $innerJoin) {
                $query->innerJoin(
                    $innerJoin['join'], $innerJoin['alias'], (isset($innerJoin['conditionType']) && $innerJoin['conditionType']) ? $innerJoin['conditionType'] : null, (isset($innerJoin['condition']) && $innerJoin['condition']) ? $innerJoin['condition'] : null, (isset($innerJoin['indexBy']) && $innerJoin['indexBy']) ? $innerJoin['indexBy'] : null
                );
            }
        }

        // добавить в запрос условия
        if (isset($params['andWhere'])) {
            foreach ($params['andWhere'] as $where) {
                $query->andWhere($where);
            }
        }

        // добавить в запрос парамтеры
        if (isset($params['parameters'])) {
            foreach ($params['parameters'] as $parameterKey => $parameterValue) {
                $query->setParameter($parameterKey, $parameterValue);
            }
        }

        // вернуть запрос
        return $query;
    }

}
