<?php
//
//namespace Nitra\MiniTetradkaBundle\Tests\Controller\Order\Mink;
//
///**
// * AbstractOrder
// */
//abstract class AbstractOrder extends \Nitra\MiniTetradkaBundle\Tests\TetradkaMinkTest
//{
//    
//    /**
//     * Тест контроллера
//     * проверяем контроллера тестра
//     */
//    abstract public function testController();
//    
//    /**
//     * Получить первый заказ
//     * @param array $params - массив параметров для запроса
//     * @return array - массив данных 
//     */
//    public function getFirstOrder(array $params = array())
//    {
//        // запрос получения заказа
//        $query = $this->buildQuery(array_merge(array(
//            'select'    => 'o',
//            'from'      => 'NitraMiniTetradkaBundle:Order',
//            'alias'     => 'o',
//        ), $params));
//            
//        // получить записи из БД 
//        return $query
//            ->setMaxResults(1)
//            ->getQuery()
//            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
//    }
//    
//    /**
//     * Получить первого покупателя
//     * @param array $params - массив параметров для запроса
//     * @return array - массив данных 
//     */
//    public function getFirstBuyer(array $params = array())
//    {
//        // запрос получения покупателя
//        $query = $this->buildQuery(array_merge(array(
//            'select'    => 'b',
//            'from'      => 'NitraMiniTetradkaBundle:Buyer',
//            'alias'     => 'b',
//        ), $params));
//            
//        // получить записи из БД 
//        return $query
//            ->setMaxResults(1)
//            ->getQuery()
//            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
//    }
//    
//    /**
//     * Получить статусы заказов
//     * @return array => (methodName => statusData)
//     */
//    public function getOrderStatuses()
//    {
//        
//        // @var array LazyInit массив статусов заказов
//        static $orderStatuses = null;
//
//        // статусы ранее не получали
//        if ($orderStatuses === null) {
//            // получить статусы заказов
//            $orderStatuses = $this->buildQuery(array(
//                    'select' => 'os',
//                    'from' => 'NitraMiniTetradkaBundle:OrderStatus',
//                    'indexBy' => 'os.methodName',
//                    'alias' => 'os'))
//                ->getQuery()
//                ->getArrayResult();
//        }
//        
//        // вернуть статусы
//        return $orderStatuses;
//    }
//    
//    /**
//     * Получить статусы позиций заказов
//     * @return array => (methodName => statusData)
//     */
//    public function getOrderEntryStatuses()
//    {
//        // @var array LazyInit массив статусов позиций заказов
//        static $orderEntryStatuses = null;
//        
//        // статусы ранее не получали
//        if ($orderEntryStatuses === null) {
//            // получить статусы позиций заказов
//            $orderEntryStatuses = $this->buildQuery(array(
//                    'select' => 'oes',
//                    'from' => 'NitraMiniTetradkaBundle:OrderEntryStatus',
//                    'indexBy' => 'oes.methodName',
//                    'alias' => 'oes'))
//                ->getQuery()
//                ->getArrayResult();
//        }
//        
//        // вернуть статусы
//        return $orderEntryStatuses;
//    }
//    
//    /**
//     * Получить счетчик кол-ва заказов
//     * @param array $params - массив параметров для запроса
//     * @return integer
//     */
//    public function getOrderCounter(array $params = array())
//    {   
//        // запрос получения кол-ва заказов
//        $query = $this->buildQuery(array_merge(array(
//            'select'    => 'COUNT(DISTINCT o)',
//            'from'      => 'NitraMiniTetradkaBundle:Order',
//            'alias'     => 'o',
//        ), $params));
//            
//        // получить записи из БД 
//        $rows = $query
//            ->getQuery()
//            ->getOneOrNullResult();
//        
//        // вернуть результат счетчика
//        return ($rows)
//            ? $rows[1]
//            : 0;
//    }
//    
//}
