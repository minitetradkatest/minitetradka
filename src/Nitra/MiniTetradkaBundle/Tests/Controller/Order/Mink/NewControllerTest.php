<?php
//
//namespace Nitra\MiniTetradkaBundle\Tests\Controller\Order\Mink;
//
///**
// * NewControllerTest
// */
//class NewControllerTest extends \Nitra\MiniTetradkaBundle\Tests\Controller\Order\Mink\AbstractOrder
//{
//    
//    /**
//     * {@inheritDoc}
//     */
//    public function testController()
//    {
////        // получить клиента
////        $client = static::loginClient();
////        $client->request('GET', '/order/new');
////        
////        // проверить контроллер
////        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Order\NewController::indexAction', $client->getRequest()->attributes->get('_controller'));
//    }
////    
////    /**
////     * Получить Crawler форма поиска 
////     * @param \Symfony\Component\DomCrawler\Crawler $crawler
////     * @return \Symfony\Component\DomCrawler\Crawler
////     */
////    public function getSearchForm(\Symfony\Component\DomCrawler\Crawler $crawler)
////    {
////        return $crawler->filter('.left_block > form');
////    }
////    
//    
////    /**
////     * Получить форму поиска
////     * @param \Symfony\Bundle\FrameworkBundle\Client $client - клиент для которого получаем фильтр
////     * @return \Symfony\Component\DomCrawler\Crawler
////     */
////    public function getSearchForm(\Symfony\Bundle\FrameworkBundle\Client $client)
////    {
////        // получить форму
////        return $client
////            ->getCrawler()
////            ->filter('.left_block > form');
////    }
////    
////    /**
////     * Получить форму создания
////     * @param \Symfony\Component\DomCrawler\Crawler $crawler
////     * @param array $products - массив продуктов формы
////     * @return \Symfony\Component\DomCrawler\Crawler
////     */
////    public function getOrderForm(\Symfony\Component\DomCrawler\Crawler $crawler)
////    {
////        // получить форму
////        return 
////            $crawler
////            ->filter('.right_block > form');
////    }
////    
////    /**
////     * Получить случаный набор продкутов из массива продкутов
////     * @param   array   $products   - массив продуктов
////     * @param   integer $num        - кол-во возвращаемыз продкутов
////     * @return  array               - массив случайныз продкутов срез с параметра $products
////     */
////    public function getRandomProducts(array $products, $num=1)
////    {
////        // получить массив ID продкутов
////        $productIds = array_keys($products);
////        shuffle($productIds);
////        
////        // получить набор случайных продуктов
////        $productsRand = array();
////        for ($i = 0; $i < $num; $i++) {
////            $productsRand[$productIds[$i]] = $products[$productIds[$i]];
////        }
////        
////        // вернуть массив случайных продктов
////        return $productsRand;
////    }
////    
////    /**
////     * Получить html формы продкутов из массива продкутов
////     * @param   array   $products   - массив продуктов
////     * @param   string  $pageHtml   - html страница форма заказа без позиций заказа
////     * @return  string              - html страницу форма заказа с позициями заказа
////     */
////    public function getHtmlProducts(array $products, $pageHtml)
////    {
////        
////        // строка поиска
////        $search = '<tbody id="cartList"><tr class="cartProductTrNull" style="display: none">';
////        
////        // строка замены 
////        $replace = '<tbody id="cartList">';
////        // добавить html позиций заказа
////        $iteration = 0;
////        foreach($products as $product) {
////            
////            // обновить счетчик итераций
////            ++$iteration;
////            
////            // добавить html TR для контента продукта позиции заказа
////            $replace .= 
////            '<tr productid="'.$product['id'].'" iteration="'.$iteration.'" id="orderEntryProductId'.$product['id'].'">
////                <td class="order_entry_name">
////                    '.$product['name'].'
////                </td>
////                <td>
////                    <div class="order_entry_quantity">
////                        <label class="required" for="order_orderEntry_'.$iteration.'_quantity" onclick="">Кол-во</label>
////                        <input type="text" class="quantity" required="required" name="order[orderEntry]['.$iteration.'][quantity]" id="order_orderEntry_'.$iteration.'_quantity">
////                    </div>
////                    <div class="order_entry_price_out price">
////                        <label class="required" for="order_orderEntry_'.$iteration.'_priceOut" onclick="">Цена</label>
////                        <input type="text" class="price_out" required="required" name="order[orderEntry]['.$iteration.'][priceOut]" id="order_orderEntry_'.$iteration.'_priceOut">
////                    </div>
////                    <div class="order_entry_total number">
////                        <label onclick="" for="order_entry_total">Всего</label>
////                        <input type="text" value="686" readonly="" class="order_entry_total_input">
////                    </div>
////                </td>
////                <td onclick="order.form.delProduct(this);" class="order_entry_delete">
////                    <a onclick="order.form.delProduct(this);" alt="Удалить" href="javascript:void(0);" class="link_icon">
////                        <i class="i_delete"></i>
////                    </a>
////                    <input type="hidden" name="order[orderEntry]['.$iteration.'][productId]" id="order_orderEntry_'.$iteration.'_productId" value="'.$product['id'].'">
////                </td>
////            </tr>';
////        }
////        
////        // добавить спрятанный tr 
////        $replace .= '<tr class="cartProductTrNull" style="display: none">';
////        
////        // вернуть html страницу форма заказа с позициями заказа
////        return str_replace($search, $replace, $pageHtml);
////    }
////
////    /**
////     * Поиск товара в форме заказа товары не найдены
////     * @depends testController
////     */
////    public function testSearchProductsNotFound()
////    {
//////        // получить клиента
//////        $client = static::loginClient();
//////        $client->request('GET', '/order/new');
//////        $crawler = $client->getCrawler();
//////        
//////        // форма поиска товаров
//////        $form = $this->getSearchForm($crawler)->form(array(
//////            'search_product[name]' => 'NONE_EXISTEN_PRODUCT_NAME',
//////        ));
//////        $client->submit($form);
//////        
//////        // результат поиска товаров
//////        $result = trim($client->getResponse()->getContent());
//////        $this->assertEmpty($result);
////    }
////    
////    /**
////     * Поиск товара по одному слову пример: "товар"
////     * @depends testSearchProductsNotFound
////     */
////    public function testSearchProductsOneWord()
////    {
//////        // получить клиента
//////        $client = static::loginClient();
//////        $client->request('GET', '/order/new');
//////        $crawler = $client->getCrawler();
//////        
//////        // форма поиска товаров
//////        $form = $this->getSearchForm($crawler)->form(array(
//////            'search_product[name]' => 'товар',
//////        ));
//////        $client->submit($form);
//////        
//////        // результат поиска товаров
//////        $result = trim($client->getResponse()->getContent());
//////        $this->assertNotEmpty($result);
////    }
//    
////    /**
////     * Поиск товара по нескольким словам пример: "товар бренд"
////     * @depends testSearchProductsOneWord
////     */
////    public function testSearchProductsSeveralWords()
////    {
//////        // получить клиента
//////        $client = static::loginClient();
//////        $client->request('GET', '/order/new');
//////        $crawler = $client->getCrawler();
//////        
//////        // форма поиска товаров
//////        $form = $this->getSearchForm($crawler)->form(array(
//////            'search_product[name]' => 'товар бренд',
//////        ));
//////        $client->submit($form);
//////        
//////        // результат поиска товаров
//////        $result = trim($client->getResponse()->getContent());
//////        $this->assertNotEmpty($result);
////    }
//
////    
////    /**
////     * Поиск товара в форме заказа 
////     * @depends testSearchProductsNotFound
////     */
////    public function testSearchProducts()
////    {
////        // получить клиента
////        $client = static::loginClient();
////        $client->request('GET', '/order/new');
////        $crawler = $client->getCrawler();
////        
////        // форма поиска товаров
////        $form = $this->getSearchForm($crawler)->form(array(
////            'search_product[name]' => 'Товар',
////        ));
////        $client->submit($form);
////        
////        // результат поиска товаров
////        $html = trim($client->getResponse()->getContent());
////        $this->assertTrue(!empty($html));
////        
////        // преобразовать результат поиска продуктов в массив найденных продуктов
////        $products = array();
////        $productsCrawler = new \Symfony\Component\DomCrawler\Crawler();
////        $productsCrawler->addHtmlContent($html);
////        // обойти все найденные продукты
////        foreach($productsCrawler->filter('tbody')->children() as $trKey => $trRow) {
////            // запомнить продукт
////            $products[$trRow->getAttribute('productid')] = array(
////                'id'        => $trRow->getAttribute('productid'),
////                'name'      => $trRow->getAttribute('productname'),
////                'article'   => $trRow->getAttribute('productarticle'),
////                'priceOut'  => $trRow->getAttribute('priceout'),
////                'isActive'  => ($trRow->getAttribute('isactive')) ? true : false,
////            );
////        }
////        
////        // проверить кол-во найденных продуктов
////        $this->assertGreaterThan(0, count($products));
////        
////        // вернуть маасив найденныз продуктов
////        return $products;
////    }
//
//    
////    protected $coverageScriptUrl = 'http://nitratetradkamini/app_test.php';
////    
////    /**
////     * @var boolean
////     */
////    protected $collectCodeCoverageInformation = true;
////    
//    
//    
////    protected   $coverageScriptUrl = 'http://nitratetradkamini/bundles/mink/phpunit_coverage.php';
////    private     $collectCodeCoverageInformationon = true;
//    
////    @depends testSearchProductsSeveralWords
//    /**
//     * Тестирование создания новой записи
//     * 
//     */
//    public function testAddNew()
//    {
//        
//        // авторизовать клиента
//        static::loginClient();
//        
//        // получить кол-во заказов передт тестированием
//        $countOrderBefore = $this->getOrderCounter();
//        
//        // Set cookie for selenium test coverage!!!
//        $this->initTestCoverage(); 
//        
//        // получить сессию
//        $session = $this->getMink()->getSession('selenium2');
//        
////        $session = $this->getMink()->getSession('sahi');
//        
////        $session->visit($this->getBaseUrl() . '/order/new');
//        $this->initTestCoverage(); 
//        $session->visit($this->getBaseUrl() . '/order');
//        $session->visit($this->getBaseUrl() . '/order/new');
//        // получить страницу
//        $page = $session->getPage();
//        
//        // получить поле поиска 
//        $page->selectFieldOption('search_product_categoryId', '5301e46557d0939314000065');
//        $searchFild = $page->findById('search_product_name');
//        $searchFild->setValue('Товар');
//        
//        
//        
////        $page->
//        $page->findById('search_product_name')->setValue('товар');
//        
//        
//        $session->wait(1000);
////        
////        echo $page->getHtml();
//        
////        // получить поле поиска 
////        $searchFild = $page->findById('search_product_name');
////        $this->assertNotNull($searchFild);
////        
////        echo "\n",get_class($page);
////        echo "\n",gettype($searchFild);
////        
////        echo $page->getHtml();
////        
////        die("\n\n\nOK\n\n\n");
////        echo $this->getBaseUrl();
////        die;
//        
//        // проверить кол-во
//        --$countOrderBefore;
//        $this->assertGreaterThan($countOrderBefore, $this->getOrderCounter());
//        
//    }
//    
//    
////    /**
////     * Тестирование создания новой записи
////     * @depends testSearchProducts
////     */
////    public function testNew($products)
////    {
////        
////        // получить кол-во заказов передт тестированием
////        $countOrderBefore = $this->getOrderCounter();
////        
////        // получить клиента
////        $client = static::loginClient();
////        $client->request('GET', '/order/new');
////        $crawler = $client->getCrawler();
////        $form = $this->getOrderForm($crawler)->form();
////        
////        // массив продуктов позиций заказа
////        $randomCount = rand(1, count($products)/2);
////        $randomProducts = $this->getRandomProducts($products, $randomCount);
////        $html = $this->getHtmlProducts($randomProducts, $crawler->html());
////        
////        // преобразовать результат в форму заказа 
////        $crawler = new \Symfony\Component\DomCrawler\Crawler(null, $form->getUri());
////        $crawler->addHtmlContent($html);
////        
////        // покупатель
////        $buyer = $this->getFirstBuyer();
////        
////        // данные формы создания заказа
////        $formValues = array(
////            // имя покупателя
////            'order[buyer_name][id]'     => $buyer['id'],
////            'order[buyer_name][name]'   => $buyer['name'],
////            // телефон покупателя
////            'order[buyer_phone][id]'    => $buyer['id'],
////            'order[buyer_phone][name]'  => $buyer['phone'],
////            // комментарий к заказу
////            'order[commentAdd]' => 'Заказ создан системой тестирования',
////        );
////        
////        // добавить позиции заказа
////        $iteration = 0; // счетчик позиций заказа 
////        foreach($randomProducts as $product) {
////            // увеличить счетчик позиций заказа
////            ++$iteration;
////            // добавить позицию заказа
////            $formValues['order[orderEntry]['.$iteration.'][quantity]'] = rand(1, 3);
////            $formValues['order[orderEntry]['.$iteration.'][priceOut]'] = $product['priceOut'];
////        }
////        
////        // форма создания заказа
////        $form = $this->getOrderForm($crawler)->form($formValues);
////        
////        // клик создание новой записи
////        $client->submit($form);
////        
////        // получить кол-во после теста
////        $countOrderAfter = $this->getOrderCounter();
////        
////        // проверить кол-во
////        $this->assertGreaterThan($countOrderBefore, $countOrderAfter);
////    }
////    
//}
