<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Order;

/**
 * ListControllerTest
 */
class ListControllerTest extends AbstractOrder
{

    /**
     * Получить форму фильтра
     * @param \Symfony\Bundle\FrameworkBundle\Client $client - клиент для которого получаем фильтр
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client)
    {
        // получить форму
        return $client
                ->getCrawler()
                ->filter('html > body .content > div .list_filters > form');
    }

    /**
     * process формы фильтра
     * @param \Symfony\Bundle\FrameworkBundle\Client $client - клиент для которого получаем фильтр
     * @param array  $values An array of values for the form fields
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function processFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client, array $values = null)
    {
        // получить форму фильтра
        $form = $this->getFilterForm($client)->form();
        // получить все поля 
        $formValues = $form->getValues();
        foreach ($formValues as $field => $value) {
            // поле токен не сбрасываем
            if ($field == 'filters_order[_token]') {
                continue;
            }
            // сбросить значение поля
            $formValues[$field] = null;
        }

        // установить новые знаяения
        foreach ($values as $field => $value) {
            $formValues[$field] = $value;
        }

        // установить обнуленные знаяения формы
        $form->setValues($formValues);
        $client->submit($form);
        $client->followRedirect();
        return $client->getCrawler();
    }

    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Order\ListController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }

    /**
     * Тестирование отображения списка 
     * @depends testController
     */
    public function testList()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');
        $crawler = $client->getCrawler();
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (' . $this->getOrderCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тест Сброс фильтра
     * @depends testList
     */
    public function testResetFilters()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array());

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (' . $this->getOrderCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр ID заказа 
     * @depends testResetFilters
     */
    public function testFilterOrderId()
    {
        // получить первый заказ
        $order = $this->getFirstOrder();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[id]' => $order['id'],
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр ID не существующего заказа 
     * @depends testFilterOrderId
     */
    public function testFilterOrderIdNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[id]' => '1001_NOT_EXISTEN_ORDER_ID',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестирование фильтр статус заказа
     * @depends testFilterOrderIdNotFound
     */
    public function testOrderStatus()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // для каждого статуса получить кол-во закзов в статусе
        foreach ($this->getOrderStatuses() as $methodName => $orderStatus) {

            // параметры получения счетчика
            $params = array(
                'innerJoin' => array(
                    array('join' => 'o.orderStatus', 'alias' => 'os')
                ),
                'andWhere' => array(
                    'os.id = :orderStatusId',
                ),
                'parameters' => array(
                    'orderStatusId' => $orderStatus['id'],
                ),
            );

            // получить кол-во заказов
            $orderCounter = $this->getOrderCounter($params);

            // фильтр по статусу
            $crawler = $this->processFilterForm($client, array(
                'filters_order[orderStatus]' => $orderStatus['id'],
            ));

            // проверить заголовок списка
            $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (' . $orderCounter . ')")')->count();
            $this->assertTrue($hasHeader > 0);
        }
    }

    /**
     * Тестирование фильтр статус заказа
     * @depends testOrderStatus
     */
    public function testOrderEntryStatus()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // для каждого статуса получить кол-во закзов в статусе
        foreach ($this->getOrderEntryStatuses() as $methodName => $orderEntryStatus) {

            // параметры получения счетчика
            $params = array(
                'andWhere' => array(
                    '(EXISTS (
                    SELECT oe 
                    FROM NitraMiniTetradkaBundle:OrderEntry oe 
                    WHERE oe.order = o.id 
                        AND oe.orderEntryStatus = :orderEntryStatusId
                    ))'
                ),
                'parameters' => array(
                    'orderEntryStatusId' => $orderEntryStatus['id'],
                ),
            );

            // получить кол-во заказов
            $orderCounter = $this->getOrderCounter($params);

            // фильтр по статусу
            $crawler = $this->processFilterForm($client, array(
                'filters_order[orderEntryStatus]' => $orderEntryStatus['id'],
            ));

            // проверить заголовок списка
            $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (' . $orderCounter . ')")')->count();
            $this->assertTrue($hasHeader > 0);
        }
    }

    /**
     * Тестировать фильтр по названию товара
     * @depends testOrderEntryStatus
     */
    public function testFilterProductName()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[productName]' => 'Товар',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (' . $this->getOrderCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по названию товара не найден
     * @depends testFilterProductName
     */
    public function testFilterProductNameNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[productName]' => '1001_NOT_EXISTEN_PRODUCT_NAME',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по покупателю ID
     * @depends testFilterProductNameNotFound
     */
    public function testFilterBuyerId()
    {
        // параметры получения заказа 
        // получить первый заказ с данными покупателя
        $order = $this->getFirstOrder(array(
            'addSelect' => array('b'),
            'innerJoin' => array(
                array('join' => 'o.buyer', 'alias' => 'b')
            ),
        ));

        // получить кол-во заказов для для найденного покупателя
        $orderCounter = $this->getOrderCounter(array(
            'innerJoin' => array(
                array('join' => 'o.buyer', 'alias' => 'b')
            ),
            'andWhere' => array(
                'b.id = :orderBuyerId',
            ),
            'parameters' => array(
                'orderBuyerId' => $order['buyer']['id'],
            ),
        ));

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[buyer][id]' => $order['buyer']['id'],
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (' . $orderCounter . ')")')->count();
        $this->assertTrue($hasHeader > 0);
        $this->assertTrue(true);
    }

    /**
     * Тестировать фильтр по покупателю ID
     * @depends testFilterBuyerId
     */
    public function testFilterBuyerIdNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[buyer][id]' => '1001_NOT_EXISTEN_BUYER_ID',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по дате создания заказа
     * @depends testFilterBuyerIdNotFound
     */
        public function testFilterOrderDate()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');
        $tdate = date("d M Y", strtotime('tomorrow'));
        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[createdAt][from]' => date('d M Y'),
            'filters_order[createdAt][to]' => $tdate,
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertFalse($hasHeader > 0);
    }
        /**
     * Тестировать фильтр по дате создания заказа
     * @depends testFilterOrderDate
     */
    public function testFilterOrderDateNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[createdAt][from]' => '22 Nov 2005',
            'filters_order[createdAt][to]' => '22 Nov 2005',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по дате доставки заказа
     * @depends testFilterOrderDateNotFound
     */
    public function testFilterOrderDeliveryDate()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[deliveryDate][from]' => date('d M Y'),
            'filters_order[deliveryDate][to]' => date('d M Y'),
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertFalse($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по дате доставки заказа
     * @depends testFilterOrderDeliveryDate
     */
    public function testFilterOrderDeliveryDateNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[deliveryDate][from]' => '22 Nov 2005',
            'filters_order[deliveryDate][to]' => '22 Nov 2005',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по адресной доставке
     * @depends testFilterOrderDeliveryDateNotFound
     */
    public function testFilterOrderIsDirectYes()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[isDirect]' => '1',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по адресной доставке
     * @depends testFilterOrderIsDirectYes
     */
    public function testFilterOrderIsDirectNo()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[isDirect]' => '0',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (1)")')->count();
        $this->assertFalse($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по наложному платежу
     * @depends testFilterOrderIsDirectNo
     */
    public function testFilterOrderIsCodYes()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[isCod]' => '1',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по наложному платежу
     * @depends testFilterOrderIsCodYes
     */
    public function testFilterOrderIsCodNo()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[isCod]' => '0',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (1)")')->count();
        $this->assertFalse($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по наложному платежу
     * @depends testFilterOrderIsCodNo
     */
    public function testFilterOrderCreatedBy()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[createdBy]' => 'admin',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по наложному платежу
     * @depends testFilterOrderCreatedBy
     */
    public function testFilterOrderCreatedByNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/order/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_order[createdBy]' => '1001_NOT_EXISTEN_USER',
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Заказы (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

}
