<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Order;

/**
 * AbstractOrder
 */
abstract class AbstractOrder extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{
    
    /**
     * Тест контроллера
     * проверяем контроллера тестра
     */
    abstract public function testController();
    
    /**
     * @var array массив статусов заказов
     * LazyInit
     */
    protected static $orderStatuses;

    /**
     * @var array массив статусов позиций заказов
     * LazyInit
     */
    protected static $orderEntryStatuses;
    
    /**
     * Получить запрос получения заказов
     * @param array $params - массив параметров для запроса
     * @return Doctrine\ORM\QueryBuilder
     */
    public function buildQuery(array $params = null)
    {
        // проверить обязательные параметры from
        if (!isset($params['from']) || !$params['from']) {
            throw new \LogicException('Не указан обязательный параметр "from" для запроса');
        }
        
        // проверить обязательные параметр alias
        if (!isset($params['alias']) || !$params['alias']) {
            throw new \LogicException('Не указан обязательный параметр "alias" для запроса');
        }
        
        // проверить обязательные параметр indexBy
        if (!isset($params['indexBy']) || !$params['indexBy']) {
            $params['indexBy'] = null;
        }
        
        // запрос получения кол-ва заказов
        $query = static::getEntityManager()
            ->createQueryBuilder()
            ->from($params['from'], $params['alias'], $params['indexBy']);
        
        // добавить в запрос select 
        if (isset($params['select'])) {
            $query->select($params['select']);
        }
        
        // добавить в запрос addSelect 
        if (isset($params['addSelect'])) {
            foreach($params['addSelect'] as $dql) {
                $query->addSelect($dql);
            }
        }
        
        // добавить в запрос INNER 
        if (isset($params['innerJoin'])) {
            foreach($params['innerJoin'] as $innerJoin) {
                $query->innerJoin(
                    $innerJoin['join'], 
                    $innerJoin['alias'], 
                    (isset($innerJoin['conditionType']) && $innerJoin['conditionType']) ? $innerJoin['conditionType'] : null,
                    (isset($innerJoin['condition']) && $innerJoin['condition']) ? $innerJoin['condition'] : null,
                    (isset($innerJoin['indexBy']) && $innerJoin['indexBy']) ? $innerJoin['indexBy'] : null
                );
            }
        }

        // добавить в запрос условия
        if (isset($params['andWhere'])) {
            foreach($params['andWhere'] as $where) {
                $query->andWhere($where);
            }
        }
        
        // добавить в запрос парамтеры
        if (isset($params['parameters'])) {
            foreach($params['parameters'] as $parameterKey => $parameterValue) {
                $query->setParameter($parameterKey, $parameterValue);
            }
        }
        
        // вернуть запрос
        return $query;
    }
    
    /**
     * Получить первый заказ
     * @param array $params - массив параметров для запроса
     * @return array - массив данных 
     */
    public function getFirstOrder(array $params = array())
    {
        // запрос получения заказа
        $query = $this->buildQuery(array_merge(array(
            'select'    => 'o',
            'from'      => 'NitraMiniTetradkaBundle:Order',
            'alias'     => 'o',
        ), $params));
            
        // получить записи из БД 
        return $query
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }
    
    /**
     * Получить первого покупателя
     * @param array $params - массив параметров для запроса
     * @return array - массив данных 
     */
    public function getFirstBuyer(array $params = array())
    {
        // запрос получения покупателя
        $query = $this->buildQuery(array_merge(array(
            'select'    => 'b',
            'from'      => 'NitraMiniTetradkaBundle:Buyer',
            'alias'     => 'b',
        ), $params));
            
        // получить записи из БД 
        return $query
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }
    
    /**
     * Получить статусы заказов
     * @return array => (methodName => statusData)
     */
    public function getOrderStatuses()
    {
        // статусы ранее не получали
        if (static::$orderStatuses === null) {
            // получить статусы заказов
            static::$orderStatuses = $this->buildQuery(array(
                    'select' => 'os',
                    'from' => 'NitraMiniTetradkaBundle:OrderStatus',
                    'indexBy' => 'os.methodName',
                    'alias' => 'os'))
                ->getQuery()
                ->getArrayResult();
        }
        
        // вернуть статусы
        return static::$orderStatuses;
    }
    
    /**
     * Получить статусы позиций заказов
     * @return array => (methodName => statusData)
     */
    public function getOrderEntryStatuses()
    {
        // статусы ранее не получали
        if (static::$orderEntryStatuses === null) {
            // получить статусы позиций заказов
            static::$orderEntryStatuses = $this->buildQuery(array(
                    'select' => 'oes',
                    'from' => 'NitraMiniTetradkaBundle:OrderEntryStatus',
                    'indexBy' => 'oes.methodName',
                    'alias' => 'oes'))
                ->getQuery()
                ->getArrayResult();
        }
        // вернуть статусы
        return static::$orderEntryStatuses;
    }
    
    /**
     * Получить счетчик кол-ва заказов
     * @param array $params - массив параметров для запроса
     * @return integer
     */
    public function getOrderCounter(array $params = array())
    {   
        // запрос получения кол-ва заказов
        $query = $this->buildQuery(array_merge(array(
            'select'    => 'COUNT(DISTINCT o)',
            'from'      => 'NitraMiniTetradkaBundle:Order',
            'alias'     => 'o',
        ), $params));
            
        // получить записи из БД 
        $rows = $query
            ->getQuery()
            ->getOneOrNullResult();
        
        // вернуть результат счетчика
        return ($rows)
            ? $rows[1]
            : 0;
    }
    
}
