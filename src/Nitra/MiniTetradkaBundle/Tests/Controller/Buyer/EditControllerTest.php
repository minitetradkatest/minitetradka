<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Buyer;

/**
 * EditControllerTest
 */
class EditControllerTest extends AbstractBuyer
{
    
    /**
     * Тестирование контроллера
     */
    public function testController()
    {
        // получить редактируюемого покупателя
        $editBuyer = $this->getFirstBuyer();
        $this->assertTrue(isset($editBuyer['id']));
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/'.$editBuyer['id'].'/edit');
        
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Buyer\EditController::indexAction', $client->getRequest()->attributes->get('_controller'));
        $this->assertEquals($editBuyer['id'], $client->getRequest()->attributes->get('pk'));
    }
    
    /**
     * Редактирование
     * @depends testController
     */
    public function testEdit()
    {
        // получить редактируюемого покупателя
        $editBuyer = $this->getFirstBuyer();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/'.$editBuyer['id'].'/edit');
        $crawler = $client->getCrawler();
        
        // префикс покупателя
        $buyerPrefix = 'test-edit-buyer';
        
        // получить форму
        $form = $crawler->filter('html > body > div .content > div > form')->form(array(
            'edit_buyer[name]'       => $buyerPrefix.'-name-'.date('YmdHis'),
            'edit_buyer[phone]'      => rand(10, 99) . '00000000'.rand(10, 99),
            'edit_buyer[comment]'    => $buyerPrefix.'-comment',
        ));
        
        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();
        
        // Получить покупателя полсе после редактирования
        $updatedBuyer = $this->getFirstBuyer();
        
        // сравнить реультат обновления
        $this->assertEquals($editBuyer['id'], $updatedBuyer['id']);
        $this->assertTrue($editBuyer['name'] != $updatedBuyer['name']);
        $this->assertTrue($editBuyer['phone'] != $updatedBuyer['phone']);
        $this->assertGreaterThan($editBuyer['updatedAt']->getTimestamp(), $updatedBuyer['updatedAt']->getTimestamp());
    }
    
}
