<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Buyer;

/**
 * AutocompleteControllerTest
 */
class AutocompleteControllerTest extends AbstractBuyer
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/');
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Buyer\AutocompleteController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }
    
    /**
     * Тест покупатель не найден
     * @depends testController
     */
    public function testBuyerNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/?q=NOT_EXIST_BUYE&searchBy=name&addNew=0');
        $crawler = $client->getCrawler();
        // получить результат поиска
        $result = trim($client->getResponse()->getContent());
        $this->assertEmpty($result);
    }

    /**
     * Тест покупатель не найден и добавить нового покупателя
     * @depends testBuyerNotFound
     */
    public function testBuyerNotFoundAddNew()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/?q=NOT_EXIST_BUYE&searchBy=name&addNew=1');
        $crawler = $client->getCrawler();
        // получить результат поиска
        $this->assertTrue($crawler->filter(':contains("Новый покупатель")')->count() > 0);
    }

    /**
     * Тест покупатель найден
     * @depends testBuyerNotFoundAddNew
     */
    public function testBuyerSearch()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/?q=покупатель&searchBy=name&addNew=0');
        $crawler = $client->getCrawler();
        // получить результат поиска
        $result = trim($client->getResponse()->getContent());
        $this->assertTrue(strlen($result) > 0);
    }
    
    /**
     * Тест покупатель найден и добавить нового покупателя
     * @depends testBuyerSearch
     */
    public function testBuyerSearchAddNew()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/?q=покупатель&searchBy=name&addNew=1');
        $crawler = $client->getCrawler();
        // получить результат поиска
        $this->assertTrue($crawler->filter(':contains("Новый покупатель")')->count() > 0);
    }
    
    /**
     * Тест покупатель найден по телефону
     * @depends testBuyerSearchAddNew
     */
    public function testBuyerSearchByPhone()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/?q=38067111000&searchBy=phone&addNew=0');
        $crawler = $client->getCrawler();
        // получить результат поиска
        $result = trim($client->getResponse()->getContent());
        $this->assertTrue(strlen($result) > 0);
    }
    
    /**
     * Тест поиска покупатель по умолчанию
     * @depends testBuyerSearchByPhone
     */
    public function testBuyerSearchByDefault()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/?q=покупатель&addNew=0');
        $crawler = $client->getCrawler();
        // получить результат поиска
        $result = trim($client->getResponse()->getContent());
        $this->assertTrue(strlen($result) > 0);
    }
    
    /**
     * Тест поиска покупатель по умолчанию
     * @depends testBuyerSearchByPhone
     */
    public function testBuyerSearchByDefaultNotExistsColumn()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer-autocomplete/?q=38067111000&searchBy=notExistsColumn&addNew=0');
        $crawler = $client->getCrawler();
        // получить результат поиска
        $result = trim($client->getResponse()->getContent());
        $this->assertTrue(strlen($result) > 0);
    }
    
}
