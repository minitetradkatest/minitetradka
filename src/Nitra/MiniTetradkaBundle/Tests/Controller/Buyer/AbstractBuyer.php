<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Buyer;

/**
 * AbstractBuyer
 */
abstract class AbstractBuyer extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{

    /**
     * Тест контроллера
     * проверяем контроллера тестра
     */
    abstract public function testController();

    /**
     * Получить первого покупателя
     * @return array - массив данных 
     */
    public function getFirstBuyer()
    {
        return static::getEntityManager()
                ->createQueryBuilder()
                ->select('b')
                ->from('NitraMiniTetradkaBundle:Buyer', 'b')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }

    public function getBuyerCounter(array $params = array())
    {
        // запрос получения кол-ва заказов
        $query = $this->buildQuery(array_merge(array(
            'select' => 'COUNT(DISTINCT b)',
            'from' => 'NitraMiniTetradkaBundle:Buyer',
            'alias' => 'b',
                ), $params));

        // получить записи из БД 
        $rows = $query
            ->getQuery()
            ->getOneOrNullResult();

        // вернуть результат счетчика
        return ($rows) ? $rows[1] : 0;
    }

}
