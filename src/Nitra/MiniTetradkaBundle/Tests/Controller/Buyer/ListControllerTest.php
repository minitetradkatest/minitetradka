<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Buyer;

/**
 * ListControllerTest
 */
class ListControllerTest extends AbstractBuyer
{

    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/');
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Buyer\ListController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }

    /**
     * Тестирование отображения списка 
     * @depends testController
     */
    public function testList()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/');
        $crawler = $client->getCrawler();
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список клиентов (' . $this->getBuyerCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестирование отображения списка 
     * @depends testList
     */
    public function getFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client)
    {
        // получить форму
        return $client
                ->getCrawler()
                ->filter('html > body .content > div .list_filters > form');
    }

    /**
     * process формы фильтра
     * @param \Symfony\Bundle\FrameworkBundle\Client $client - клиент для которого получаем фильтр
     * @param array  $values An array of values for the form fields
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function processFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client, array $values = null)
    {
        // получить форму фильтра
        $client = static::loginClient();
        $client->request('GET', '/buyer/');
        $form = $this->getFilterForm($client)->form();
        // получить все поля 
        $formValues = $form->getValues();
        foreach ($formValues as $field => $value) {
            // поле токен не сбрасываем
            if ($field == 'filters_buyer[token]') {
                continue;
            }
            // сбросить значение поля
            $formValues[$field] = null;
            unset($formValues[$field]);
        }
        // установить новые знаяения
        foreach ($values as $field => $value) {
            $formValues[$field] = $value;
        }
        // установить обнуленные значения формы
        $form->setValues($formValues);
        $client->submit($form);
        $client->followRedirect();
             
        return $client->getCrawler();
    }

    /**
     * Тест Сброс фильтра
     * @depends testList
     */
    public function testResetFilters()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array());

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список клиентов (' . $this->getBuyerCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр имя клиента
     * @depends testList
     */
    public function testFilterBuyerName()
    {
        // получить первый заказ
        $buyer = $this->getFirstBuyer();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/');
        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_buyer[name]' => $buyer['name'],
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список клиентов (1)")')->count();

        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по имени не существующего клиента
     * @depends testFilterBuyerName
     */
    public function testFilterBuyerNameNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_buyer[name]' => '1001_NOT_EXISTEN_BUYER_NAME',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список клиентов (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр имя клиента
     * @depends testFilterBuyerNameNotFound
     */
    public function testFilterBuyerPhone()
    {
        // получить первый заказ
        $buyer = $this->getFirstBuyer();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_buyer[name]' => '',
            'filters_buyer[phone]' => $buyer['phone'],
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список клиентов (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по имени не существующего клиента
     * @depends testFilterBuyerName
     */
    public function testFilterBuyerPhoneNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_buyer[phone]' => '1001_NOT_EXISTEN_BUYER_PHONE',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список клиентов (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

}
