<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Buyer;

/**
 * NewControllerTest
 */
class NewControllerTest extends AbstractBuyer
{
    
    /**
     * Тестирование контроллера
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/new');
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Buyer\NewController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }
    
    /**
     * Тестирование создания новой записи
     * @depends testController
     */
    public function testNew()
    {
        // получить кол-во валют передт тестов
        $countBuyerBefore = $this->getBuyerCounter();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/new');
        $crawler = $client->getCrawler();
        
        // префикс покупателя
        $buyerPrefix = 'test-new-buyer'.($countBuyerBefore+1);
        
        // получить форму
        $form = $crawler->filter('html > body > div .content > div > form')->form(array(
            'new_buyer[name]'       => $buyerPrefix.'-name',
            'new_buyer[phone]'      => '9876543210'.rand(10, 99),
            'new_buyer[comment]'    => $buyerPrefix.'-comment',
        ));
        
        // клик создание новой записи
        $client->submit($form);
        
        // получить кол-во валют после теста
        $countBuyerAfter = $this->getBuyerCounter();
        
        // проверить кол-во
        $this->assertGreaterThan($countBuyerBefore, $countBuyerAfter);
    }
    
}
