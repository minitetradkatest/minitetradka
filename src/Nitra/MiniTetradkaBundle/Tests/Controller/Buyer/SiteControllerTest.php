<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Buyer;

/**
 * SiteControllerTest
 */
class SiteControllerTest extends AbstractBuyer
{
    
    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить покупателя 
        $buyer = $this->getFirstBuyer();
        $this->assertTrue(isset($buyer['id']));
        
        // получить не авторизованного клиента
        $client = static::loginClient();
        $client->request('GET', '/buyer/get-buyer-info-site');
        $crawler = $client->getCrawler();
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Buyer\SiteController::getBuyerInfoAction', $client->getRequest()->attributes->get('_controller'));
    }
    
    /**
     * получить массив проверяемых url
     * @return array
     */
    public function getUrls()
    {
        // получить покупателя
        $buyer = $this->getFirstBuyer();
        
        // array( 'Ключевое_слово_в ответе', 'URL' )
        return array(
            array('error',      '/buyer/get-buyer-info-site'),
            array('error',      '/buyer/get-buyer-info-site?id=1000000000000_BUYER_ID_NOT_EXISTS'),
            array('error',      '/buyer/get-buyer-info-site?phone=BUYER_PHONE_NOT_FOUND'),
            array('orders',     '/buyer/get-buyer-info-site?id='.$buyer['id'].'&orders=1'),
            array('success',    '/buyer/get-buyer-info-site?id='.$buyer['id']),
            array('success',    '/buyer/get-buyer-info-site?phone='.$buyer['phone']),
        );
    }
    
    /**
     * @depends testController
     */
    public function testUrls()
    {
        // получить клиента
        $client = static::loginClient();
        
        // получить URL 
        $urls = $this->getUrls();
        
        // обойти все тестируемые URL 
        foreach($urls as $url) {
            // получить контент по url
            $client->request('GET', $url[1]);
            $crawler = $client->getCrawler();
            // проверяем stristr потому что ответ в виде json 
            // $crawler->getHtml() || $crawler->filter() не доступны
            $result = (stristr($client->getResponse()->getContent(), $url[0]))
                ? true 
                : false;
            $this->assertTrue($result);
            
        }
    }
    
}
