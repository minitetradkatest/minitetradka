<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\GeoRegion;

/**
 * AbstractGeoRegion
 */
abstract class AbstractGeoRegion extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{

    /**
     * Тест контроллера
     */
    abstract public function testController();

     /**
     * Получить регион
     */
    public function getFirstGeoRegion()
    {
        return static::getEntityManager()
                ->createQueryBuilder()
                ->select('gr')
                ->from('NitraTetradkaGeoBundle:Region', 'gr')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * Получить счетчик кол-ва регионов
     * @return integer
     */
    public function getGeoRegionCounter()
    {
        // получить записи из БД 
        $rows = static::getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(gr)')
            ->from('NitraTetradkaGeoBundle:Region', 'gr')
            ->getQuery()
            ->getOneOrNullResult();

        // вернуть результат счетчика
        return ($rows) ? $rows[1] : 0;
    }

}
