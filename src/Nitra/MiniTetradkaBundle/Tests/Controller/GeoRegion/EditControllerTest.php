<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\GeoRegion;

/**
 * EditControllerTest
 */
class EditControllerTest extends AbstractGeoRegion
{

    /**
     * Тестирование контроллера
     */
    public function testController()
    {
        // получить редактируемый регион
        $editGeoRegion = $this->getFirstGeoRegion();
        $this->assertTrue(isset($editGeoRegion['id']) && !is_null($editGeoRegion['id']));

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-region/' . $editGeoRegion['id'] . '/edit');

        // проверить контроллер
        $this->assertEquals(
            'Nitra\GeoBundle\Controller\Region\EditController::indexAction', $client->getRequest()->attributes->get('_controller')
        );
        $this->assertEquals(
            $editGeoRegion['id'], $client->getRequest()->attributes->get('pk')
        );
    }

    /**
     * Редактирование
     * @depends testController
     */
    public function testEdit()
    {
        // получить редактируюемого покупателя
        $editGeoRegion = $this->getFirstGeoRegion();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-region/' . $editGeoRegion['id'] . '/edit');
        $crawler = $client->getCrawler();

        // префикс региона
        $regionPrefix = 'test-edit-region';

        // получить форму
        $form = $crawler->filter('html > body > div .content > div > form')->form(array(
            'edit_region[name]' => $regionPrefix . '-name-' . date('YmdHis'),
        ));

        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();

        // Получить регион после редактирования
        $updatedGeoRegion = $this->getFirstGeoRegion();

        // сравнить реультат обновления
        $this->assertEquals($editGeoRegion['id'], $updatedGeoRegion['id']);
        $this->assertTrue($editGeoRegion['name'] != $updatedGeoRegion['name']);
        $this->assertGreaterThan($editGeoRegion['updatedAt']->getTimestamp(), $updatedGeoRegion['updatedAt']->getTimestamp());
    }

}
