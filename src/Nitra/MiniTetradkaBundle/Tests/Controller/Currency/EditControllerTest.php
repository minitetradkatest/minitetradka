<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Currency;

/**
 * EditControllerTest
 */
class EditControllerTest extends AbstractCurrency
{
    
    /**
     * Получить первую валюту
     * @return array - массив данных валюты
     */
    public function getEditCurrency()
    {
        // получить все валюты
        $currencies = $this->getAllCurrency();
        $arrayKeys = array_keys($currencies);
        return $currencies[$arrayKeys[0]];
    }
    
    /**
     * Тестирование контроллера
     */
    public function testController()
    {
        // получить редактируюемую валюту
        $editCurrency = $this->getEditCurrency();
        $this->assertTrue(isset($editCurrency['code']));
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/'.$editCurrency['code'].'/edit');
        
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Currency\EditController::indexAction', $client->getRequest()->attributes->get('_controller'));
        $this->assertEquals($editCurrency['code'], $client->getRequest()->attributes->get('pk'));
    }
    
    /**
     * Редактирование
     * @depends testController
     */
    public function testEdit()
    {
        // получить редактируюемую валюту
        $editCurrency = $this->getEditCurrency();
        
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/'.$editCurrency['code'].'/edit');
        $crawler = $client->getCrawler();
        
        // получить форму
        $form = $crawler->filter('html > body > div .content > div > form')->form(array(
            'edit_currency[exchange]'    => ($editCurrency['exchange'] + 0.10),
        ));
        
        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();
        
        // Получить валюту после редактирования
        $updatedCurrency = $this->getEditCurrency();
        
        // сравнить реультат обновления
        $this->assertEquals($editCurrency['code'], $updatedCurrency['code']);
        $this->assertTrue($editCurrency['exchange'] != $updatedCurrency['exchange']);
        $this->assertGreaterThan($editCurrency['updatedAt']->getTimestamp(), $updatedCurrency['updatedAt']->getTimestamp());
    }
    
}
