<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Currency;

/**
 * NewControllerTest
 */
class NewControllerTest extends AbstractCurrency
{

    /**
     * Тестирование контроллера
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/new');
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Currency\NewController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }

    /**
     * Тестирование создания новой записи
     * @depends testController
     */
    public function testNew()
    {
        // получить кол-во валют передт тестированием
        $countCurrencyBefore = $this->getCurrencyCounter();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/new');
        $crawler = $client->getCrawler();

        // код валюты 
        $code = 'T' . ($countCurrencyBefore + 1);

        // получить форму
        $form = $crawler->filter('html > body > div .content > div > form')->form(array(
            'new_currency[code]' => 'c' . $code,
            'new_currency[name]' => 'name' . $code,
            'new_currency[exchange]' => '1.1',
            'new_currency[symbol]' => 's' . $code,
        ));

        // клик создание новой записи
        $client->submit($form);
        // получить кол-во валют после теста
        $countCurrencyAfter = $this->getCurrencyCounter();

        // проверить кол-во
        $this->assertGreaterThan($countCurrencyBefore, $countCurrencyAfter);
    }

}
