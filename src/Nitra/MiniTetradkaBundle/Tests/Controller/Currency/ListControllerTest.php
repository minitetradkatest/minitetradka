<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Currency;

/**
 * ListControllerTest
 */
class ListControllerTest extends AbstractCurrency
{

    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');
        // проверить контроллер
        $this->assertEquals('Nitra\MiniTetradkaBundle\Controller\Currency\ListController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }

    /**
     * Тестирование отображения списка 
     * @depends testController
     */
    public function testList()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');
        $crawler = $client->getCrawler();
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (' . $this->getCurrencyCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестирование отображения списка 
     * @depends testList
     */
    public function getFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client)
    {
        // получить форму
        return $client
                ->getCrawler()
                ->filter('html > body .content > div .list_filters > form');
    }

    /**
     * process формы фильтра
     * @param \Symfony\Bundle\FrameworkBundle\Client $client - клиент для которого получаем фильтр
     * @param array  $values An array of values for the form fields
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function processFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client, array $values = null)
    {
        // получить форму фильтра
        $client = static::loginClient();
        $client->request('GET', '/currency/');
        $form = $this->getFilterForm($client)->form();
        // получить все поля 
        $formValues = $form->getValues();
        foreach ($formValues as $field => $value) {
            // поле токен не сбрасываем
            if ($field == 'filters_currency[_token]') {
                continue;
            }
            // сбросить значение поля
            $formValues[$field] = null;
        }
        // установить новые знаяения

        foreach ($values as $field => $value) {
            $formValues[$field] = $value;
        }
        // установить обнуленные значения формы
        $form->setValues($formValues);
        $client->submit($form);
        $client->followRedirect();
        return $client->getCrawler();
    }

    /**
     * Тест Сброс фильтра
     * @depends testList
     */
    public function testResetFilters()
    {

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array());

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (' . $this->getCurrencyCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр имя клиента
     * @depends testResetFilters
     */
    public function testFilterСurrencyСode()
    {
        // получить первый заказ
        $currency = $this->getFirstCurrency();
        //  echo "\n\n\n", $currency; die;
        // var_dump($currency['code']);die;
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[code]' => $currency['code'],
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по коду не существующей валюты
     * @depends testFilterСurrencyСode
     */
    public function testFilterСurrencyСodeNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[code]' => '1001_NOT_EXISTEN_CURRENCY_CODE',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр кода валюты
     * @depends testFilterСurrencyСodeNotFound
     */
    public function testFilterСurrencyName()
    {
        // получить первый заказ
        $currency = $this->getFirstCurrency();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[name]' => $currency['name'],
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по имени не существующей валюты
     * @depends testFilterСurrencyName
     */
    public function testFilterСurrencyNameNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[code]' => '1001_NOT_EXISTEN_CURRENCY_NAME',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр значению курса
     * @depends testFilterСurrencyNameNotFound
     */
    public function testFilterСurrencyExchange()
    {
        // получить первый заказ
        $currency = $this->getFirstCurrency();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[exchange]' => $currency['exchange'],
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр не существующее значение курса
     * @depends testFilterСurrencyExchange
     */
    public function testFilterСurrencyExchangeNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[exchange]' => '9999999999999',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр символу валюты
     * @depends testFilterСurrencyExchangeNotFound
     */
    public function testFilterСurrencySymbol()
    {
        // получить первый заказ
        $currency = $this->getFirstCurrency();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[symbol]' => $currency['symbol'],
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр не существующий символ валюты
     * @depends testFilterСurrencySymbol
     */
    public function testFilterСurrencySymbolNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/currency/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_currency[symbol]' => '1001_NOT_EXISTEN_CURRENCY_SYMBOL',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список валют (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

}
