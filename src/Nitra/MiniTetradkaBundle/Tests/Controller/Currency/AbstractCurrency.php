<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\Currency;

/**
 * AbstractCurrency
 */
abstract class AbstractCurrency extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{

    /**
     * Тест контроллера
     * проверяем контроллера тестра
     */
    abstract public function testController();

    /**
     * Получить массив валют в БД
     * Получить массив валют в БД
     * @return array
     */
    public function getAllCurrency()
    {
        // получить записи из БД 
        $rows = static::getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from('NitraMiniTetradkaBundle:Currency', 'c')
            ->getQuery()
            ->getArrayResult();

        // результирующий массив
        $result = array();
        foreach ($rows as $row) {
            $result[$row['code']] = $row;
        }

        // вернуть запист в БД
        return $result;
    }

    /**
     * Получить первого покупателя
     * @return array - массив данных 
     */
    public function getFirstCurrency()
    {
        return static::getEntityManager()
                ->createQueryBuilder()
                ->select('c')
                ->from('NitraMiniTetradkaBundle:Currency', 'c')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }

    public function getCurrencyCounter(array $params = array())
    {
        // запрос получения кол-ва валют
        $query = $this->buildQuery(array_merge(array(
            'select' => 'COUNT(DISTINCT c)',
            'from' => 'NitraMiniTetradkaBundle:Currency',
            'alias' => 'c',
                ), $params));

        // получить записи из БД 
        $rows = $query
            ->getQuery()
            ->getOneOrNullResult();
        // вернуть результат счетчика
        return ($rows) ? $rows[1] : 0;
    }

}
