<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\GeoCity;

/**
 * AbstractGeoCity
 */
abstract class AbstractGeoCity extends \Nitra\MiniTetradkaBundle\Tests\TetradkaTest
{

    /**
     * Тест контроллера
     */
    abstract public function testController();

    /**
     * Получить город
     */
    public function getFirstGeoCity()
    {
        return static::getEntityManager()
                ->createQueryBuilder()
                ->select('gc')
                ->from('NitraTetradkaGeoBundle:City', 'gc')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * Получить счетчик кол-ва городов
     * @return integer
     */
    public function getGeoCityCounter()
    {
        // получить записи из БД 
        $rows = static::getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(gc)')
            ->from('NitraTetradkaGeoBundle:City', 'gc')
            ->getQuery()
            ->getOneOrNullResult();

        // вернуть результат счетчика
        return ($rows) ? $rows[1] : 0;
    }

}
