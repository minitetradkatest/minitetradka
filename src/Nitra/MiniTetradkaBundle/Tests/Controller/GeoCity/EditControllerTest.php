<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\GeoCity;

/**
 * EditControllerTest
 */
class EditControllerTest extends AbstractGeoCity
{

    /**
     * Тестирование контроллера
     */
    public function testController()
    {
        // получить редактируемый город
        $editGeoCity = $this->getFirstGeoCity();
        $this->assertTrue(isset($editGeoCity['id']) && !is_null($editGeoCity['id']));

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-city/' . $editGeoCity['id'] . '/edit');

        // проверить контроллер
        $this->assertEquals(
            'Nitra\GeoBundle\Controller\City\EditController::indexAction', $client->getRequest()->attributes->get('_controller')
        );
        $this->assertEquals(
            $editGeoCity['id'], $client->getRequest()->attributes->get('pk')
        );
    }

    /**
     * Редактирование
     * @depends testController
     */
    public function testEdit()
    {
        // получить редактируюемого покупателя
        $editGeoCity = $this->getFirstGeoCity();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-city/' . $editGeoCity['id'] . '/edit');
        $crawler = $client->getCrawler();

        // префикс региона
        $regionPrefix = 'test-edit-city';

        // получить форму
        $form = $crawler->filter('html > body > div .content > div > form')->form(array(
            'edit_city[name]' => $regionPrefix . '-name-' . date('YmdHis'),
        ));

        // клик создание новой записи
        $client->submit($form);
        $crawler = $client->getCrawler();

        // Получить регион после редактирования
        $updatedGeoCity = $this->getFirstGeoCity();

        // сравнить реультат обновления
        $this->assertEquals($editGeoCity['id'], $updatedGeoCity['id']);
        $this->assertTrue($editGeoCity['name'] != $updatedGeoCity['name']);
        $this->assertGreaterThan($editGeoCity['updatedAt']->getTimestamp(), $updatedGeoCity['updatedAt']->getTimestamp());
    }

}
