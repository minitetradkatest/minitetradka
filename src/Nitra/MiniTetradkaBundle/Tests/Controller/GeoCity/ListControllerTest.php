<?php

namespace Nitra\MiniTetradkaBundle\Tests\Controller\GeoCity;

/**
 * ListControllerTest
 */
class ListControllerTest extends AbstractGeoCity
{

    /**
     * {@inheritDoc}
     */
    public function testController()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-city/');
        // проверить контроллер
        $this->assertEquals('Nitra\GeoBundle\Controller\City\ListController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }

    /**
     * Тестирование отображения списка 
     * @depends testController
     */
    public function testList()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-city/');
        $crawler = $client->getCrawler();
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список городов (' . $this->getGeoCityCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестирование отображения списка 
     * @depends testList
     */
    public function getFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client)
    {
        // получить форму
        return $client
                ->getCrawler()
                ->filter('html > body .content > div .list_filters > form');
    }

    /**
     * process формы фильтра
     * @param \Symfony\Bundle\FrameworkBundle\Client $client - клиент для которого получаем фильтр
     * @param array  $values An array of values for the form fields
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function processFilterForm(\Symfony\Bundle\FrameworkBundle\Client $client, array $values = null)
    {
        // получить форму фильтра
        $client = static::loginClient();
        $client->request('GET', '/geo-city/');
        $form = $this->getFilterForm($client)->form();
        // получить все поля 
        $formValues = $form->getValues();
        foreach ($formValues as $field => $value) {
            // поле токен не сбрасываем
            if ($field == 'filters_city[token]') {
                continue;
            }
            // сбросить значение поля
            $formValues[$field] = null;
            unset($formValues[$field]);
        }
        // установить новые знаяения
        foreach ($values as $field => $value) {
            $formValues[$field] = $value;
        }
        // установить обнуленные значения формы
        $form->setValues($formValues);
        $client->submit($form);
        $client->followRedirect();
        return $client->getCrawler();
    }

    /**
     * Тест Сброс фильтра
     * @depends testList
     */
    public function testResetFilters()
    {

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-city/');

        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array());

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список городов (' . $this->getGeoCityCounter() . ')")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр название города
     * @depends testResetFilters
     */
    public function testFilterCityName()
    {
        // получить первый заказ
        $city = $this->getFirstGeoCity();

        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-city/');
        // сбросить фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_city[name]' => $city['name'],
        ));
        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список городов (1)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

    /**
     * Тестировать фильтр по названию не существующего города
     * @depends testFilterCityName
     */
    public function testFilterCityNameNotFound()
    {
        // получить клиента
        $client = static::loginClient();
        $client->request('GET', '/geo-city/');

        // фильтр
        $crawler = $this->processFilterForm($client, array(
            'filters_city[name]' => '1001_NOT_EXISTEN_CITY_NAME',
        ));

        // проверить заголовок списка
        $hasHeader = $crawler->filter('html > body .content > header > h1:contains("Список городов (0)")')->count();
        $this->assertTrue($hasHeader > 0);
    }

}