<?php
namespace Nitra\MiniTetradkaBundle\Listener;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * OrderPostLoad
 */
class OrderPostLoad 
{
    
    /**
     * конструктор
     */
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }
    
    /**
     * postLoad
     */
    public function postLoad(LifecycleEventArgs $eventArgs) 
    {
        
        // получить сущность
        $entity = $eventArgs->getEntity();
        
        // Подгружаем только для обьектов содержащих ссылку на товар
        if (method_exists($entity, 'getProductId')) {

            // get odm reference to product_id 
            $productId = $entity->getProductId();
            $product = $this->dm->getReference('NitraMiniTetradkaBundle:Product', $productId);
            
            // set the product on the order 
            $em = $eventArgs->getEntityManager();
            $productReflProp = $em->getClassMetadata(get_class($entity))
                ->reflClass->getProperty('product');
            $productReflProp->setAccessible(true);
            $productReflProp->setValue($entity, $product);
        }
    }

}

