<?php
namespace Nitra\MiniTetradkaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\MiniTetradkaBundle\Entity\OrderStatus;

/**
 * LoadOrderStatusData
 * Загрузить статусы позиций заказов
 */
class LoadOrderStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // массив статусов
        $statuses = array();
        $statuses[] = array('methodName' => 'waiting',   'cssClass' => 'i_new2', 'name' => 'Ожидание', 'sortOrder' => 10);
        $statuses[] = array('methodName' => 'ordered',   'cssClass' => 'i_save-and-list yellow', 'name' => 'Заказан',  'sortOrder' => 20);
        $statuses[] = array('methodName' => 'completed', 'cssClass' => 'i_save green', 'name' => 'Завершен', 'sortOrder' => 30);
        $statuses[] = array('methodName' => 'canceled',  'cssClass' => 'i_close red', 'name' => 'Отменен',  'sortOrder' => 40);
        
        // обойти массив создаваемых статусов
        foreach($statuses as $row) {
            
            // создать статус 
            $status = new OrderStatus();
            $status->setMethodName($row['methodName']);
            $status->setCssClass($row['cssClass']);
            $status->setName($row['name']);
            $status->setSortOrder($row['sortOrder']);
            
            // persist
            $manager->persist($status);
            
        }
        
        // сохранить 
        $manager->flush();
    }
    
}
