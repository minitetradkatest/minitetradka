<?php
namespace Nitra\MiniTetradkaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\MiniTetradkaBundle\Entity\Buyer;

/**
 * LoadBuyerData
 * Загрузить покупателей
 */
class LoadBuyerData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // создать записи 
        for ($index = 1; $index <= 10; $index++) {
            
            // название переменной 
            $objName = 'buyer'.$index;
            
            // создать объект 
            $$objName = new Buyer();
            $$objName->setName('Покупатель '.$index);
            $$objName->setPhone('38067111000'.$index);
            $$objName->setComment('Комментарий '.$index);
            $manager->persist($$objName);
            
            // запомнить
            $this->addReference($objName, $$objName);
        }
        
        // сохранить 
        $manager->flush();
    }
    
}
