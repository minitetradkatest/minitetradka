<?php
namespace Nitra\MiniTetradkaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Nitra\MiniTetradkaBundle\Entity\Order;
use Nitra\MiniTetradkaBundle\Entity\OrderEntry;

/**
 * LoadOrderData
 * Загрузить статусы позиций заказов
 */
class LoadOrderData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    private $em;
    
    /**
     * @var массив покупателей
     */
    private $buyers;
    
    /**
     * @var 
     */
    private $products;
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
        
        // установить EntityManager
        $this->em = $this->container->get('doctrine')->getManager();
        
        // установить DocumentManager
        $this->dm = $this->container->get('doctrine_mongodb')->getManager();
    }
    
    
    /**
     * Получить случайного покупателя
     * @return array buyer
     */
    public function getRandomBuyer()
    {
        
        // если ранее не был получен список покупателей
        if (!$this->buyers) {
            // получить покупателей
            $this->buyers = $this->em
                ->createQueryBuilder()
                ->select('b.id, b.name, b.phone')
                ->from('NitraMiniTetradkaBundle:Buyer', 'b', 'b.id')
                ->getQuery()
                ->getArrayResult();
        }
        
        
        // случайный ID покупателя
        $buyerId = rand(min(array_keys($this->buyers)), max(array_keys($this->buyers)));
        
        // вренуть покупателя 
        return $this->buyers[$buyerId];
    }
    /**
     * Получить случайный продукт
     * @return Product
     */
    public function getRandomProduct()
    {
        // если ранее не был получен список продуктов
        if (!$this->products) {
            // получить продукты
            $products = $this->dm
                ->getRepository('NitraMiniTetradkaBundle:Product')
                ->findAll();
            
            // запомнить массив продуктов
            $this->products = array();
            $iteration = 0;
            foreach($products as $product) {
                
                // пропустить не активный продукт
                if (!$product->getIsActive()) {
                    continue;
                }
                
                // имя продукта по умодчанию 
                $productName = trim((string)$product->getFullNameForSearch());
                
                // если имя пустое
                if (!$productName) {
                    // перейти к след. продукту
                    continue;
                }
                
                // запомнить продукт
                $iteration++;
                $this->products[$iteration] = array(
                    'id' => $product->getId(),
                    'name' => $productName,
                    'priceOut' => $product->getStorePriceOut($this->container->getParameter('nitra_minitetradka_store_id')),
                );
            }
        }
        
        // случайный ключ продукта
        $productKey = rand(min(array_keys($this->products)), max(array_keys($this->products)));
        
        // вернуть продкут
        return $this->products[$productKey];
    }
    
    /**
     * получить новый заказ
     * @return Nitra\MiniTetradkaBundle\Entity\Order
     */
    public function getNewOrder()
    {
        
        // получить случайного покупателя
        $buyer = $this->getRandomBuyer();
        
        // получить статус заказа
        $orderStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderStatus')
            ->findOneBy(array(
                'methodName' => $this->container->getParameter('nitra_minitetradka_default_order_status')
            ));
        
        // получить статус позиции заказа
        $orderEntryStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderEntryStatus')
            ->findOneBy(array(
                'methodName' => $this->container->getParameter('nitra_minitetradka_default_order_entry_status')
            ));
        
        // создать заказ 
        $order = new Order();
        $order->setBuyer($this->em->getReference('Nitra\MiniTetradkaBundle\Entity\Buyer', $buyer['id']));
        $order->setOrderStatus($orderStatus);
        $order->setDeliveryDate(new \DateTime('+2 days'));
        
        // добавить позиции заказа 
        for($i=1; $i<=rand(1,10); $i++) {
            
            // получить случайный продукт
            $product = $this->getRandomProduct();
            
            // если не указана цена 
            if (!$product['priceOut']) {
                // получить случайную цену
                $product['priceOut'] = rand(500, 1000);
            }
            
            // создать позицию заказа
            $orderEntry = new OrderEntry();
            $orderEntry->setOrder($order);
            $orderEntry->setQuantity(rand(1, 5));
            $orderEntry->setPriceIn($product['priceOut']);
            $orderEntry->setPriceRecommended($product['priceOut']);
            $orderEntry->setPriceOut($product['priceOut']);
            $orderEntry->setCurrency($this->getReference('currencyUAH'));
            $orderEntry->setOrderEntryStatus($orderEntryStatus);
            $orderEntry->setProduct($this->dm->getReference('Nitra\MiniTetradkaBundle\Document\Product', $product['id']));
            $order->addOrderEntry($orderEntry);
             
            // запомнить позицию заказа
            $this->em->persist($orderEntry);
        }
        
        // вернуть объект заказа
        return $order;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // подгрузить покупателей
        $this->getRandomBuyer();
        
        // получить статус заказа
        $completedOrderStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderStatus')
            ->findOneBy(array('methodName' => 'completed'));
        
        // получить статус позиции заказа
        $completedOrderEntryStatus = $this->em
            ->getRepository('NitraMiniTetradkaBundle:OrderEntryStatus')
            ->findOneBy(array('methodName' => 'completed'));
        
        // получить первого покупателя
        $arrayKeys = array_keys($this->buyers);
        $buyer = $this->buyers[$arrayKeys[0]];
        
        // создаем сразу завершенный заказ без создания истории
        // для первого покупателя данный покупатель фигурирует в тестах с завершенным заказом
        $completedOrder = $this
            ->getNewOrder()
            ->setBuyer($this->em->getReference('Nitra\MiniTetradkaBundle\Entity\Buyer', $buyer['id']))
            ->setOrderStatus($completedOrderStatus);
        
        // установить все позициям заказа статус завершен
        foreach($completedOrder->getOrderEntry() as $orderEntry) {
            $orderEntry->setOrderEntryStatus($completedOrderEntryStatus);
        }
        
        // пометить для сохранения завершенный заказ        
        $manager->persist($completedOrder);
        
        // создать заказы
        for($i=1; $i<=15; $i++) {
            $manager->persist($this->getNewOrder());
        }
        
        // сохранить 
        $manager->flush();
    }
    
}
