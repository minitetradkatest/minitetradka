<?php
namespace Nitra\MiniTetradkaBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Nitra\MiniTetradkaBundle\Entity\Currency;

/**
 * LoadCurrencyData
 * Загрузить валюты
 */
class LoadCurrencyData extends AbstractFixture implements OrderedFixtureInterface 
{

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // uah
        $currencyUAH = new Currency();
        $currencyUAH->setCode('uah');
        $currencyUAH->setName('Гривны');
        $currencyUAH->setExchange(1);
        $currencyUAH->setSymbol('грн.');
        $manager->persist($currencyUAH);
        $this->addReference('currencyUAH', $currencyUAH);
        
        // rub
        $currencyRUB = new Currency();
        $currencyRUB->setCode('rub');
        $currencyRUB->setName('Рубли');
        $currencyRUB->setExchange(0,30);
        $currencyRUB->setSymbol('руб.');
        $manager->persist($currencyRUB);
        $this->addReference('currencyRUB', $currencyRUB);
        
        // usd
        $currencyUSD = new Currency();
        $currencyUSD->setCode('usd');
        $currencyUSD->setName('Доллары');
        $currencyUSD->setExchange(11.50);
        $currencyUSD->setSymbol('$');
        $manager->persist($currencyUSD);
        $this->addReference('currencyUSD', $currencyUSD);
        
        // eur
        $currencyEUR = new Currency();
        $currencyEUR->setCode('eur');
        $currencyEUR->setName('Евро');
        $currencyEUR->setExchange(15.50);
        $currencyEUR->setSymbol('€');
        $manager->persist($currencyEUR);
        $this->addReference('currencyEUR', $currencyEUR);
        
        // сохранить 
        $manager->flush();
    }
    
}

