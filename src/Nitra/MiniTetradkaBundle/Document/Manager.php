<?php
namespace Nitra\MiniTetradkaBundle\Document;

use FOS\UserBundle\Document\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document
 */
class Manager extends BaseUser
{

    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $phone
     *
     * @MongoDB\Field(type="string")
     * @Assert\Length(max = 15)
     */
    private $phone;

    /**
     * @var string $firstname
     *
     * @MongoDB\Field(type="string")
     * @Assert\Length(max = 255)
     */
    private $firstname;

    /**
     * @var string $lastname
     *
     * @MongoDB\Field(type="string")
     * @Assert\Length(max = 255)
     */
    private $lastname;

    /**
     * Конструктор
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Manager
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Manager
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Manager
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    
}
