<?php
namespace Nitra\MiniTetradkaBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document
 */
class Store
{

    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    private $name;

    /**
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    private $host;

    /**
     * @MongoDB\String
     * @Assert\Length(max = 255)
     */
    private $addres;

    /**
     * @MongoDB\Hash
     */
    private $phone = array();

    /**
     * @MongoDB\Hash
     */
    private $email = array();

    /**
     * @MongoDB\Hash
     */
    private $skype = array();

    /**
     * @MongoDB\Hash
     */
    private $icq = array();

    /**
     * @Gedmo\Translatable
     * @MongoDB\String
     */
    private $productDelivery;

    /**
     * @MongoDB\String
     */
    private $analyticsKod;

   

    public function __toString()
    {
        return (String) $this->name;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Store
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return \Store
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * Get host
     *
     * @return string $host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set addres
     *
     * @param string $addres
     * @return \Store
     */
    public function setAddres($addres)
    {
        $this->addres = $addres;
        return $this;
    }

    /**
     * Get addres
     *
     * @return string $addres
     */
    public function getAddres()
    {
        return $this->addres;
    }

    /**
     * Set phone
     *
     * @param hash $phone
     * @return \Store
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return hash $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param hash $email
     * @return \Store
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return hash $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set productDelivery
     *
     * @param string $productDelivery
     * @return \Store
     */
    public function setProductDelivery($productDelivery)
    {
        $this->productDelivery = $productDelivery;
        return $this;
    }

    /**
     * Get productDelivery
     *
     * @return string $productDelivery
     */
    public function getProductDelivery()
    {
        return $this->productDelivery;
    }

    /**
     * Set analyticsKod
     *
     * @param string $analyticsKod
     * @return \Store
     */
    public function setAnalyticsKod($analyticsKod)
    {
        $this->analyticsKod = $analyticsKod;
        return $this;
    }

    /**
     * Get analyticsKod
     *
     * @return string $analyticsKod
     */
    public function getAnalyticsKod()
    {
        return $this->analyticsKod;
    }

    /**
     * Set skype
     *
     * @param hash $skype
     * @return \Store
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
        return $this;
    }

    /**
     * Get skype
     *
     * @return hash $skype
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set icq
     *
     * @param hash $icq
     * @return \Store
     */
    public function setIcq($icq)
    {
        $this->icq = $icq;
        return $this;
    }

    /**
     * Get icq
     *
     * @return hash $icq
     */
    public function getIcq()
    {
        return $this->icq;
    }

}
