<?php

namespace Nitra\TetradkaGeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nitra\GeoBundle\Entity\Region as BaseRegion;

/**
 * Nitra\TetradkaGeoBundle\Entity\Region
 * @ORM\Entity()
 */
class Region extends BaseRegion implements Model\RegionInterface
{
    
}